package com.blue_telescope.xerox_autobot.ui {
	import com.blue_telescope.xerox_autobot.assets.MetricPanelBkg;
	import com.blue_telescope.xerox_autobot.data.DataModel;
	import com.blue_telescope.xerox_autobot.manager.PopupManager;

	import flash.display.Sprite;

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class MetricPanel extends Sprite 
	{
		public static const INIT_PRODUCTION	:Number = .4;	//.5 to .4		05/16/14
		public static const INIT_QUALITY	:Number = .4;	//.5 to .4		05/16/14
		public static const INIT_COST		:Number = .5;
		
		private static const METRIC_X		:Number = 218;
		private static const METRIC_Y		:Number = 17;
		private static const OFFSET_Y		:Number = 52;
		
		private static const UNIT_PRODUCTION:Number = .03;	//.05 to .03	05/16/14
		private static const UNIT_QUALITY	:Number = .03;	//.05 to .03	05/16/14
		private static const UNIT_COST		:Number = .1;
		
		private var _bkg					:MetricPanelBkg;
		private var _productionMetric		:MetricBar;
		private var _qualityMetric			:MetricBar;
		private var _costMetric				:MetricBar;
		
		private var _dataModel				:DataModel;
		
		private var _isAutobotPanel			:Boolean = false;
		
		
		public function MetricPanel(pIsAutobotPanel:Boolean = false) 
		{
			_dataModel = DataModel.getInstance();	
			_isAutobotPanel = pIsAutobotPanel;
			
			configUI();
		}

		private function configUI():void
		{
			_bkg = new MetricPanelBkg();
			addChild(_bkg);
			if (_isAutobotPanel) _bkg.gotoAndStop(2);
			
			_productionMetric = new MetricBar(MetricBar.PRODUCTION, INIT_PRODUCTION);
			_qualityMetric = new MetricBar(MetricBar.QUALITY, INIT_QUALITY);
			_costMetric = new MetricBar(MetricBar.COST, INIT_COST);
			
			_productionMetric.x = _qualityMetric.x = _costMetric.x = METRIC_X;
			_productionMetric.y = METRIC_Y;
			_qualityMetric.y = _productionMetric.y + OFFSET_Y;
			_costMetric.y = _qualityMetric.y + OFFSET_Y;
			
			addChild(_productionMetric);
			addChild(_qualityMetric);
			addChild(_costMetric);
			
			reset();
		}

		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		public function reset():void
		{
			_productionMetric.rate = INIT_PRODUCTION;
			_qualityMetric.rate = INIT_QUALITY;
			_costMetric.rate = INIT_COST;
			//if (_isAutobotPanel) _costMetric.rate = .4;
		}
		
		public function destroy():void
		{
			//trace("INFO MetricPanel :: destroy EVER???, this is " + this);
			
			if (_bkg) removeChild(_bkg);
			
			if (_productionMetric) _productionMetric.destroy();
			if (_qualityMetric) _qualityMetric.destroy();
			if (_costMetric) _costMetric.destroy();
			removeChild(_productionMetric);
			removeChild(_qualityMetric);
			removeChild(_costMetric);
			
			_bkg = null;
			_productionMetric = null;
			_qualityMetric = null;
			_costMetric = null;
		}
		
		public function raiseProduction(multiplication:Number = 1):void
		{
			//trace("INFO MetricPanel :: + raiseProduction by " + UNIT_PRODUCTION*multiplication);
			_productionMetric.rate += UNIT_PRODUCTION*multiplication;
			_dataModel.updateProduction(_productionMetric.rate);
		}
		
		public function dropProduction(multiplication:Number = 1):void
		{
//			trace("INFO MetricPanel :: - dropProduction, _productionMetric is " + _productionMetric);
			if (_productionMetric)
			_productionMetric.rate -= UNIT_PRODUCTION*multiplication;
			_dataModel.updateProduction(_productionMetric.rate);
		}
		
		public function raiseQuality(multiplication:Number = 1):void
		{
			//trace("INFO MetricPanel :: + raiseQuality by " + UNIT_PRODUCTION*multiplication);
			_qualityMetric.rate += UNIT_QUALITY*multiplication;
			_dataModel.updateQuality(_qualityMetric.rate);
		}
		
		public function dropQuality(multiplication:Number = 1):void
		{
//			trace("INFO MetricPanel :: - dropQuality, _qualityMetric:" + _qualityMetric);
			if (_qualityMetric)
			_qualityMetric.rate -= UNIT_QUALITY*multiplication;
			_dataModel.updateQuality(_qualityMetric.rate);
		}
		
		public function raiseCost(multiplication:Number = 1):void
		{
//			trace("INFO MetricPanel :: + raiseCost by " + UNIT_PRODUCTION*multiplication);
			_costMetric.rate += UNIT_COST*multiplication;
			_dataModel.updateCost(_costMetric.rate);
		}
		
		public function dropCost(multiplication:Number = 1):void
		{
//			trace("INFO MetricPanel :: - dropCost , _costMetric:" + _costMetric);
			if (_costMetric)
			_costMetric.rate -= UNIT_COST*multiplication;
			_dataModel.updateCost(_costMetric.rate);
		}
		
		public function stopBlinking():void
		{
			_productionMetric.stopBlinking();
			_qualityMetric.stopBlinking();
			_costMetric.stopBlinking();
		}
	}//c
}//p