package com.blue_telescope.xerox_autobot.ui {
	import com.blue_telescope.xerox_autobot.assets.PopupAsset1;
	import com.blue_telescope.xerox_autobot.assets.PopupAsset2;
	import com.blue_telescope.xerox_autobot.assets.PopupAsset3;
	import com.blue_telescope.xerox_autobot.assets.PopupAsset4;
	import com.blue_telescope.xerox_autobot.assets.PopupAsset5;
	import com.blue_telescope.xerox_autobot.assets.PopupAsset6;
	import com.blue_telescope.xerox_autobot.assets.PopupAsset7;
	import com.blue_telescope.xerox_autobot.data.DataModel;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.getDefinitionByName;

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	[Event(name="skip", type="flash.events.Event")]
	[Event(name="ok", type="flash.events.Event")]
	public class BasePopup extends Sprite 
	{
		public static const TRAINING_POPUP_ID	:uint = 1;
		public static const IT_POPUP_ID			:uint = 2;
		public static const NEWHIRE_POPUP_ID	:uint = 3;
		public static const SECONDSHIFT_POPUP_ID:uint = 4;
		public static const TURNOVER_POPUP_ID	:uint = 5;
		public static const AUTOBOT_POPUP_ID	:uint = 6;
		public static const SUMMARY_POPUP_ID	:uint = 7;
		
		protected var _dummyArr					:Array = [PopupAsset1, PopupAsset2, PopupAsset3, PopupAsset4, PopupAsset5, PopupAsset6, PopupAsset7];
		
		protected var _asset					:*;
		protected var _skipButton				:Sprite;
		protected var _okButton					:Sprite;
		
		protected var _dataModel				:DataModel;
		
		
		private var __promptID					:uint = 1;
		public function set promptID(n:uint):void
		{
			if (__promptID != n) {
				__promptID = n;
				configUI();
			}
		}
		public function get promptID():uint
		{
			return __promptID;
		}
		
		private var __taskID					:uint = 0;	//0 to 9
		public function set taskID(n:uint):void
		{
			if (__taskID != n) {
				__taskID = n;
				if (__promptID == AUTOBOT_POPUP_ID) {
					_asset.label.gotoAndStop(__taskID + 1);
				}
			}
		}
		public function get taskID():uint
		{
			return __taskID;
		}
		
		
		public function BasePopup(pPromptID:uint = 1, taskID:uint=0) 
		{
			__promptID = pPromptID;
			__taskID = taskID;	//for autobot popup
			
			_dummyArr = null;
			_dataModel = DataModel.getInstance();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			configUI();
		}

		protected function configUI():void
		{
			if (_asset) removeChild(_asset);
			
			var assetClassName:String = "com.blue_telescope.xerox_autobot.assets.PopupAsset" + __promptID;
			var ClassRef : Class = getDefinitionByName(assetClassName) as Class;
			_asset = new ClassRef() as MovieClip;
			addChild(_asset);
			
			if (__promptID == AUTOBOT_POPUP_ID) _asset.label.gotoAndStop(__taskID + 1);
			if (__promptID == SUMMARY_POPUP_ID) {
				_asset.taskLeftField.text = _dataModel.getIncompleteNumOfTasks() + "";
				_asset.productionField.text = _dataModel.getProductionEvaluation();
				_asset.qualityField.text = _dataModel.getQualityEvaluation(); 
				_asset.headcountField.text = _dataModel.getCostEvaluation(); 
			}
			
			setButtons();
		}
		
		protected function setButtons():void
		{
			if (_asset.skipButton) {
				_skipButton = _asset.skipButton;
				_skipButton.mouseEnabled = true;
				_skipButton.buttonMode = true;
				_skipButton.mouseChildren = false;
				_skipButton.addEventListener(MouseEvent.CLICK, onSkipPressed);
			}
			if (_asset.okButton) {
				_okButton = _asset.okButton;
				_okButton.mouseEnabled = true;
				_okButton.buttonMode = true;
				_okButton.mouseChildren = false;
				_okButton.addEventListener(MouseEvent.CLICK, onOKPressed);
			}
		}
		
		public function destroy():void
		{
			if (_skipButton) {
				_skipButton.removeEventListener(MouseEvent.CLICK, onSkipPressed);
				_skipButton = null;
			}
			
			if (_okButton) {
				_okButton.removeEventListener(MouseEvent.CLICK, onOKPressed);
				_okButton = null;
			}
			
			if (_asset) {
				removeChild(_asset);
				_asset = null;
			}
		}
		

		////////////////////////////////////////////////////////////////////////////
		// EventHandlers
		////////////////////////////////////////////////////////////////////////////
		protected function onSkipPressed(e:MouseEvent):void 
		{
			dispatchEvent(new Event("skip"));
		}
		
		protected function onOKPressed(e:MouseEvent):void 
		{
			dispatchEvent(new Event("ok"));
		}
	}//c
}//p