package com.blue_telescope.xerox_autobot.ui {
	import com.blue_telescope.xerox_autobot.assets.ClueCircleAsset;

	import flash.display.Sprite;

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	[Event(name="showClue", type="flash.events.Event")]
	[Event(name="hideClue", type="flash.events.Event")]
	public class ClueCircle extends Sprite 
	{
		private var _asset					:ClueCircleAsset;
		private var _taskIcon				:TaskIcon;
		
		private var __taskID					:uint = 0;	
		public function set taskID(n:uint):void
		{
			if (__taskID != n) {
				__taskID = n;
				update();
			}
		}
		public function get taskID():uint
		{
			return __taskID;
		}
		
		
		public function ClueCircle(pTaskID:uint = 0) 
		{
			__taskID = pTaskID;
			configUI();
		}

		private function configUI():void
		{
			_asset = new ClueCircleAsset();
			addChild(_asset);
				
			buildTaskIcon();
		}
		
		private function buildTaskIcon():void
		{
			_taskIcon = new TaskIcon(__taskID);
			_taskIcon.x = -75;
			_taskIcon.y = -50;
			_taskIcon.active = true;
			_taskIcon.mouseChildren = false;
			addChild(_taskIcon);
		}
		
		private function update():void
		{
			if (_taskIcon.taskID != __taskID) {
				removeChild(_taskIcon);
				buildTaskIcon();
			}
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// PublicMethods
		////////////////////////////////////////////////////////////////////////////	
//		public function showTextBubble(isMatching:Boolean = false):void
//		{
//			//
//		}
//		
//		public function hideTextBubble():void
//		{
//			//
//		}
		
		public function showIcon():void
		{
			_taskIcon.visible = true;
		}
		
		public function hideIcon():void
		{
			_taskIcon.visible = false;
		}
	}//c
}//p