package com.blue_telescope.xerox_autobot.ui {
	import com.blue_telescope.xerox_autobot.Configuration;
	import com.blue_telescope.xerox_autobot.assets.StopWatchAsset;
	import com.blue_telescope.xerox_autobot.data.DataModel;
	import com.blue_telescope.xerox_autobot.events.GameEvent;
	import com.blue_telescope.xerox_autobot.manager.PopupManager;
	import com.greensock.TweenLite;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.utils.Timer;

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	[Event(name="activateAutobot", type="com.blue_telescope.xerox_autobot.events.GameEvent")]
	[Event(name="gameEnd", type="com.blue_telescope.xerox_autobot.events.GameEvent")]
	public class StopWatch extends Sprite {
		private var _asset : StopWatchAsset;
		private var _minHand : MovieClip;
		private var _gameTimer : Timer;
		private var _timeCounterField : TextField;
		private var _dataModel : DataModel;
		private var _popupManager : PopupManager;

		public function StopWatch() {
			addEventListener(Event.ADDED_TO_STAGE, configUI);
		}

		private function configUI(e : Event) : void {
			removeEventListener(Event.ADDED_TO_STAGE, configUI);

			_dataModel = DataModel.getInstance();
			_popupManager = PopupManager.getInstance();

			_asset = new StopWatchAsset();
			addChild(_asset);

			_minHand = _asset.minHand;
			_gameTimer = new Timer(1000);

			if (Configuration.isTesting) {
				_timeCounterField = _asset.timeCounter.counterField;
			} else {
				_asset.timeCounter.visible = false;
			}
		}
		
		public function reset():void
		{
			trace("INFO StopWatch :: reset");
			stop();

			TweenLite.killTweensOf(_minHand);
			TweenLite.to(_minHand, .5, {rotation:0, onComplete:function(){_gameTimer.reset();}});
			if (Configuration.isTesting) _timeCounterField.text = 0 + "";
		}

		public function destroy() : void 
		{
//			trace("INFO StopWatch :: destroy!!!!! EVER?????");
			if (_gameTimer) {
				stop();
			}

			if (_asset) {
				removeChild(_asset);
				_asset = null;
			}

			_minHand = null;
			_gameTimer = null;
		}

		public function start() : void 
		{
//			trace("INFO StopWatch :: start!!!!!");
			_gameTimer.addEventListener("timer", timerHandler);
			_gameTimer.start();
		}

		public function stop() : void 
		{
//			trace("INFO StopWatch :: stop");
			_gameTimer.removeEventListener("timer", timerHandler);
			_gameTimer.stop();
		}
		

		// //////////////////////////////////////////////////////////////////////////
		// EventHandlers
		// //////////////////////////////////////////////////////////////////////////
		private function timerHandler(e : TimerEvent) : void 
		{
			var secCount : uint = _gameTimer.currentCount;
			//trace("INFO StopWatch :: timerHandler, secCount is " + secCount);
			if (Configuration.isTesting) _timeCounterField.text = secCount + "";
			
			var targetR : Number = secCount * 2;
			_minHand.rotation = targetR;
			
			switch (secCount) {
				case 15:
					if (Configuration.isTesting) {
						//-- for testing
						dispatchEvent(new GameEvent(GameEvent.ACTIVATE_AUTOBOT));
//						trace("INFO StopWatch :: timerHandler, secCount is 15 and testing!!!");
					}
					break;
				case 37:
					//-- from 4sec to 3sec
				case 90:
					//-- from 3sec to 2sec
				case 150:
					//-- from 2sec to 1sec
					dispatchEvent(new GameEvent(GameEvent.DROP_INTERVAL));
					break;
				case _dataModel.trainingPopupDelay:
					// -- "Time for Training" popup, 39 ///
					_popupManager.openTrainingPopup();
					break;
				case _dataModel.itHelpPopupDelay:
					// -- "IT Help" popup, 57 ///
					_popupManager.openITPopup();
					break;
				case _dataModel.newHiresDelay:
					// -- "New Hires" popup, 75 ///
					_popupManager.openNewHirePopup();
					break;
				case _dataModel.secondShiftPopupDelay:
					// -- "2nd shift" popup, 92
					_popupManager.openSecondShiftPopup(); ///
					break;
				case _dataModel.turnoverPopupDelay:
					// -- "Turnover" popup, 104
					_popupManager.openTurnoverPopup(); ///
					break;
				case _dataModel.activateAutobotDelay:
					// -- "Autobot" popup & activate the "try it With Xerox Autobot" button, 120
					_popupManager.onTurnOverHappened();
					dispatchEvent(new GameEvent(GameEvent.ACTIVATE_AUTOBOT));
					break;
				//case 
				case _dataModel.gameEndDelay:
					// -- gameEnd, 180
					dispatchEvent(new GameEvent(GameEvent.GAME_END));
					stop();
					break;
			}// switch
		}//function
	}// c
}// p