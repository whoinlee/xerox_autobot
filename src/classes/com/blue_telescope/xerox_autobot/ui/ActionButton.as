package com.blue_telescope.xerox_autobot.ui {
	import com.blue_telescope.xerox_autobot.assets.ActionButtonBkg;
	import com.blue_telescope.xerox_autobot.assets.ActionButtonLabel0;
	import com.blue_telescope.xerox_autobot.assets.ActionButtonLabel1;
	import com.blue_telescope.xerox_autobot.assets.ActionButtonLabel2;
	import com.blue_telescope.xerox_autobot.assets.ActionButtonLabel3;
	import com.blue_telescope.xerox_autobot.assets.ActionButtonLabel4;

	import flash.display.Sprite;
	import flash.utils.getDefinitionByName;

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class ActionButton extends Sprite 
	{	
//		private static const ACTION_LABEL0	:String = "Take me to my office";		
//		private static const ACTION_LABEL1	:String = "Try it with Xerox Autobot";	
//		private static const ACTION_LABEL2	:String = "Try the challenge again";	
//		private static const ACTION_LABEL3	:String = "Touch here to see an actual demonstration";	
//		private static const ACTION_LABEL4	:String = "Touch here to see a demonstration video";	
		
		public var targetY					:Number;
		
		private const inactiveAlpha			:Number = 0.5;
		private const activeAlpha			:Number = 1;
		
		private var _bkg					:ActionButtonBkg;
		private var _label					:Sprite;
		
		private var __active				:Boolean = false;
		public function set active(b:Boolean):void
		{
			if (__active != b) {
				__active = b;
				mouseEnabled = buttonMode = __active;
				_bkg.alpha = _label.alpha = (__active)? activeAlpha:inactiveAlpha;
			}
		}
		public function get active():Boolean
		{
			return __active;
		}
		
		private var __actionID:uint = 0;
		public function get actionID():uint
		{
			return __actionID;
		}
		
		private var __bkgWidth:Number = 0;
		public function set bkgWidth(n:Number):void
		{
			if (n > 40) {
				var middleWidth:Number = n - 40;
				_bkg.buttonMiddle.width = middleWidth;
				_bkg.buttonRight.x = _bkg.buttonMiddle.x + _bkg.buttonMiddle.width;
				if (_label.height > 40) _bkg.height = _label.height + 50; 
			}
		}
		

		public function ActionButton(pActionID:uint = 0, pActive:Boolean = true) 
		{
			var dummyClass:Array = [ActionButtonLabel0, ActionButtonLabel1, ActionButtonLabel2, ActionButtonLabel3, ActionButtonLabel4];
			dummyClass = null;
			__actionID = pActionID;
			__active = pActive;
			buttonMode = mouseEnabled = __active;
			mouseChildren = false;
	
			configUI();
		}

		private function configUI():void
		{
			_bkg = new ActionButtonBkg();
			addChild(_bkg);
			
			var assetClassName:String = "com.blue_telescope.xerox_autobot.assets.ActionButtonLabel" + __actionID;
			var ClassRef : Class = getDefinitionByName(assetClassName) as Class;
			_label = new ClassRef();
			_label.x = _bkg.buttonMiddle.x;
			_label.y = 18;
			addChild(_label);
			
			_bkg.buttonMiddle.width = Math.floor(_label.width + 7);
			_bkg.buttonRight.x = _bkg.buttonMiddle.x + _bkg.buttonMiddle.width;
			if (_label.height > 40) _bkg.height = _label.height + 50; 
			
			_bkg.alpha = (__active)? activeAlpha : inactiveAlpha;
		}
		
		public function destroy():void
		{
			if (_bkg) removeChild(_bkg);
			if (_label) removeChild(_label);
			_bkg = null;
			_label = null;
		}
	}//c
}//p