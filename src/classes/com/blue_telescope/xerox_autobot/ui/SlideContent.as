package com.blue_telescope.xerox_autobot.ui {
	import com.base.utils.TextUtils;
	import com.blue_telescope.xerox_autobot.assets.GreenBalloon;
	import com.blue_telescope.xerox_autobot.assets.TextBullet;
	import com.blue_telescope.xerox_autobot.data.SlideData;
	import com.blue_telescope.xerox_autobot.manager.StyleManager;

	import flash.display.Graphics;
	import flash.display.Loader;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class SlideContent extends Sprite 
	{
		private static const SINGLE_COL_TYPE	:String = "1";
		private static const DOUBLE_COL_PIC_TYPE:String = "2";
		private static const DOUBLE_COL_TYPE	:String = "3";
		
		private var _bkgBalloon					:GreenBalloon;
		private var _headlineField				:TextField;
		private var _contentHolder				:Sprite;
		private var _picHolder					:Loader;
		private var _picMask					:Shape;
		
		private var _headlineFormat				:TextFormat;
		private var _headlineFormat2			:TextFormat;
		private var _headerFormat				:TextFormat;
		private var _descFormat					:TextFormat;
		private var _descLgFormat					:TextFormat;
		
		private var _data						:SlideData;
		private var _styleManager				:StyleManager;
		
		
		public function SlideContent(pData:SlideData) 
		{
			_data = pData;
			_styleManager = StyleManager.getInstance();

			addEventListener(Event.ADDED_TO_STAGE, configUI);
		}

		private function configUI(e:Event):void
		{
			trace("INFO SlideContent: configUI");
			
			removeEventListener(Event.ADDED_TO_STAGE, configUI);
			TextUtils.traceFonts();
			
			_headlineFormat = StyleManager.getHeadlineFormat();
			_headlineFormat2 = StyleManager.getSmHeadlineFormat();
			_headerFormat = StyleManager.getHeaderFormat();
			_descFormat = StyleManager.getDescFormat();
			_descLgFormat = StyleManager.getDescLgFormat();
			
			_bkgBalloon = new GreenBalloon();
			_bkgBalloon.x = 148;
			_bkgBalloon.y = 64;
			addChild(_bkgBalloon);
			
			//-- createTextField(text : String, html : Boolean = false, multiLine : Boolean = false, wordWrap : Boolean = false, autoSize : String = null, selectable : Boolean = false, cssOrTextFormat : * = null, defaultCSS:String = "p")
			if ((_data.type == SINGLE_COL_TYPE)) {
				_headlineField = TextUtils.createTextField(_data.headline, true, true, true, "left", false, _headlineFormat);
			} else {
				_headlineField = TextUtils.createTextField(_data.headline, true, true, true, "left", false, _headlineFormat2);
			}
			_headlineField.width = 1530;
			_headlineField.styleSheet = _styleManager.styleSheet;
			_headlineField.htmlText = _data.headline;
			addChild(_headlineField);
			_headlineField.x = 210;
			_headlineField.y = (_headlineField.textHeight > 100)? 120:133;
			
			_contentHolder = new Sprite();
			addChild(_contentHolder);
			_contentHolder.x = (_data.type == DOUBLE_COL_PIC_TYPE)? 977:210;
			//_contentHolder.y = (_data.type == SINGLE_COL_TYPE)? 287:247; 
			_contentHolder.y = 250;	//통일!!
			
			buildContent();
		}
		
		private function buildContent():void
		{
			trace("INFO SlideContent: buildContent");
			
			var slideContentArr:Array = _data.slideContentArr;
			var contentTotal:uint = slideContentArr.length;
			var headerField:TextField;
			var descTotal:uint;
			var descField:TextField;
			var bullet:TextBullet;
			var nextContentX:Number = 0;
			var nextHeaderY:Number = 0;
			var nextDescY:Number;
			switch (_data.type) {
				case SINGLE_COL_TYPE:
					_headlineField.y = (_headlineField.textHeight > 100)? 120:133;
					//-- set content
					for (var i:uint = 0; i<contentTotal; i++) {
						headerField = TextUtils.createTextField(slideContentArr[i].header, true, true, true, "left", false, _headerFormat);
						headerField.width = 1500;
						//headerField.border = true;//for test
						headerField.y = nextHeaderY;
						headerField.styleSheet = _styleManager.styleSheet;
						headerField.htmlText = slideContentArr[i].header;
						_contentHolder.addChild(headerField);
						nextDescY = Math.round(nextHeaderY + headerField.textHeight + 7);
						descTotal = slideContentArr[i].descArr.length;
						if (descTotal>1) {
							nextDescY += 12;		//new, 05/21/14
							for (var i2:uint = 0; i2<descTotal; i2++) {
								bullet = new TextBullet();
								bullet.y = nextDescY;
								_contentHolder.addChild(bullet);
								
								descField = TextUtils.createTextField(slideContentArr[i].descArr[i2], true, true, true, "left", false, _descLgFormat);
								descField.x = 25;	//40+25 = 65
								descField.y = nextDescY;
								descField.width = 1350;
								descField.styleSheet = _styleManager.styleSheet;
								descField.htmlText = slideContentArr[i].descArr[i2];
								_contentHolder.addChild(descField);
								nextDescY += Math.round(descField.textHeight + 19);
							}
							nextHeaderY = Math.round(nextDescY + descField.textHeight + 20);
						} else {
							descField = TextUtils.createTextField(slideContentArr[i].descArr[0], true, true, true, "left", false, _descLgFormat);
							descField.y = nextDescY;
							descField.width = 1365;
							descField.styleSheet = _styleManager.styleSheet;
							descField.htmlText = slideContentArr[i].descArr[0];
							_contentHolder.addChild(descField);
							nextHeaderY = Math.round(nextDescY + descField.textHeight + 20);
						}
					}
					break;
				case DOUBLE_COL_PIC_TYPE:
					_headlineField.y = (_headlineField.textHeight > 100)? 104:120;
					//-- load an image
					_picHolder = new Loader();
					_picHolder.contentLoaderInfo.addEventListener(Event.COMPLETE, onPicLoaded);
					_picHolder.x = 210;
					_picHolder.y = _contentHolder.y + 5;
					_picHolder.alpha = 0;
					addChild(_picHolder);
					var picRequest:URLRequest = new URLRequest(_data.imgPath);
					_picHolder.load(picRequest);
					
					_picMask = new Shape();
					_picMask.x = _picHolder.x;
					_picMask.y = _picHolder.y;
					addChild(_picMask);
					_picHolder.mask = _picMask;
					
					//-- set content
					for (var j:uint = 0; j<contentTotal; j++) {
						headerField = TextUtils.createTextField(slideContentArr[j].header, true, true, true, "left", false, _headerFormat);
						headerField.width = 775;
						headerField.y = nextHeaderY;
						headerField.styleSheet = _styleManager.styleSheet;
						headerField.htmlText = slideContentArr[j].header;
//						headerField.border = true;	//for test
						_contentHolder.addChild(headerField);
						trace(slideContentArr[k].header + ", headerField.textHeight:" + headerField.textHeight);
						nextDescY = (headerField.textHeight < 10)? nextHeaderY : Math.round(nextHeaderY + headerField.textHeight + 7);
						descTotal = slideContentArr[j].descArr.length;
						if (descTotal>1) {
							for (var j2:uint = 0; j2<descTotal; j2++) {
								bullet = new TextBullet();
								bullet.y = nextDescY;
								_contentHolder.addChild(bullet);
								
								descField = TextUtils.createTextField(slideContentArr[j].descArr[j2], true, true, true, "left", false, _descFormat);
								descField.x = nextContentX + 25;
								descField.y = nextDescY;
								descField.width = 710;
//								descField.border = true;
								descField.styleSheet = _styleManager.styleSheet;
								descField.htmlText = slideContentArr[j].descArr[j2];
								_contentHolder.addChild(descField);
								nextDescY += Math.round(descField.textHeight + 17);
							}
							nextHeaderY = Math.round(nextDescY + descField.textHeight + 27);
						} else {
							descField = TextUtils.createTextField(slideContentArr[j].descArr[0], true, true, true, "left", false, _descFormat);
							descField.y = nextDescY;
							descField.width = 725;
//							descField.border = true;
							descField.styleSheet = _styleManager.styleSheet;
							descField.htmlText = slideContentArr[j].descArr[0];
							_contentHolder.addChild(descField);
							
							if (slideContentArr[j].descArr[0].substr(0,4) == "<img") {
//								trace("INFO SlideContent :: ever?? " + slideContentArr[j].descArr[0].substr(0,4));
								descField.x = -5;
							}
							
							nextHeaderY = Math.round(nextDescY + descField.textHeight + 27);
						}
					}
					break;
				case DOUBLE_COL_TYPE:
					_headlineField.y = (_headlineField.textHeight > 100)? 104:120;
					//-- set content
					//var leftContentHeight:Number;
					for (var k:uint = 0; k<contentTotal; k++) {
						headerField = TextUtils.createTextField(slideContentArr[k].header, true, true, true, "left", false, _headerFormat);
						headerField.width = 775;
						headerField.x = nextContentX;
						headerField.y = nextHeaderY;
//						headerField.border = true;	//for test
						headerField.styleSheet = _styleManager.styleSheet;
						headerField.htmlText = slideContentArr[k].header;
						_contentHolder.addChild(headerField);
						
						if ((nextHeaderY + headerField.textHeight)>510) {
							nextContentX = 778;
							nextHeaderY = 0;
							headerField.x = nextContentX;
							headerField.y = nextHeaderY;
						}
//						trace(slideContentArr[k].header + ", headerField.textHeight:" + headerField.textHeight);
						nextDescY = (headerField.textHeight < 10)? nextHeaderY : Math.round(nextHeaderY + headerField.textHeight + 15);
						descTotal = slideContentArr[k].descArr.length;
						if (descTotal>1) {
							for (var k2:uint = 0; k2<descTotal; k2++) {
								bullet = new TextBullet();
								bullet.x = nextContentX;
								bullet.y = nextDescY;
								_contentHolder.addChild(bullet);
								
								descField = TextUtils.createTextField(slideContentArr[k].descArr[k2], true, true, true, "left", false, _descFormat);
								descField.x = nextContentX + 25;
								descField.y = nextDescY;
								descField.width = 710;
								descField.styleSheet = _styleManager.styleSheet;
								descField.htmlText = slideContentArr[k].descArr[k2];
								_contentHolder.addChild(descField);
								nextDescY += Math.round(descField.textHeight + 17);
//								trace(slideContentArr[k].descArr[k2] + ", nextDescY: " + nextDescY);
								if (nextDescY > 510) {
									nextDescY = 0;
									nextContentX = 778;
								}
							}//for k2
							nextHeaderY = Math.round(nextDescY + descField.textHeight + 27);
						} else {
							descField = TextUtils.createTextField(slideContentArr[k].descArr[0], true, true, true, "left", false, _descFormat);
							descField.x = nextContentX;
							descField.y = nextDescY;
							descField.width = 725;
							descField.styleSheet = _styleManager.styleSheet;
							descField.htmlText = slideContentArr[k].descArr[0];
							_contentHolder.addChild(descField);
							nextHeaderY = Math.round(nextDescY + descField.textHeight + 27);
						}
//						trace(slideContentArr[k].descArr[0] + ", nextHeaderY: " + nextHeaderY);
						if (nextHeaderY > 510) {
							nextHeaderY = 0;
							nextContentX = 778;
						}
					}//for k
					break;
			}
		}
		
		public function destroy():void
		{
			removeChild(_bkgBalloon);
			removeChild(_headlineField);
			removeChild(_contentHolder);
			if (_picHolder) removeChild(_picHolder);
			if (_picMask) removeChild(_picMask);
			
			_bkgBalloon = null;
			_headlineField = null;
			_contentHolder = null;
		}
		
		private function onPicLoaded (e:Event):void
		{
//			trace("INFO onPicLoaded :: _picHolder.width:" + _picHolder.width);
//			trace("INFO onPicLoaded :: _picHolder.height:" + _picHolder.height);
			
			var w:Number = _picHolder.width;
			var h:Number = _picHolder.height;
			var pg:Graphics = _picMask.graphics;
			pg.clear();
			pg.beginFill(0xffffff);
			pg.drawRoundRect(0, 0, w, h, 60, 60);
			pg.endFill();
			
			_picHolder.alpha = 1;
		}
	}//c
}//p