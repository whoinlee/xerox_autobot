package com.blue_telescope.xerox_autobot.ui {
	import com.blue_telescope.xerox_autobot.assets.BubbleAsset;
	import com.greensock.TweenLite;

	import flash.display.MovieClip;
	import flash.display.Sprite;

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class TextBubble extends Sprite 
	{
		private var _asset					:BubbleAsset;
		private var _labelMC				:MovieClip;
		
		private var __taskID				:uint = 0;
		public function set taskID(n:uint):void
		{
			__taskID = n;
			update();
		}
		public function get taskID():uint
		{
			return __taskID;
		}
		
		private var __isMatching				:Boolean = false;
		public function set isMatching(b:Boolean):void
		{
			if (__isMatching != b) {
				__isMatching = b;
				update();
			}
		}
		
		public function get isVisible():Boolean
		{
			return (_asset.visible)
		}
		
		
		public function TextBubble(pTaskID:uint=0, pIsMatching:Boolean = false) 
		{
			__taskID = pTaskID;
			__isMatching = pIsMatching;
			configUI();
		}

		private function configUI():void
		{
			_asset = new BubbleAsset();
			addChild(_asset);
			hide();

			_labelMC = _asset.label;
			update();
		}
		
		private function update():void
		{
			//trace("INFO TextBubble :: update, __isMatching?" + __isMatching);
			
			if (__isMatching) {
				//-- is matching bubble w. matching text?
				var frameLabel:String = "label" + __taskID + "2";
				_labelMC.gotoAndStop(frameLabel);
			} else {
				var frameIndex:uint = __taskID + 1;
				_labelMC.gotoAndStop(frameIndex);
			}
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// PublicMethods
		////////////////////////////////////////////////////////////////////////////
		public function show():void
		{
			if (!_asset.visible) {
				_asset.scaleX = _asset.scaleY = 0;
				_asset.alpha = .2;
				_asset.visible = true;
				TweenLite.killTweensOf(_asset);
				TweenLite.to(_asset, .25, {alpha:1, scaleX:1, scaleY:1, ease:"Back.easeOut"});
			}
		}
		
		public function hide():void
		{
			if (_asset) _asset.visible = false;
		}
		
		public function destroy():void
		{
			if (_asset) removeChild(_asset);
			_asset = null;
			_labelMC = null;
		}
	}//c
}//p