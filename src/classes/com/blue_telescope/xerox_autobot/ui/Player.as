package com.blue_telescope.xerox_autobot.ui {
	import com.blue_telescope.xerox_autobot.assets.PaperAsset;
	import com.blue_telescope.xerox_autobot.assets.Player0;
	import com.blue_telescope.xerox_autobot.assets.Player1;
	import com.blue_telescope.xerox_autobot.assets.Player2;
	import com.blue_telescope.xerox_autobot.assets.Player3;
	import com.blue_telescope.xerox_autobot.assets.Player4;
	import com.blue_telescope.xerox_autobot.assets.Player5;
	import com.blue_telescope.xerox_autobot.assets.Player6;
	import com.blue_telescope.xerox_autobot.assets.Player7;
	import com.blue_telescope.xerox_autobot.assets.Player8;
	import com.blue_telescope.xerox_autobot.assets.Player9;
	import com.blue_telescope.xerox_autobot.data.DataModel;
	import com.blue_telescope.xerox_autobot.manager.SoundController;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.utils.clearInterval;
	import flash.utils.clearTimeout;
	import flash.utils.getDefinitionByName;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	[Event(name="showClue", type="flash.events.Event")]
	[Event(name="hideClue", type="flash.events.Event")]
	[Event(name="inactive", type="flash.events.Event")]
	[Event(name="active", type="flash.events.Event")]
	public class Player extends Sprite 
	{
		public static const NAV_H			:Number = 110;
		private static const inactiveDuration:Number = 3000;			//in miliseconds, 3sec
		
		public var orgX						:Number;
		public var orgY						:Number;		
		public var isTraining				:Boolean = false;
		public var isAlwaysActive			:Boolean = false;		//when the player is only one left, it shouldn't be deactivated
		
		private var _asset					:*;
		private var _puff					:MovieClip;
		private var _clueCircle				:ClueCircle;
		
		private var _inbox					:MovieClip;
		private var _taskArr				:Array = [];
		private var _taskHolder				:Sprite;
		
		private var _waitTillActiveID		:Number;				//-- inactive for 'inactiveDuration' right after a new task is assigned
		private var _waitTillWorkDoneID		:Number;				//-- takes '_singleTaskCompleteDuration' to complete a task
		private var _singleTaskCompleteDuration	:Number = 20000;	//-- in miliseconds, 10sec if not trained, 5sec after trained
		
		private var _soundController		:SoundController;
		
		private var __playerID				:uint = 0;	
		public function get playerID():uint
		{
			return __playerID;
		}
		
		private var _active					:Boolean = true;
		private var _isWorking				:Boolean = false;
		
		private var __isTrained				:Boolean = false;	
		public function set isTrained(b:Boolean):void
		{
			if (__isTrained != b) {
				__isTrained = b;
				_singleTaskCompleteDuration = (__isTrained)? 10000:20000;
			}
		}
		
		private var __isClueHidden			:Boolean = true;
		public function get isClueHidden()	:Boolean
		{
			return __isClueHidden;
		}
		
		public function get isTrained()		:Boolean
		{
			return __isTrained;
		}
		
		private var __numOfTasks			:uint = 0;
		public function get numOfTasks():uint
		{
			__numOfTasks = _taskArr.length;
			return __numOfTasks;
		}


		public function Player(pPlayerID:uint=0, pActive:Boolean=true) 
		{
			__playerID = pPlayerID;
			
			_singleTaskCompleteDuration = 20000;
			if (__playerID <= 1) {
				_singleTaskCompleteDuration = 12000;
			} else if (__playerID == 7) {
				_singleTaskCompleteDuration = 10000;
			}
			_active = pActive;
			_soundController = SoundController.getInstance();
			configUI();
		}

		private function configUI():void
		{
			var dummyClass:Array = [Player0, Player1, Player2, Player3, Player4, Player5, Player6, Player7, Player8, Player9];
			var assetClassName:String = "com.blue_telescope.xerox_autobot.assets.Player" + __playerID;
			var ClassRef : Class = getDefinitionByName(assetClassName) as Class;
			
			_asset = new ClassRef();
			addChild(_asset);
			
			_puff = _asset.smokeMC;
			_puff.alpha = .2;
			
			if (__playerID <= 7) {
				_inbox = _asset.inbox;
				_taskHolder = new Sprite();
				_taskHolder.x = _inbox.x;
				_taskHolder.y = _inbox.y;
				_asset.addChild(_taskHolder);
			}
			
			dummyClass = null;
		}
		
		private function buildClueCircle(taskID):void
		{
			_clueCircle = new ClueCircle(taskID);
			addChild(_clueCircle);
			
			_clueCircle.addEventListener("showClue", onShowClue);
			_clueCircle.addEventListener("hideClue", onHideClue);
			_clueCircle.visible = false;
		}
		
		private function destroyClueCircle():void
		{
			if (_clueCircle) {
				removeChild(_clueCircle);
				_clueCircle = null;
			}
		}
		
		public function inactivatePlayer():void
		{
//			trace("INFO Player :: inactivatePlayer, playerID:" + __playerID);
			_active = false;
			DataModel.getInstance().removeActivePlayer(__playerID);
			if (!isAlwaysActive && !isTraining && (__playerID != 7)) {
				dispatchEvent(new Event("inactive"));
			}
		}
		
		public function activatePlayer():void
		{
//			trace("INFO Player :: activatePlayer, playerID:" + __playerID);
			_active = true;
			DataModel.getInstance().addActivePlayer(__playerID);
			if (__playerID != 7)
			dispatchEvent(new Event("active"));
		}
		
		private function waitTillWorkDone():void
		{
			__numOfTasks = _taskArr.length;
			if (__numOfTasks>0) {
				var lastPaper:PaperAsset = _taskArr.pop();
				_taskHolder.removeChild(lastPaper);
			}

			if (__numOfTasks == 1) {
				_isWorking = false;
				clearInterval(_waitTillWorkDoneID);
			}
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Public Method
		////////////////////////////////////////////////////////////////////////////
		public function reset():void
		{
			isTraining = false;
			isTrained = false;
			
			clearInterval(_waitTillWorkDoneID);
			clearTimeout(_waitTillActiveID);
			
			_active = true;
			_isWorking = false;
			isAlwaysActive = false;
			_singleTaskCompleteDuration = 20000;
			if (__playerID <= 1) {
				_singleTaskCompleteDuration = 12000;
			} else if (__playerID == 7) {
				_singleTaskCompleteDuration = 10000;
			}
			
			activatePlayer();
			destroyClueCircle();
			
			var totalPaper:uint = _taskArr.length;
			for (var i:uint=0;i<totalPaper;i++) {
				var paper:PaperAsset = _taskArr[i];
				_taskHolder.removeChild(paper);
				paper = null;
			}
			_taskArr = [];
			__numOfTasks = 0;
		}
				
		public function addTask(isShowingPuff:Boolean = true):void
		{
			trace("INFO Player :: addTask, playerID:" + __playerID);
			
			if (__playerID >=8) return;
			
			if (isShowingPuff) showPuff();	//excludes autobot
			
			var nextY:Number = 0;
			var totalPaper:uint = _taskArr.length;
			if (totalPaper>0) {
				nextY = _taskArr[totalPaper-1].y - 10;
			} else if (!isTraining) {
				_isWorking = true;
				_waitTillWorkDoneID = setInterval(waitTillWorkDone, _singleTaskCompleteDuration);
			}
			var newPaper:PaperAsset = new PaperAsset();
			newPaper.y = nextY;
			_taskHolder.addChild(newPaper);
			_taskArr.push(newPaper);
			__numOfTasks = _taskArr.length;
			
			if (!isTraining && !isAlwaysActive) {
				inactivatePlayer();
				_waitTillActiveID = setTimeout(activatePlayer, inactiveDuration);
			}
		}
		
		public function removeTask():void
		{
//			trace("INFO Player :: removeTask, playerID:" + __playerID);
			if (__playerID >=8) return;	
			if (_taskArr.length > 0) {
				var paper:PaperAsset = _taskArr.pop();
				_taskHolder.removeChild(paper);
			}
		}
		
		public function showClue(taskID:uint, isShowingIcon:Boolean = true):void
		{
//			trace("INFO Player :: showClue, playerID:" + __playerID);
			if (_clueCircle == null) {
				buildClueCircle(taskID);
			}
			_clueCircle.taskID = taskID;

			if (!_clueCircle.visible || _clueCircle.alpha <.5) {
				TweenLite.killTweensOf(_clueCircle);
				_clueCircle.alpha = 0;
				_clueCircle.visible = true;
				_clueCircle.showIcon(); 
				if (!isShowingIcon) _clueCircle.hideIcon();
				TweenLite.to(_clueCircle, .5, {alpha:1, ease:Quad.easeOut}); 
				__isClueHidden = false;
			}
		}
		
		public function hideClue():void
		{
			if (_clueCircle) {
//				_clueCircle.hideTextBubble();
				TweenLite.killTweensOf(_clueCircle);
				TweenLite.to(_clueCircle, .3, {alpha:0, onComplete:function(){_clueCircle.visible = false;}});
				__isClueHidden = true;
			}
		}
		
		public function stopWorking():void
		{
//			trace("INFO Player :: stopWorking, player" + __playerID + " isWorking? " + _isWorking);
			
			//-- when sent to a training session, or ITHelp is given
			clearInterval(_waitTillWorkDoneID);
			clearTimeout(_waitTillActiveID);
			if (!isAlwaysActive) inactivatePlayer();
		}
		
		public function restartWorking():void
		{
//			trace("INFO Player :: restartWorking, player" + __playerID + " wasWorking? " + _isWorking);
			
			clearInterval(_waitTillWorkDoneID);
			if (_isWorking) {
				_waitTillWorkDoneID = setInterval(waitTillWorkDone, _singleTaskCompleteDuration);
			}
			activatePlayer();
		}		
		
		public function showPuff(soundVolume:Number = .4):void
		{
			_puff.gotoAndPlay(2);
			_soundController.playPoof(soundVolume);
		}

		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onShowClue(e:Event):void
		{
			//trace("INFO Player :: onTaskIconDownHanlder, dispatching showClue");
			__isClueHidden = false;
			dispatchEvent(e.clone());
		}
		
		private function onHideClue(e:Event):void
		{
			//trace("INFO Player :: onTaskIconDownHanlder, dispatching hideClue");
			__isClueHidden = true;
			dispatchEvent(e.clone());
		}
	}//c
}//p