package com.blue_telescope.xerox_autobot.ui {
	import com.blue_telescope.xerox_autobot.assets.TaskButton0;
	import com.blue_telescope.xerox_autobot.assets.TaskButton1;
	import com.blue_telescope.xerox_autobot.assets.TaskButton2;
	import com.blue_telescope.xerox_autobot.assets.TaskButton3;
	import com.blue_telescope.xerox_autobot.assets.TaskButton4;
	import com.blue_telescope.xerox_autobot.assets.TaskButton5;
	import com.blue_telescope.xerox_autobot.assets.TaskButton6;
	import com.blue_telescope.xerox_autobot.assets.TaskButton7;
	import com.blue_telescope.xerox_autobot.assets.TaskButton8;
	import com.blue_telescope.xerox_autobot.assets.TaskButton9;

	import flash.display.Sprite;
	import flash.utils.getDefinitionByName;

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class TaskButton extends Sprite 
	{
		public var orgX						:Number;
		public var orgY						:Number;
		
		private var _asset					:Sprite;
		
		private var _taskID					:uint = 0;	//0~9
		public function get taskID():uint
		{
			return _taskID;
		}

		private var __selected				:Boolean = false;
		public function set selected(b:Boolean):void
		{
			if (__selected != b) {
				__selected = b;
				update();
			}
		}
		public function get selected():Boolean
		{
			return __selected;
		}
		
		
		public function TaskButton(pTaskID:uint=0) 
		{
			//** listed item buttons for each category in the SelectorView **//
			_taskID = pTaskID;
			mouseChildren = false;
			configUI();
		}

		private function configUI():void
		{
			var dummyClass:Array = [TaskButton0, TaskButton1, TaskButton2, TaskButton3, TaskButton4, TaskButton5, TaskButton6, TaskButton7, TaskButton8, TaskButton9];
			var assetClassName:String = "com.blue_telescope.xerox_autobot.assets.TaskButton" + _taskID;
			var ClassRef : Class = getDefinitionByName(assetClassName) as Class;
				
			_asset = new ClassRef();
			addChild(_asset);
			
			dummyClass = null;
			update();
		}
		
		private function update():void
		{
			mouseEnabled = buttonMode = !__selected;
		}
		
		public function destroy():void
		{
			removeChild(_asset);
			_asset = null;
		}
	}//c
}//p