package com.blue_telescope.xerox_autobot.ui {
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;

	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class MetricBar extends Sprite 
	{
		public static const PRODUCTION		:uint = 0;
		public static const QUALITY			:uint = 1;
		public static const COST			:uint = 2;
		public static const FULL_W			:Number = 278;
		public static const BAR_H			:Number = 30;
		
		private static const METRIC_COLORS	:Array = [0x6DAF3D, 0x34BCBA, 0x2895D5];
		
		private var _asset					:Shape;
		private var _barMask				:Shape;
		private var _color					:Number = METRIC_COLORS[PRODUCTION];
		private var _metricID				:uint = PRODUCTION;
		private var _isBlinking				:Boolean = false;
		private var _assetTween				:TweenMax;
		
		private var __rate					:Number = 0;
		public function set rate(n:Number):void
		{
			if (__rate != n) {
				__rate = n;
				if (__rate>1) {
					__rate = 1;
				} else if (__rate<0) {
					__rate = 0;
				}
				
				if ((_metricID == COST) && (__rate<.1)) __rate = .1;	//-- added on 5/16/14
				
				update();
			}
		}
		public function get rate():Number
		{
			return __rate;
		}
		
		
		public function MetricBar(pID:uint = PRODUCTION, pRate:Number = 0) 
		{
			if (pID>0) {
				_metricID = pID;
				_color = METRIC_COLORS[_metricID];
			}
			__rate = pRate;
			
			configUI();
		}

		private function configUI():void
		{
			_asset = new Shape();
			addChild(_asset);
			var ag:Graphics = _asset.graphics;
			ag.beginFill(_color);
			ag.drawRect(0,0, FULL_W-10, BAR_H);
			ag.moveTo(FULL_W - 10, 0);	//triangle head
			ag.lineTo(FULL_W, BAR_H/2);
			ag.lineTo(FULL_W - 10, BAR_H);
			ag.lineTo(FULL_W - 10, 0);
			ag.endFill();
			
			_barMask = new Shape();
			addChild(_barMask);
			var mg:Graphics = _barMask.graphics;
			mg.beginFill(0xff0000);
			mg.drawRect(0,0, FULL_W, BAR_H);
			mg.endFill();
			
			_asset.mask = _barMask;
			
			update();
		}
		
		private function update():void
		{
			//trace("INFO MetricBar :: update, __rate? " + __rate);
			//trace("INFO MetricBar :: update, _metricID: " + _metricID);
			evaluate();
			var targetX:Number = -FULL_W + __rate*FULL_W;
			TweenLite.to(_asset, .25, {delay:.25, x:targetX});
		}
		
		private function evaluate():void
		{
			switch (_metricID) {
				case PRODUCTION:
				case QUALITY:
					if (__rate <= .3) {
						startBlinking();
					} else {
						stopBlinking();
					}
					break;
				case COST:
					if (__rate >= .79) {
						startBlinking();
					} else {
						stopBlinking();
					}
					break;
			}
		}
		
		private function startBlinking():void
		{
			if (!_isBlinking) {
//				trace("INFO MetricBar :: startBlinking, id: " + _metricID);
//				trace("INFO MetricBar :: startBlinking, rate: " + __rate);
				_isBlinking = true;
				_asset.alpha = 1;
				_assetTween = TweenMax.to(_asset, .4, {alpha:.2, repeat:-1, yoyo:true});
			}
		}
		
		public function stopBlinking():void
		{
			if (_isBlinking) {
//				trace("INFO MetricBar :: stopBlinking, id: " + _metricID);
//				trace("INFO MetricBar :: stopBlinking, rate: " + __rate);
				_isBlinking = false;
				if (_assetTween) _assetTween.pause();
				_asset.alpha = 1;
			}
		}
		
		public function destroy():void
		{
			trace("INFO MetricBar :: destroy!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			
			if (_barMask) removeChild(_barMask);
			if (_asset) removeChild(_asset);
			_barMask = null;
			_asset = null;
		}
	}//c
}//p