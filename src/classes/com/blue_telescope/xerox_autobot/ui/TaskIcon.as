package com.blue_telescope.xerox_autobot.ui {
	import com.blue_telescope.xerox_autobot.Configuration;
	import com.blue_telescope.xerox_autobot.assets.TaskIcon0;
	import com.blue_telescope.xerox_autobot.assets.TaskIcon1;
	import com.blue_telescope.xerox_autobot.assets.TaskIcon10;
	import com.blue_telescope.xerox_autobot.assets.TaskIcon2;
	import com.blue_telescope.xerox_autobot.assets.TaskIcon3;
	import com.blue_telescope.xerox_autobot.assets.TaskIcon4;
	import com.blue_telescope.xerox_autobot.assets.TaskIcon5;
	import com.blue_telescope.xerox_autobot.assets.TaskIcon6;
	import com.blue_telescope.xerox_autobot.assets.TaskIcon7;
	import com.blue_telescope.xerox_autobot.assets.TaskIcon8;
	import com.blue_telescope.xerox_autobot.assets.TaskIcon9;
	import com.greensock.TweenLite;
	import com.greensock.easing.Expo;
	import com.greensock.easing.Quint;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.utils.getDefinitionByName;

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	[Event(name="skip", type="flash.events.Event")]
	[Event(name="ok", type="flash.events.Event")]
	[Event(name="popupClosed", type="flash.events.Event")]
	public class TaskIcon extends Sprite 
	{
		public var loaded					:Boolean = false;
		public var targetX					:Number;
		public var targetY					:Number;
		public var isPopupTried				:Boolean = false;
		public var isPopupOpen				:Boolean = false;
		
		private var _asset					:MovieClip;
		private var _popup					:BasePopup;
		
		
		private var __taskID				:uint = 0;	
		public function get taskID():uint
		{
			return __taskID;
		}
		
		private var __active				:Boolean = false;
		public function set active(b:Boolean):void
		{
			//trace("INFO TaskIcon :: set active to " + b + ", taskID:" + __taskID);
			if (__active != b) {
				__active = b;
				update();
			}
		}
		public function get active():Boolean
		{
			return __active;
		}
		
		private var __isAutomated				:Boolean = false;
		public function set isAutomated(b:Boolean):void
		{
			if (__isAutomated != b) {
				__isAutomated = b;
				if (Configuration.isTesting) {
					if (__isAutomated) {
						_asset.gotoAndStop("auto");
					} else {
						_asset.gotoAndStop(1);
					}
				}
			}
		}
		public function get isAutomated():Boolean
		{
			return __isAutomated;
		}
		
		private var __isIcon					:Boolean = false;
		public function set isIcon(b:Boolean):void
		{
			if (b != __isIcon) {
				__isIcon = b;
				update();
			}
		}
		

		public function TaskIcon(pTaskID:uint=0, pActive:Boolean=false, pIsIcon:Boolean = false) 
		{	
			__taskID = pTaskID;
			__active = pActive;
			__isIcon = pIsIcon;
			
			configUI();
		}

		private function configUI():void
		{
			var dummyClass:Array = [TaskIcon0, TaskIcon1, TaskIcon2, TaskIcon3, TaskIcon4, TaskIcon5, TaskIcon6, TaskIcon7, TaskIcon8, TaskIcon9, TaskIcon10];
			var assetClassName:String = "com.blue_telescope.xerox_autobot.assets.TaskIcon" + __taskID;
			var ClassRef : Class = getDefinitionByName(assetClassName) as Class;
			
			_asset = new ClassRef();
			addChild(_asset);
			
			if (!Configuration.isTesting) _asset.order.visible = false;
			
			dummyClass = null;
			update();
		}
		
		private function update():void
		{
			var frameIndex:uint = (__active)? 2:1;
			_asset.gotoAndStop(frameIndex);
			
			if (!__isIcon) {
				_asset.alpha = 1;
			} else {
				_asset.alpha = (__active)? 1:.75;
			}
			
			if (_asset.order) _asset.order.visible = Configuration.isTesting;
			if (Configuration.isTesting && __isAutomated) _asset.gotoAndStop("auto");
		}
		
		private function buildPopup():void
		{
			_popup = new BasePopup(BasePopup.AUTOBOT_POPUP_ID, __taskID);
			_popup.x = -25;
			addChild(_popup);
			_popup.visible = false;
			
			_popup.addEventListener("skip", onSkipPressed);
			_popup.addEventListener("ok", onOKPressed);
		}
		
		private function destroyPopup():void
		{
			if (_popup) {
				_popup.removeEventListener("skip", onSkipPressed);
				_popup.removeEventListener("ok", onOKPressed);
				removeChild(_popup);
				_popup = null;
			}
			dispatchEvent(new Event("popupClosed"));
		}
		
		public function destroy():void
		{
			//trace("INFO TaskIcon :: destroy");
			
			if (_popup) {
				_popup.removeEventListener("skip", onSkipPressed);
				_popup.removeEventListener("ok", onOKPressed);
				removeChild(_popup);
				_popup = null;
			}
			if (_asset) {
				removeChild(_asset);
			}
			_asset = null;
		}
		
		public function goto(frameLabel:String):void
		{
			_asset.gotoAndStop(frameLabel);
		}
		
		public function openPopup():void
		{
//			trace("INFO TaskIcon :: openPopup");
			
			isPopupTried = true;
			isPopupOpen = true;
			
			buildPopup();
			
			_popup.alpha = 0;
			_popup.scaleX = _popup.scaleY = 0;
			_popup.visible = true;
			TweenLite.killTweensOf(_popup);
			TweenLite.to(_popup, .4, {alpha:1, scaleX:1, scaleY:1, ease:Quint.easeOut});
		}
		
		public function closePopup():void
		{
//			trace("INFO TaskIcon :: closePopup");
			
			if (_popup) {
				TweenLite.killTweensOf(_popup);
				TweenLite.to(_popup, .1, {delay:0, scaleY:0, scaleX:0, alpha:0, ease:Expo.easeOut, onComplete:destroyPopup});
				isPopupOpen = false;
			}
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// EventHandlers
		////////////////////////////////////////////////////////////////////////////
		private function onSkipPressed(e:Event):void 
		{
//			trace("INFO TaskIcon :: onSkipPressed");
			
			isAutomated = false;
			closePopup();
			dispatchEvent(e.clone());
		}
		
		private function onOKPressed(e:Event):void 
		{
//			trace("INFO TaskIcon :: onOKPressed");
			
			isAutomated = true;
			closePopup();
			dispatchEvent(e.clone());
		}
	}//c
}//p