package com.blue_telescope.xerox_autobot.manager {
	import com.blue_telescope.xerox_autobot.Configuration;
	import com.blue_telescope.xerox_autobot.Main;
	import com.blue_telescope.xerox_autobot.data.DataModel;
	import com.blue_telescope.xerox_autobot.ui.BasePopup;
	import com.blue_telescope.xerox_autobot.ui.MetricPanel;
	import com.blue_telescope.xerox_autobot.view.GameView;
	import com.greensock.TweenLite;
	import com.greensock.easing.Expo;
	import com.greensock.easing.Quint;

	import flash.display.Sprite;
	import flash.events.*;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class PopupManager extends EventDispatcher
	{
		private static var instance				:PopupManager;
		
		public static const APP					:String = "Main";
		public static const GAME_VIEW			:String = "GameView";
		public static const METRIC_VIEW			:String = "MetricPanel";
		
		public var popup						:BasePopup;
		
		//-- views
		private var _app						:Main; 			//-- reference to the application instance
		private var _views						:Object; 		//-- holds named list of view instances
		private var _gameView					:GameView;
//		private var _metricPanel				:MetricPanel;
		
		private var _isPopupOpen				:Boolean = false;
		public function get isPopupOpen()		:Boolean { return _isPopupOpen; }

		private var _dataModel					:DataModel;
		
		
		public function PopupManager(enforcer:SingletonEnforcer) 
		{
			//trace("INFO PopupManager :: dummy param enforcer is " + enforcer);
			enforcer = null;
			_views = new Object();
			_dataModel = DataModel.getInstance();
		}
		
		public static function getInstance():PopupManager
		{
			if (instance == null) {
				instance = new PopupManager(new SingletonEnforcer());
			}
			return instance;	
		}
		
		private function buildPopup(popupID:uint = 1, taskID:uint = 0):void
		{
//			trace("INFO PopupManager :: buildPopup");
			if (popup) {
				popup.promptID = popupID;
				popup.taskID = taskID;
			} else {
				popup = new BasePopup(popupID, taskID);	//add the popup when game view is being registered
				_gameView.addChild(popup);
			}
			popup.x = Configuration.STAGE_W/2;
			popup.y = 792;
			popup.visible = false;
			
			switch (popupID) {
				case BasePopup.TRAINING_POPUP_ID:	
					//1
					popup.addEventListener("skip", onNoTrainingRequested);
					popup.addEventListener("ok", onTrainingRequested);
					break;
				case BasePopup.IT_POPUP_ID:
					//2
					popup.addEventListener("skip", onNoITHelpRequested);
					popup.addEventListener("ok", onITHelpRequested);
					break;
				case BasePopup.NEWHIRE_POPUP_ID:
					//3
					popup.addEventListener("skip", onNoNewHireRequested);
					popup.addEventListener("ok", onNewHireRequested);
					break;
				case BasePopup.SECONDSHIFT_POPUP_ID:
					//4
					popup.addEventListener("skip", onNoSecondShiftRequested);
					popup.addEventListener("ok", onSecondShiftRequested);
					break;
				case BasePopup.TURNOVER_POPUP_ID:
					//5
					popup.addEventListener("ok", onTurnOverHappened);
					break;
				case BasePopup.SUMMARY_POPUP_ID:
					//7
					popup.y = 347;
					break;
			}
		}
		
		private function openPopup(popupID:uint = 1, taskID:uint = 0):void
		{
			_isPopupOpen = true;
			buildPopup(popupID, taskID);

			popup.scaleX = 0;
			popup.scaleY = 0;	
			popup.alpha = 0;
			popup.visible = true;
			TweenLite.killTweensOf(popup);
			TweenLite.to(popup, .4, {alpha:1, scaleX:1, scaleY:1, ease:Quint.easeOut});
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		public function registerApp ( app:* ):void
		{
			_app = app;
		}
		
		public function registerView ( viewName:String, view:Sprite ):void
		{
			_views [ viewName ] = view;	
			if (viewName == GAME_VIEW) {
				_gameView = view as GameView;
			} 
//			else if (viewName == METRIC_VIEW) {
//				_metricPanel = view as MetricPanel;
//			}
		}
		
		public function closePopup(e:Event = null):void
		{
			_isPopupOpen = false;
			if (popup) {
				TweenLite.killTweensOf(popup);
				TweenLite.to(popup, .25, {delay:.2, scaleY:0, alpha:0, ease:Expo.easeOut, onComplete:onPopupClosed});
			} 
		}
		
		private function onPopupClosed():void
		{
			if (popup) {
				popup.visible = false;
				popup.destroy();
				_gameView.removeChild(popup);
			}
			popup = null;
		}
	
		public function openTrainingPopup():void
		{
//			trace ("INFO PopupManager :: openTrainingPopup");
			
			openPopup(BasePopup.TRAINING_POPUP_ID);
		}
		
		public function openITPopup():void
		{
//			trace ("INFO PopupManager :: openITPopup");
			
			if (_isPopupOpen) {
				//-- previous Training popup is still open
				onNoTrainingRequested();
				TweenLite.delayedCall(.5, openPopup, [BasePopup.IT_POPUP_ID]);
			} else {
				openPopup(BasePopup.IT_POPUP_ID);
			}
		}
		
		public function openNewHirePopup():void
		{
			//trace ("INFO PopupManager :: openNewHirePopup");
			
			if (_isPopupOpen) {
				//-- previous IT popup is still open
				onNoITHelpRequested();
				TweenLite.delayedCall(.5, openPopup, [BasePopup.NEWHIRE_POPUP_ID]);
			} else {
				openPopup(BasePopup.NEWHIRE_POPUP_ID);
			}
		}
		
		public function openSecondShiftPopup():void
		{
			//trace ("INFO PopupManager :: openSecondShiftPopup");
			
			if (_isPopupOpen) {
				//-- previous NewHire popup is still open, then no need to open 2nd shift
				onNoNewHireRequested();
				TweenLite.delayedCall(.5, openPopup, [BasePopup.SECONDSHIFT_POPUP_ID]);
			} else {
				openPopup(BasePopup.SECONDSHIFT_POPUP_ID);
			}
		}
		
		public function openTurnoverPopup():void
		{
			//trace ("INFO PopupManager :: openTurnoverPopup");
			
			if (_isPopupOpen) {
				//-- previous SecondShift popup is still open
				onNoSecondShiftRequested();
				TweenLite.delayedCall(.5, openPopup, [BasePopup.TURNOVER_POPUP_ID]);
			} else {
				openPopup(BasePopup.TURNOVER_POPUP_ID);
			}
		}
		
		public function openSummaryPopup():void
		{
//			trace ("INFO PopupManager :: openSummaryPopup");
			openPopup(BasePopup.SUMMARY_POPUP_ID);
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onNoTrainingRequested(e:Event = null):void
		{
//			trace("INFO PopupManager :: onNoTrainingRequested");
			popup.removeEventListener("skip", onNoTrainingRequested);
			popup.removeEventListener("ok", onTrainingRequested);
			//_metricPanel.dropQuality();
			_gameView.onNoTrainingRequested();
			closePopup();
		}
		
		private function onTrainingRequested(e:Event):void
		{
//			trace("INFO PopupManager :: onTrainingRequested");
			popup.removeEventListener("skip", onNoTrainingRequested);
			popup.removeEventListener("ok", onTrainingRequested);
			_gameView.addMembersToTraining();
			closePopup();
		}
		
		private function onNoITHelpRequested(e:Event = null):void
		{
//			trace("INFO PopupManager :: onNoITHelpRequested");
			popup.removeEventListener("skip", onNoITHelpRequested);
			popup.removeEventListener("ok", onITHelpRequested);
//			_metricPanel.dropProduction();
//			_metricPanel.dropQuality();
			_gameView.onNoITHelpRequested();
			closePopup();
		}
		
		private function onITHelpRequested(e:Event):void
		{
//			trace("INFO PopupManager :: onITHelpRequested");
			popup.removeEventListener("skip", onNoITHelpRequested);
			popup.removeEventListener("ok", onITHelpRequested);
			_gameView.assignITMembersToOccupiedCubicles();
			closePopup();
		}
		
		private function onNoNewHireRequested(e:Event = null):void
		{
//			trace("INFO PopupManager :: onNoNewHireRequested");
			popup.removeEventListener("skip", onNoNewHireRequested);
			popup.removeEventListener("ok", onNewHireRequested);
//			_metricPanel.dropProduction();
//			_metricPanel.dropQuality();
//			_gameView.hideTrainees();
			_gameView.onNoNewHireRequested();
			closePopup();
		}
		
		private function onNewHireRequested(e:Event):void
		{
//			trace("INFO PopupManager :: onNewHireRequested");
			popup.removeEventListener("skip", onNoNewHireRequested);
			popup.removeEventListener("ok", onNewHireRequested);
//			_gameView.hideTrainees();
			_gameView.hireNewMembers();
			closePopup();
		}
		
		private function onNoSecondShiftRequested(e:Event = null):void
		{
//			trace("INFO PopupManager :: onNoSecondShiftRequested");
			popup.removeEventListener("skip", onNoSecondShiftRequested);
			popup.removeEventListener("ok", onSecondShiftRequested);
//			_metricPanel.dropProduction();
//			_metricPanel.dropQuality();
			_gameView.onNoSecondShiftRequested();
			closePopup();
		}
		
		private function onSecondShiftRequested(e:Event):void
		{
//			trace("INFO PopupManager :: onSecondShiftRequested");
			popup.removeEventListener("skip", onNoSecondShiftRequested);
			popup.removeEventListener("ok", onSecondShiftRequested);
			_gameView.startSecondShift();
			closePopup();
		}
		
		public function onTurnOverHappened(e:Event=null):void
		{
			trace("INFO PopupManager :: onTurnOverHappened, _isPopupOpen:" + _isPopupOpen);
			if (_isPopupOpen) {
				_isPopupOpen = false;
				popup.removeEventListener("ok", onTurnOverHappened);
				_gameView.whenTheBestEmployeeQuit();
				closePopup();
			}
		}
	}//c
}//p

class SingletonEnforcer {}