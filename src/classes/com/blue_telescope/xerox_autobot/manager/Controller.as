package com.blue_telescope.xerox_autobot.manager {
	import com.blue_telescope.xerox_autobot.data.DataModel;
	import com.blue_telescope.xerox_autobot.events.GameEvent;

	import flash.display.Sprite;
	import flash.events.*;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class Controller extends EventDispatcher
	{
		private static var instance				:Controller;
		
		//-- view name list
		public static const APP					:String = "Main";
		public static const GAME_VIEW			:String = "GameView";
		public static const SELECTOR_VIEW		:String = "SelectorView";
		public static const AUTOBOT_DEMO_VIEW	:String = "AutobotDemoView";
		public static const SLIDE_VIEW			:String = "SlideView";
		
		private var _app						:*; 			//-- reference to the application instance
		private var _views						:Object; 		//-- holds named list of view instances
		private var _dataModel					:DataModel;
		
		
		public function Controller(enforcer:SingletonEnforcer) 
		{
			trace("INFO Controller :: dummy param enforcer is " + enforcer);
			_views = new Object();
			_dataModel = DataModel.getInstance();
		}
		
		public static function getInstance():Controller
		{
			if (instance == null) {
				instance = new Controller(new SingletonEnforcer());
			}
			return instance;	
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		public function registerApp ( app:* ):void
		{
			_app = app;
		}
		
		public function registerView ( viewName:String, view:Sprite ):void
		{
			//trace ("INFO Controller :: registerView - viewName is " + viewName + ", view is " + view);
			
			_views [ viewName ] = view;
			switch (viewName) {
				case SELECTOR_VIEW:
					view.addEventListener(GameEvent.START_GAME, onStartGameRequested);
					break;
				case GAME_VIEW:
					view.addEventListener(GameEvent.START_DEMO, onStartDemoRequested);
					view.addEventListener(GameEvent.GAME_END, onGameEnd);
					view.addEventListener(GameEvent.AUTOBOT_START, onAutobotStart);
					view.addEventListener(GameEvent.RESTART_GAME, onRestartGameRequested);
					view.addEventListener(GameEvent.START_PRESENTATION, onStartPresentation);
					break;
				case AUTOBOT_DEMO_VIEW:
					view.addEventListener(GameEvent.DEMO_END, onDemoEnd);
					break;
				case SLIDE_VIEW:
					view.addEventListener(GameEvent.PRESENTATION_END, onPresentationEnd);
					break;
			}
		}
	
		public function getView ( viewName:String ) : Object
		{
			return (_views [ viewName ]);
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onStartGameRequested(e:GameEvent):void
		{
			//trace("INFO Controller :: onStartGameRequested ");
			_app.startGame();
		}

		private function onRestartGameRequested(e:GameEvent):void
		{
			//trace("INFO Controller :: onRestartGameRequested ");
			_app.showSelectorView();
		}
		
		private function onGameEnd(e:GameEvent):void
		{
			//trace("INFO Controller :: onGameEnd");
			_app.onGameEnd();
		}
		
		private function onAutobotStart(e:GameEvent):void
		{
			//trace("INFO Controller :: onAutobotStart");
			//-- reset timer
			_app.onAutobotStart();
		}
		
		private function onStartDemoRequested(e:GameEvent):void
		{
//			trace("INFO Controller :: onStartDemoRequested ");
			_app.showAutobotView();
		}
		
		private function onDemoEnd(e:GameEvent):void
		{
//			trace("INFO Controller :: onDemoEnd");
			_app.hideAutobotView();
		}
		
		private function onStartPresentation(e:GameEvent):void
		{
//			trace("INFO Controller :: onStartPresentation");
			_app.showSlideView();
		}
		
		private function onPresentationEnd(e:GameEvent):void
		{
//			trace("INFO Controller :: onPresentationEnd");
			_app.hideSlideView();
			_app.showSelectorView();
		}
	}//c
}//p

class SingletonEnforcer {}