package com.blue_telescope.xerox_autobot.manager {
	import com.blue_telescope.xerox_autobot.assets.PhoneSound;
	import com.blue_telescope.xerox_autobot.assets.PoofSound;
	import com.blue_telescope.xerox_autobot.assets.TickSound;
	import com.blue_telescope.xerox_autobot.assets.TypeSound1;
	import com.blue_telescope.xerox_autobot.assets.TypeSound2;
	import com.reintroducing.sound.SoundManager;

	import flash.utils.getDefinitionByName;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class SoundController
	{
		private static var instance				:SoundController;
		
		private var _sm							:SoundManager;
			
		
		public function SoundController(enforcer:SingletonEnforcer) 
		{
			trace("INFO SoundController :: dummy param enforcer is " + enforcer);
			var dummySoundClassArr:Array = [PhoneSound, PoofSound, TickSound, TypeSound1, TypeSound2];
			dummySoundClassArr = null;
		}
		
		public static function getInstance():SoundController
		{
			if (instance == null) {
				instance = new SoundController(new SingletonEnforcer());
				instance.init();
			}
			return instance;	
		}

		private function init():void
		{
//			trace ("INFO SoundController :: init");
			
			_sm = SoundManager.getInstance();
			
			var phoneSoundClassName:String = "com.blue_telescope.xerox_autobot.assets.PhoneSound";
			var phoneSClassRef:Class = getDefinitionByName(phoneSoundClassName) as Class;
			_sm.addLibrarySound(phoneSClassRef, "PHONE");
			
			var poofSoundClassName:String = "com.blue_telescope.xerox_autobot.assets.PoofSound";
			var poofSClassRef:Class = getDefinitionByName(poofSoundClassName) as Class;
			_sm.addLibrarySound(poofSClassRef, "POOF");
			
			var tickSoundClassName:String = "com.blue_telescope.xerox_autobot.assets.TickSound";
			var tickSClassRef:Class = getDefinitionByName(tickSoundClassName) as Class;
			_sm.addLibrarySound(tickSClassRef, "TICK");
			
			var typeSound1ClassName:String = "com.blue_telescope.xerox_autobot.assets.TypeSound1";
			var ts1ClassRef:Class = getDefinitionByName(typeSound1ClassName) as Class;
			_sm.addLibrarySound(ts1ClassRef, "TYPE1");
			
			var typeSound2ClassName:String = "com.blue_telescope.xerox_autobot.assets.TypeSound2";
			var ts2ClassRef:Class = getDefinitionByName(typeSound2ClassName) as Class;
			_sm.addLibrarySound(ts2ClassRef, "TYPE2");
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		public function playAmbience(volume:Number = .4):void
		{
			trace("INFO SoundController :: playAmbience");
			
//			playSound($name:String, $volume:Number = 1, $startTime:Number = 0, $loops:int = 0, $resumeTween:Boolean = true):void
//			_sm.playSound("TYPE1", 0, 0, 6, true);
//			_sm.fadeSound("TYPE1", volume, 2);
			_sm.playSound("TYPE1", volume, 0, 10, false);
		}
		
		public function stopAmbience():void
		{
			_sm.stopSound("TYPE1");
		}
		
		public function fadeOutAmbience(targetVolume:Number = 0):void
		{
//			trace("INFO SoundController :: fadeOutAmbience, to volume " + targetVolume);
			
			//--fadeSound($name:String, $targVolume:Number = 0, $fadeLength:Number = 1, $stopOnComplete:Boolean = false):void
			_sm.fadeSound("TYPE1", targetVolume, 2);
		}
		
		public function playTick(volume:Number = .3):void
		{	
			_sm.playSound("TICK", volume, 0, 21, false);	//.1 to .3
		}
		
		public function stopTick():void
		{
			_sm.stopSound("TICK");
		}
		
		public function playPhone(volume:Number = .2):void
		{
			_sm.playSound("PHONE", volume, 0, 0, true);		//.3 to .2
		}
		
		public function stopPhone():void
		{
			_sm.stopSound("PHONE");
		}
		
		public function fadeOutPhone(targetVolume:Number = 0):void
		{
//			trace("INFO SoundController :: fadeOutPhone, to volume " + targetVolume);
			
			_sm.fadeSound("PHONE", targetVolume, 1);
		}
		
		public function playTyping(volume:Number = .5):void
		{
			trace("INFO SoundController :: playTyping");
			
			_sm.playSound("TYPE2", volume, 0, 4, false);
		}
		
		public function stopTyping():void
		{
			_sm.stopSound("TYPE2");
		}
		
		public function playPoof(volume:Number = .6):void
		{
			_sm.playSound("POOF", volume, 0, 0, false);
		}
		
		public function setMute():void
		{
			_sm.muteAllSounds();
		}
		
		public function setUnmute():void
		{
			_sm.unmuteAllSounds();
		}
		
		public function stopAllSounds():void
		{
			_sm.stopAllSounds();
		}

	}//c
}//p

class SingletonEnforcer {}