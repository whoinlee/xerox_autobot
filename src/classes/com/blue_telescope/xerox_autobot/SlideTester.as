package com.blue_telescope.xerox_autobot {
	import com.blue_telescope.xerox_autobot.data.DataModel;
	import com.blue_telescope.xerox_autobot.manager.Controller;
	import com.blue_telescope.xerox_autobot.view.AutobotDemoView;
	import com.blue_telescope.xerox_autobot.view.GameView;
	import com.blue_telescope.xerox_autobot.view.SelectorView;
	import com.blue_telescope.xerox_autobot.view.SlideView;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;
	import com.greensock.easing.Quint;
	import com.greensock.plugins.GlowFilterPlugin;
	import com.greensock.plugins.TintPlugin;
	import com.greensock.plugins.TweenPlugin;

	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.media.Video;
	import flash.net.NetStream;
	import flash.system.fscommand;
	import flash.ui.Mouse;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class SlideTester extends Sprite {
		
		//-- views
		private var _slideView:SlideView;
		
		//-- singletons
		private var _controller:Controller;
		private var _dataModel:DataModel;
		

		public function SlideTester() {
			TweenPlugin.activate([GlowFilterPlugin, TintPlugin]);

			_controller = Controller.getInstance();
			_dataModel = DataModel.getInstance();
			//_controller.registerApp(this);
			
			_dataModel.getData(Configuration.CONFIG_PATH);
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.SHOW_ALL;	//for local test
			stage.showDefaultContextMenu = false;
			
			if (_dataModel.isDataReady) {
				init();
			} else {
				_dataModel.addEventListener(DataModel.DATA_READY, init);
			}
		}
		
		private function init(e:Event = null) : void 
		{
			if (Configuration.hideCursor) Mouse.hide();
			if (Configuration.fullScreen) fscommand("fullscreen","true");
			
			buildSlideView();
		}
		
		//-- slideView
		private function buildSlideView():void
		{
			destroySlideView();
			
			_slideView = new SlideView();
			addChild(_slideView);
		}
		
		private function destroySlideView():void
		{
			if (_slideView) {
				_slideView.visible = false;
				_slideView.destroy();
				removeChild(_slideView);
				_slideView = null;
			}
		}
	
		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		public function showSlideView():void
		{
			buildSlideView();
			
			_slideView.alpha = 0;
			_slideView.visible = true;
			TweenLite.to(_slideView, .75, {alpha:1, ease:Quad.easeOut});
		}
		
		public function hideSlideView():void
		{
			if (_slideView) {
				TweenLite.killTweensOf(_slideView);
				TweenLite.to(_slideView, .5, {alpha:0, ease:Quint.easeOut, onComplete:destroySlideView});
			}
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
	}
}