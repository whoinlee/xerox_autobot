package com.blue_telescope.xerox_autobot {

	public class Configuration  {

		public static const STAGE_W			:Number = 1920;
		public static const STAGE_H			:Number = 1080;
		public static const CONFIG_PATH		:String = "config/config.xml";
		public static const STYLE_PATH		:String = "config/style.css";
		
		public static var hideCursor 		:Boolean = false;
		public static var fullScreen 		:Boolean = true;
		public static var idleTimeout 		:Number = 120;
		
		public static var attractPath 		:String = "assets/video/attract.mp4";
		public static var autobotDemoPath 	:String = "assets/video/XeroxAutoBotDraft3.mp4";
		
		public static var isTesting			:Boolean = false;
		
	}//c
}//p