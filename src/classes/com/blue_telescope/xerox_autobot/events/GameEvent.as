package com.blue_telescope.xerox_autobot.events {
	import flash.events.*;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class GameEvent extends Event
	{
		public static const START_GAME					:String = "startGame";
		public static const DROP_INTERVAL				:String = "dropIconLoadingInterval";
		public static const ACTIVATE_AUTOBOT			:String = "activateAutobot";
		public static const GAME_END					:String = "gameEnd";
		public static const AUTOBOT_START				:String = "autobotStart";
		public static const START_DEMO					:String = "startDemo";
		public static const DEMO_END					:String = "demoEnd";
		public static const DEMO_CLOSED					:String = "demoClosed";
		public static const RESTART_GAME				:String = "restartGame";
		public static const START_PRESENTATION			:String = "startPresentation";
		public static const PRESENTATION_END			:String = "presentationEnd";
		
		
		public function GameEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false) {
			super(type, bubbles, cancelable);
		}

        public override function clone():Event {
            return new GameEvent(type);
        }
		
	}//c
}//p