package com.blue_telescope.xerox_autobot.data {

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class TaskData  {
		public static const CATEGORY0		:String = "GetInfo";								//read/get info
		public static const CATEGORY1		:String = "ProcessInfo";							//make decision/process info
		public static const CATEGORY2		:String = "UpdateInfo";								//write/update info
		public static const CATEGORY3		:String = "ShareInfo";								//report/share info	
		public static const CATEGORIES		:Array = [CATEGORY0, CATEGORY1, CATEGORY2, CATEGORY3];
		
		//-- category0
		public static const TASK0LABEL		:String = "EMAIL & DOCUMENTS";
		public static const TASK1LABEL		:String = "WEBSITES";
		public static const TASK2LABEL		:String = "BUSINESS APPLICATIONS";
		//-- category1
		public static const TASK3LABEL		:String = "COMBINING DOCUMENTS";
		public static const TASK4LABEL		:String = "CALCULATING DATA";
		public static const TASK5LABEL		:String = "ASSIGNING WORK";	
		//-- category2						
		public static const TASK6LABEL		:String = "BUSINESS APPLICATIONS";
		public static const TASK7LABEL		:String = "WEBSITES";
		public static const TASK8LABEL		:String = "EMAIL & DOCUMENTS";	
		//-- category3					
		public static const TASK9LABEL		:String = "GENERATING & SENDING REPORTS";			
		public static const TASK_LABELS		:Array = [TASK0LABEL, TASK1LABEL, TASK2LABEL, TASK3LABEL, TASK4LABEL, TASK5LABEL, TASK6LABEL, TASK7LABEL, TASK8LABEL, TASK9LABEL];
		public static const TASK_CATEGORIES	:Array = [0, 0, 0, 1, 1, 1, 2, 2, 2, 3];
		public static const TASK_ORDERS		:Array = [0, 1, 2, 6, 7, 8, 3, 4, 5, 9];
		
		//-- category0
		public static const BUBBLE0			:String = "Read email";
		public static const BUBBLE1			:String = "Get info from websites";
		public static const BUBBLE2			:String = "Get info from business applications";	
		//-- category1
		public static const BUBBLE3			:String = "Process info by combining documents";	//2nd step, MATCHING_BUBBLE3: "Read combined document"
		public static const BUBBLE4			:String = "Process info by calculating data";		//2nd step, MATCHING_BUBBLE4: "Make decision by referencing data"
		public static const BUBBLE5			:String = "Make decisions about assigning work";	//2nd step, MATCHING_BUBBLE5: "Assign work"
		//-- category2
		public static const BUBBLE6			:String = "Update info to business applications";
		public static const BUBBLE7			:String = "Write info to websites";			
		public static const BUBBLE8			:String = "Update documents";						
		//-- category3
		public static const BUBBLE9			:String = "Generate a report";						
		public static const TASK_BUBBLES	:Array = [BUBBLE0,BUBBLE1,BUBBLE2,BUBBLE3,BUBBLE4,BUBBLE5,BUBBLE6,BUBBLE7,BUBBLE8,BUBBLE9];
		
		//-- category1
		public static const MATCHING_BUBBLE3:String = "Read combined document";
		public static const MATCHING_BUBBLE4:String = "Make decision by referencing data";
		public static const MATCHING_BUBBLE5:String = "Assign work";
		//-- category3
		public static const MATCHING_BUBBLE9:String = "Send report";
		public static const MATCHING_BUBBLES:Array = ["","","",MATCHING_BUBBLE3,MATCHING_BUBBLE4,MATCHING_BUBBLE5,"","","",MATCHING_BUBBLE9];

		private var _taskID:uint = 0;
		private var _categoryID:uint = 0;
		private var _taskOrder:uint = 0;
		private var _taskLabel:String = "";
		private var _taskBubble:String = "";
		private var _matchingBubble:String = "";
		public var isCompleted:Boolean = false;
		
		public function get taskID():uint
		{
			return _taskID;
		}
		public function get categoryID():uint
		{
			return _categoryID;
		}
		public function get taskOrder():uint
		{
			return _taskOrder;
		}
		public function get taskLabel():String
		{
			return _taskLabel;
		}
		public function get taskBubble():String
		{
			return _taskBubble;
		}
		public function get matchingBubble():String
		{
			return _matchingBubble;
		}
		
		
		public function TaskData(pTaskIndex:uint)
		{
			_taskID = pTaskIndex;
			
			_categoryID = TASK_CATEGORIES[_taskID];
			_taskOrder = TASK_ORDERS[_taskID];
			_taskLabel = TASK_LABELS[_taskID];
			_taskBubble = TASK_BUBBLES[_taskID];
			_matchingBubble = MATCHING_BUBBLES[_taskID];
		}
		
		public static function getCategoryID(pTaskID:uint):uint
		{
			return TASK_CATEGORIES[pTaskID];
		}
		
		public static function getTaskOrder(pTaskID:uint):String
		{
			return TASK_ORDERS[pTaskID];
		}
		
		public static function getTaskLabel(pTaskID:uint):String
		{
			return TASK_LABELS[pTaskID];
		}
		
		public static function getTaskBubble(pTaskID:uint):String
		{
			return TASK_BUBBLES[pTaskID];
		}
		
		public static function getMatchingBubble(pTaskID:uint):String
		{
			return MATCHING_BUBBLES[pTaskID];
		}
	}//c
}//p