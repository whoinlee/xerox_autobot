package com.blue_telescope.xerox_autobot.data {

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class SlideData  {
		/*
		 <slide type="1">
			<headline><![CDATA[How do we identify potential opportunities in a and how do we potential opportunities do we identify potential?]]></headline>
			<img>assets/slides/xeroxPic.jpg</img>
			<body>
				<content>
					<header><![CDATA[Observe the workers]]></header>
					<descriptions>
						<desc><![CDATA[Ask questions]]></desc>
					</descriptions>
				</content>
				<content>
					<header><![CDATA[What do people really have to do?]]></header>
					<descriptions>
						<desc><![CDATA[OCR (Read an image or paper and transcribe it)]]></desc>
					</descriptions>
				</content>
				<content>
					<header><![CDATA[If we have workers using only data that is already in electronic form perform their processes, then we have a potential for automation.]]></header>
					<descriptions>
						<desc><![CDATA[Or is there a way to separate the "Human-Required" portion and add automation to the remainder?]]></desc>
						<desc><![CDATA["What does a human have to do?"]]></desc>
					</descriptions>
				</content>
			</body>
		 </slide>
		 */
		
		private var _type						:String = "1";	//-- "1":single column slide, "2":double column w. a pic, "3":double column w/o a pic
		public function get type()				:String
		{
			return _type;
		}
		
		private var _headline					:String = "Slide Headline Goes Here.";
		public function get headline()			:String
		{
			return _headline;
		}
		
		private var _imgPath					:String = "assets/slides/xeroxPic.jpg";
		public function get imgPath()			:String
		{	
			return _imgPath;
		}
		
		private var _slideContentArr			:Array = [];	//-- an array of SlideContentData
		public function get slideContentArr()	:Array
		{
			return _slideContentArr;
		}
		
		
		public function SlideData(slideItem:XML)
		{
			_type = slideItem.@type;
			_headline = slideItem.headline.toString();
			if (slideItem.img) _imgPath = slideItem.img.toString();
			_slideContentArr = [];
			var slideContentList:XMLList = slideItem.body.content;
			for each (var slideContent:XML in slideContentList) {
				var slideContentData:SlideContentData = new SlideContentData(slideContent);
				_slideContentArr.push(slideContentData);
			}
		}
	}//c
}//p