package com.blue_telescope.xerox_autobot.data {
	import com.blue_telescope.xerox_autobot.Configuration;
	import com.blue_telescope.xerox_autobot.ui.MetricPanel;

	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="dataReady", type="flash.events.Event")]
	public class DataModel extends EventDispatcher
	{
		private static var instance				:DataModel;
		public static const DATA_READY			:String = "dataReady";
		
		private var _dataLoader					:URLLoader;
		private var _isDataReady				:Boolean = false;
		public function get isDataReady():Boolean
		{
			return _isDataReady;
		}
		
		private var _selectedTaskDataArr		:Array = [];			//an array of TaskData for initially selected tasks from SelectorView
		public function get selectedTaskDataArr():Array
		{
			sortSelectedTasks();
			return _selectedTaskDataArr;
		}
		
		private var _activePlayerIDs			:Array = [1, 2, 3, 4];	//an array of active player's ID
		public function get activePlayerIDs():Array
		{
			return _activePlayerIDs;
		}

		private var _iconLoadInInterval			:Number;				
		public function get iconLoadInInterval():Number
		{
			return _iconLoadInInterval;
		}
		
		private var _iconIntervalOffset			:Number;				
		public function get iconIntervalOffset():Number
		{
			return _iconIntervalOffset;
		}
			
		private var _autobotPopupInterval		:Number;
		public function get autobotPopupInterval():Number
		{
			return _autobotPopupInterval;
		}
		
		private var _trainingPopupDelay			:uint;
		public function get trainingPopupDelay():uint
		{
			return _trainingPopupDelay;
		}
		
		private var _itHelpPopupDelay			:uint;
		public function get itHelpPopupDelay()	:uint
		{
			return _itHelpPopupDelay;
		}
		
		private var _newHiresDelay				:uint;
		public function get newHiresDelay()		:uint
		{
			return _newHiresDelay;
		}
		
		private var _secondShiftPopupDelay		:uint;
		public function get secondShiftPopupDelay():uint
		{
			return _secondShiftPopupDelay;
		}
		
		private var _turnoverPopupDelay			:uint;
		public function get turnoverPopupDelay():uint
		{
			return _turnoverPopupDelay;
		}
		
		private var _activateAutobotDelay		:uint;
		public function get activateAutobotDelay():uint
		{
			return _activateAutobotDelay;
		}

		private var _gameEndDelay				:uint;
		public function get gameEndDelay()		:uint
		{
			return _gameEndDelay;
		}
		
		private var _slideBkgColor				:Number = 0xdcefee;
		public function get slideBkgColor()		:Number
		{
			return _slideBkgColor;
		}
		
		private var _slideDataArr				:Array = [];
		public function get slideDataArr()		:Array
		{
			return _slideDataArr;
		}
		
		private var _productionRate				:Number = MetricPanel.INIT_PRODUCTION;
		private var _qualityRate				:Number = MetricPanel.INIT_QUALITY;
		private var _costRate					:Number = MetricPanel.INIT_COST;
		public function get productionRate()	:Number
		{
			return _productionRate;
		}
		public function get qualityRate()		:Number
		{
			return _qualityRate;
		}
		public function get costRate()			:Number
		{
			return _costRate;
		}
		
		private var _incompleteNumOfTasks		:uint;
		
		
		///////////////////////
		//-- constructor
		///////////////////////
		public function DataModel(enforcer:SingletonEnforcer)
		{
			trace("dummy " + enforcer);
		}
		
		public static function getInstance():DataModel 
		{
			if (DataModel.instance == null) {
				DataModel.instance = new DataModel(new SingletonEnforcer());
			}
			return DataModel.instance;
		}
		
		private function sortSelectedTasks():void
		{
			_selectedTaskDataArr.sortOn("taskOrder");
		}
		
		
		///////////////////////
		//-- public methods
		///////////////////////
		public function getData(path:String):void
		{	
			_dataLoader = new URLLoader();

			var urlReq:URLRequest = new URLRequest(path);
			_dataLoader.addEventListener(Event.COMPLETE, onDataLoaded);
			_dataLoader.load(urlReq);
		}
		
		public function setData(xml:XML):void
		{
			trace("INFO DataModel :: setData");	
				
			//--appVars
			Configuration.hideCursor = (xml.appVars.hideCursor.toString().toLowerCase() == "true")? true:false;
			Configuration.fullScreen = (xml.appVars.fullScreen.toString().toLowerCase() == "true")? true:false;
			Configuration.idleTimeout = Number(xml.appVars.idleTimeout) * 1000;
			Configuration.attractPath = xml.appVars.attractPath.toString();
			Configuration.autobotDemoPath = xml.appVars.autobotDemoPath.toString();
			
			//--gameVars 
			//-- in miliseconds
			_iconLoadInInterval = Number(xml.gameVars.intervals.iconLoadInInterval)*1000;
			_iconIntervalOffset = Number(xml.gameVars.intervals.intervalOffset)*1000;
			_autobotPopupInterval = Number(xml.gameVars.intervals.autobotPopupInterval)*1000;
			//-- in sec
			_trainingPopupDelay = Number(xml.gameVars.intervals.trainingPopup);
			_itHelpPopupDelay = Number(xml.gameVars.intervals.itHelpPopup);
			_newHiresDelay = Number(xml.gameVars.intervals.newHiresPopup);
			_secondShiftPopupDelay = Number(xml.gameVars.intervals.secondShiftPopup);
			_turnoverPopupDelay = Number(xml.gameVars.intervals.turnoverPopup);
			_activateAutobotDelay = Number(xml.gameVars.intervals.activateAutobot);
			_gameEndDelay = Number(xml.gameVars.intervals.gameEnd);
			
			//--slideVars
			_slideBkgColor = Number(xml.slides.@bkgColor);
			var slideList:XMLList = xml.slides.slide;
			_slideDataArr = [];
			for each (var slideItem:XML in slideList) {
				var slideData:SlideData = new SlideData(slideItem);
				_slideDataArr.push(slideData);
			}
		}
		
		public function reset():void
		{
			_selectedTaskDataArr = [];
			_activePlayerIDs =  [1, 2, 3, 4];
			
			_productionRate	= MetricPanel.INIT_PRODUCTION;
			_qualityRate = MetricPanel.INIT_QUALITY;
			_costRate = MetricPanel.INIT_COST;
		}
		
		public function addTask(taskID:uint):void
		{
			_selectedTaskDataArr.push(new TaskData(taskID));
		}
		
		public function addActivePlayer(playerID:uint):void
		{
			if (playerID>=7 || playerID==0) return;
			//if (playerID>=8) return;
			
			var total:uint = _activePlayerIDs.length;
			for (var i:uint=0; i<total; i++) {
				if (_activePlayerIDs[i] == playerID) return;
			}
			_activePlayerIDs.push(playerID);
		}
		
		public function removeActivePlayer(playerID:uint):void
		{
			if (playerID>=7 || playerID==0) return;
//			if (playerID>=8) return;
			
			var targetIndex:int = _activePlayerIDs.indexOf(playerID);
			if (targetIndex>=0) _activePlayerIDs.splice(targetIndex, 1);
		}
		
		public function decreaseIconLoadInInterval():void
		{
			_iconLoadInInterval	-= 	_iconIntervalOffset;
			if (_iconLoadInInterval < 1000) _iconLoadInInterval = 1000;	//-- 1sec minimum
		}
		
		public function setCompleted(taskID:uint, isSuccess:Boolean = true):void
		{
//			trace("INFO DataModel :: setCompleted, taskID:" + taskID);
			var total:uint = _selectedTaskDataArr.length;
			for (var i:uint = 0; i<total; i++) {
				if (taskID == _selectedTaskDataArr[i].taskID) {
					var taskData:TaskData = _selectedTaskDataArr[i];
					taskData.isCompleted = isSuccess;
					break;
				}
			}
		}
		
		public function setIncompleteTotal(n:uint):void
		{
			_incompleteNumOfTasks = n;
		}
		
		
		public function getIncompleteNumOfTasks():uint
		{
//			var total:uint = _selectedTaskDataArr.length;
//			var count:uint = 0;
//			for (var i:uint = 0; i<total; i++) {
//				var taskData:TaskData = _selectedTaskDataArr[i];
//				if (!taskData.isCompleted) {
//					count++;
//				}
//			}
//			//return count;
//			return total;
//			trace("totalTasks: " + _selectedTaskDataArr.length);
//			trace("_incompleteNumOfTasks: " + _incompleteNumOfTasks);
			return (_incompleteNumOfTasks);
		}
		
		public function getCompleteNumOfTasks():uint
		{
			var total:uint = _selectedTaskDataArr.length;
			var count:uint = 0;
			for (var i:uint = 0; i<total; i++) {
				var taskData:TaskData = _selectedTaskDataArr[i];
				if (taskData.isCompleted) {
					count++;
				}
			}
			return count;
		}
		
		public function getTotalNumOfTasks():uint
		{
			return _selectedTaskDataArr.length;
		}
		
		public function updateProduction(r:Number):void
		{
			_productionRate = r;
		}
		
		public function updateQuality(r:Number):void
		{
			_qualityRate = r;
		}
		
		public function updateCost(r:Number):void
		{
			_costRate = r;
		}
		
		public function getProductionEvaluation()	:String
		{
			//trace("INFO DataModel :: getProductionEvaluation, _productionRate is " + _productionRate);
			if (_productionRate<=.3) {
				return "LOW";
			} else if (_productionRate>=.69) {
				//-- not .7
				return "HIGH";
			} else {
				return "AVERAGE";
			}
		}
		public function getQualityEvaluation()		:String
		{
			//trace("INFO DataModel :: getQualityEvaluation, _qualityRate is " + _qualityRate);
			if (_qualityRate<=.3) {
				return "LOW";
			} else if (_qualityRate>=.69) {
				//-- not .7
				return "HIGH";
			} else {
				return "AVERAGE";
			}
		}
		public function getCostEvaluation()			:String
		{
			//trace("INFO DataModel :: getCostEvaluation, _costRate is " + _costRate);
			if (_costRate<=.4) {
				return "LOW";
			} else if (_costRate>=.79) {
				//-- not .8
				return "HIGH";
			} else {
				return "AVERAGE";
			}
		}
		
		public function getNumOfActiveTeamMembers()		:uint
		{
			var total:uint = _activePlayerIDs.length;
			//-- exclude the supervisor (top cubicle)
			if (_activePlayerIDs.indexOf(1) >= 0) total--;
			return total;
		}
		
		public function getNumActiveTeamMemberIDs()		:Array
		{
			var activeTeamMemberIDs:Array = _activePlayerIDs;
			
			//-- exclude manager, 0
			var targetIndex:int = activeTeamMemberIDs.indexOf(0);
			if (targetIndex>=0) activeTeamMemberIDs.splice(targetIndex, 1);
			//-- exclude supervisor, 1
			targetIndex = activeTeamMemberIDs.indexOf(1);
			if (targetIndex>=0) activeTeamMemberIDs.splice(targetIndex, 1);
			//-- exclude IT, 7
			targetIndex = activeTeamMemberIDs.indexOf(7);
			if (targetIndex>=0) activeTeamMemberIDs.splice(targetIndex, 1);
			
			return activeTeamMemberIDs;
		}
		
		
		///////////////////////
		//-- event handlers
		///////////////////////
		private function onDataLoaded(e:Event):void
		{
			_dataLoader.removeEventListener(Event.COMPLETE, onDataLoaded);
			
			var xml:XML = XML(_dataLoader.data);
			setData(xml);
			xml = null;
			_isDataReady = true;
			dispatchEvent(new Event(DATA_READY));
		}
	}//c
}//p

class SingletonEnforcer {}