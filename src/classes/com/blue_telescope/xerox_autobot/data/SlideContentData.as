package com.blue_telescope.xerox_autobot.data {

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class SlideContentData  {
		/*
		 <slide type="1">
			...
			<body>
				<content>
					<header><![CDATA[Observe the workers]]></header>
					<descriptions>
						<desc><![CDATA[Ask questions]]></desc>
					</descriptions>
				</content>
				...
				<content>
					<header><![CDATA[If we have workers using only data that is already in electronic form perform their processes, then we have a potential for automation.]]></header>
					<descriptions>
						<desc><![CDATA[Or is there a way to separate the "Human-Required" portion and add automation to the remainder?]]></desc>
						<desc><![CDATA["What does a human have to do?"]]></desc>
					</descriptions>
				</content>
			</body>
		 </slide>
		 */
		
		private var _header						:String = "Slide Content Header Goes Here.";
		public function get header()			:String
		{
			return _header;
		}
		
		private var _descArr					:Array = [];	//-- an array of desc data
		public function get descArr()			:Array
		{
			return _descArr;
		}
		
		
		public function SlideContentData(slideContentItem:XML)
		{
			_header = slideContentItem.header.toString();
			_descArr = [];
			var descList:XMLList = slideContentItem.descriptions.desc;
			for each (var descItem:XML in descList) {
				var descStr:String = descItem.toString();
//				trace("INFO SlideContentData descStr:" + descStr);
				_descArr.push(descStr);
			}
		}
	}//c
}//p