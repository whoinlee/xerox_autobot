package com.blue_telescope.xerox_autobot.data {

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class PlayerData  {
		
		public static const MANAGER_INDEX		:uint = 0;
		public static const SUPERVISOR_INDEX	:uint = 1;
		public static const MEMBER2_INDEX		:uint = 2;
		public static const MEMBER3_INDEX		:uint = 3;
		public static const MEMBER4_INDEX		:uint = 4;
		public static const NEWHIRE1_INDEX		:uint = 5;	
		public static const NEWHIRE2_INDEX		:uint = 6;	
		public static const SERVER_IT_INDEX		:uint = 7;	
		public static const INSTRUCTOR_INDEX	:uint = 8;	
		public static const AUTOBOT_INDEX		:uint = 9;	
		
		public static const PLAYER0				:String = "FemaleManager";
		public static const PLAYER1				:String = "MaleSupervisor";
		public static const PLAYER2				:String = "MaleMember2";
		public static const PLAYER3				:String = "FemaleMember3";
		public static const PLAYER4				:String = "FemaleMember4";
		public static const PLAYER5				:String = "NewFemaleMember5";		
		public static const PLAYER6				:String = "NewMaleMember6";				
		public static const PLAYER7				:String = "MaleIT";		
		public static const PLAYER8				:String = "MaleInstructor";		
		public static const PLAYER9				:String = "Autobot";
		public static const POSITIONS			:Array = [PLAYER0, PLAYER1, PLAYER2, PLAYER3, PLAYER4, PLAYER5, PLAYER6, PLAYER7, PLAYER8, PLAYER9];
		
		private var _playerID					:uint = 0;
		private var _position					:String = "FemaleManager";
		public function get playerID():uint
		{
			return _playerID;
		}
		public function get position():String
		{
			return _position;
		}
		
		
		public function PlayerData(pPlayerID:uint)
		{
			_playerID = pPlayerID;
			_position = POSITIONS[_playerID];
		}
//		
		public static function getPosition(pPlayerID:uint):String
		{
			return POSITIONS[pPlayerID];
		}
	}//c
}//p