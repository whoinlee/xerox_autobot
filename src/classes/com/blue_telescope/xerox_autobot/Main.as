package com.blue_telescope.xerox_autobot {
	import com.blue_telescope.xerox_autobot.data.DataModel;
	import com.blue_telescope.xerox_autobot.manager.Controller;
	import com.blue_telescope.xerox_autobot.manager.SoundController;
	import com.blue_telescope.xerox_autobot.view.AutobotDemoView;
	import com.blue_telescope.xerox_autobot.view.GameView;
	import com.blue_telescope.xerox_autobot.view.SelectorView;
	import com.blue_telescope.xerox_autobot.view.SlideView;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;
	import com.greensock.easing.Quint;
	import com.greensock.plugins.GlowFilterPlugin;
	import com.greensock.plugins.TintPlugin;
	import com.greensock.plugins.TweenPlugin;

	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.system.fscommand;
	import flash.ui.Mouse;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class Main extends Sprite {
		
		//-- views
		private var _gameView:GameView;
		private var _selectorView:SelectorView;
		private var _autobotView:AutobotDemoView;
		private var _slideView:SlideView;
		
		private var _attractLoop:Sprite;
		private var _video:Video;
		private var _ns:NetStream;
		private var _showAttractLoopID:Number;
		
		//-- singletons
		private var _controller:Controller;
		private var _dataModel:DataModel;
		private var _soundController:SoundController;
		

		public function Main() {
			TweenPlugin.activate([GlowFilterPlugin, TintPlugin]);

			_controller = Controller.getInstance();
			_dataModel = DataModel.getInstance();
			_controller.registerApp(this);
			
			_dataModel.getData(Configuration.CONFIG_PATH);
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.SHOW_ALL;	//for local test
			stage.showDefaultContextMenu = false;
			
			if (_dataModel.isDataReady) {
				init();
			} else {
				_dataModel.addEventListener(DataModel.DATA_READY, init);
			}
		}
		
		private function init(e:Event = null) : void 
		{
			if (Configuration.hideCursor) Mouse.hide();
			if (Configuration.fullScreen) fscommand("fullscreen","true");
			
			_soundController = SoundController.getInstance();
			
			buildGameView();
			buildAttractLoop();
		}
		
		//-- gameView
		private function buildGameView():void
		{
			_gameView = new GameView();
			addChild(_gameView);
			_gameView.visible = false;
		}
		
		private function resetGameView():void
		{
			trace("INFO Main :: resetGameView");
			
			_gameView.reset();
		}
		
		private function startGameView():void
		{
			clearTimeout(_showAttractLoopID);
			_gameView.removeEventListener(MouseEvent.MOUSE_MOVE, resetShowAttractLoopTimer);
			_gameView.startGame();
		}
		
		//-- selectorView
		private function buildSelectorView():void
		{
			destroySelectorView();
			
			_selectorView = new SelectorView();
			addChild(_selectorView);
			
			_selectorView.addEventListener(MouseEvent.MOUSE_MOVE, resetShowAttractLoopTimer);
			resetShowAttractLoopTimer();
		}
		
		private function destroySelectorView():void
		{
			clearTimeout(_showAttractLoopID);
			if (_selectorView) {
				_selectorView.removeEventListener(MouseEvent.MOUSE_MOVE, resetShowAttractLoopTimer);
				
				_selectorView.visible = false;
				_selectorView.destroy();
				removeChild(_selectorView);
				_selectorView = null;
			}
		}
		
		//-- autobotView
		private function buildAutobotView():void
		{
			clearTimeout(_showAttractLoopID);
			_gameView.removeEventListener(MouseEvent.MOUSE_MOVE, resetShowAttractLoopTimer);
			
			destroyAutobotView();
			_autobotView = new AutobotDemoView();
			_autobotView.x = Math.floor(Configuration.STAGE_W/2);
			_autobotView.y = Math.floor(Configuration.STAGE_H/2);
			addChild(_autobotView);
		}
		
		private function destroyAutobotView(e:Event = null):void
		{
			if (_autobotView) {
				_autobotView.destroy();
				removeChild(_autobotView);
				_autobotView = null;
			}
		}
		
		//-- attractLoop
		private function buildAttractLoop():void
		{
			destroyAttractLoop();
			
			_attractLoop = new Sprite();
			addChild(_attractLoop);
			_attractLoop.addEventListener(MouseEvent.MOUSE_MOVE, showSelectorView);
			
			_video = new Video();
			_video.width = Configuration.STAGE_W;
			_video.height = Configuration.STAGE_H;
			_attractLoop.addChild(_video);
			
			var nc:NetConnection = new NetConnection();
			nc.connect(null);	//-- load sound or video

			_ns = new NetStream(nc);
			_video.attachNetStream(_ns);
			var metaSniffer:Object = new Object();
			_ns.client = metaSniffer;
//			metaSniffer.onMetaData = getMetaData;
			metaSniffer.onPlayStatus = getPlayStatus;
			
			_ns.play(Configuration.attractPath);
		}
		
		private function destroyAttractLoop():void
		{
			clearTimeout(_showAttractLoopID);
			
			if (_attractLoop) {
				_attractLoop.removeEventListener(MouseEvent.MOUSE_MOVE, showSelectorView);
				
				if (_ns) _ns.pause();
				if (_video) _video.clear();	
				_attractLoop.removeChild(_video);
				_video = null;
				_ns = null;
				
				removeChild(_attractLoop);
				_attractLoop = null;
			}
		}
		
		//-- slideView
		private function buildSlideView():void
		{
			destroySlideView();
			
			_slideView = new SlideView();
			addChild(_slideView);
			
			_slideView.addEventListener(MouseEvent.MOUSE_MOVE, resetShowAttractLoopTimer);
			resetShowAttractLoopTimer();
		}
		
		private function destroySlideView():void
		{
			clearTimeout(_showAttractLoopID);
			if (_slideView) {
				_slideView.removeEventListener(MouseEvent.MOUSE_MOVE, resetShowAttractLoopTimer);
				
				_slideView.visible = false;
				_slideView.destroy();
				removeChild(_slideView);
				_slideView = null;
			}
		}
		
		private function onAutobotDemoHidden():void
		{
			destroyAutobotView();
			_gameView.addEventListener(MouseEvent.MOUSE_MOVE, resetShowAttractLoopTimer);
			resetShowAttractLoopTimer();
		}
	
		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		public function startGame():void
		{
			hideSelectorView();
			TweenLite.delayedCall(.5, startGameView);
		}
		
		public function showSelectorView(e:MouseEvent = null):void
		{
			trace("INFO Main :: showSelectorView");
			
			resetGameView();
			hideAttractLoop();
			buildSelectorView();
			
			_selectorView.alpha = 0;
			_selectorView.visible = true;
			TweenLite.to(_selectorView, .75, {alpha:1, ease:Quad.easeOut});
		}
		
		public function hideSelectorView():void
		{
			clearTimeout(_showAttractLoopID);
			if (_selectorView) {
				_selectorView.removeEventListener(MouseEvent.MOUSE_MOVE, resetShowAttractLoopTimer);
				TweenLite.killTweensOf(_selectorView);
				TweenLite.to(_selectorView, .5, {alpha:0, ease:Quint.easeOut, onComplete:destroySelectorView});
			}
		}
		
		public function showAttractLoop(pDelay:Number = 0):void
		{
			trace("INFO Main :: showAttractLoop");
			
			clearTimeout(_showAttractLoopID);
			
			_soundController.stopAllSounds();
			
			buildAttractLoop();
			_attractLoop.alpha = 0;
			_attractLoop.visible = true;
			TweenLite.to(_attractLoop, .75, {delay:pDelay, alpha:1, ease:Quad.easeOut, onComplete:resetGameView});
			
			destroySelectorView();
			destroySlideView();
			_gameView.removeEventListener(MouseEvent.MOUSE_MOVE, resetShowAttractLoopTimer);
		}
		
		public function hideAttractLoop():void
		{
			_gameView.visible = true;
			if (_attractLoop) {
				_ns.pause();
				_attractLoop.removeEventListener(MouseEvent.MOUSE_MOVE, showSelectorView);
				TweenLite.killTweensOf(_attractLoop);
				TweenLite.to(_attractLoop, .5, {alpha:0, ease:Quad.easeOut, onComplete:destroyAttractLoop});
			}
		}
		
		public function showAutobotView():void
		{
//			trace("INFO Main :: showAutobotView");

			_soundController = SoundController.getInstance();
			_soundController.stopAllSounds();
			
			_gameView.removeEventListener(MouseEvent.MOUSE_MOVE, resetShowAttractLoopTimer);
			clearTimeout(_showAttractLoopID);
			
			buildAutobotView();
			_autobotView.alpha = 0;
			_autobotView.scaleX = _autobotView.scaleY = 0;
			_autobotView.visible = true;
			TweenLite.to(_autobotView, .5, {alpha:1, width:Configuration.STAGE_W, height:Configuration.STAGE_H, ease:Quad.easeOut});
		}
	
		public function hideAutobotView():void
		{
//			trace("INFO Main :: hideAutobotView");
			_gameView.endAutobotVideo();
			TweenLite.to(_autobotView, .5, {alpha:-.5, scaleY:0, ease:Quint.easeOut, onComplete:onAutobotDemoHidden});
		}
		
		public function showSlideView():void
		{
			buildSlideView();
			
			_slideView.alpha = 0;
			_slideView.visible = true;
			TweenLite.to(_slideView, .75, {alpha:1, ease:Quad.easeOut});
		}
		
		public function hideSlideView():void
		{
			if (_slideView) {
				_slideView.removeEventListener(MouseEvent.MOUSE_MOVE, resetShowAttractLoopTimer);
				clearTimeout(_showAttractLoopID);
				
				TweenLite.killTweensOf(_slideView);
				TweenLite.to(_slideView, .5, {alpha:0, ease:Quint.easeOut, onComplete:destroySlideView});
			}
		}
		
		public function onGameEnd():void
		{
			_gameView.addEventListener(MouseEvent.MOUSE_MOVE, resetShowAttractLoopTimer);
			resetShowAttractLoopTimer();
		}
		
		public function onAutobotStart():void
		{
			clearTimeout(_showAttractLoopID);
			_gameView.removeEventListener(MouseEvent.MOUSE_MOVE, resetShowAttractLoopTimer);
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function resetShowAttractLoopTimer(e:MouseEvent = null):void
		{
			//trace("INFO AutobotDemoView :: resetShowAttractLoopTimer");
			clearTimeout(_showAttractLoopID);
			_showAttractLoopID = setTimeout(showAttractLoop, Configuration.idleTimeout);
		}
		
//		private function getMetaData(mdata:Object):void
//		{
//			trace("INFO AutobotDemoView :: getMetaData"); 
//			for (var prop in mdata) {
//				trace("prop1:" + prop);
//				trace("mdata[prop]:" + mdata[prop]);
//			}
//		}
		
		private function getPlayStatus(pstatus:Object):void
		{ 
			trace("INFO AutobotDemoView :: getPlayStatus");
//			for (var prop in pstatus) {
//				trace("prop:" + prop);
//				trace("pstatus[prop]:" + pstatus[prop]);
//			}
			
			if (pstatus.code == "NetStream.Play.Complete") {
				_ns.play(Configuration.attractPath, 0);
			}
		}
	}
}