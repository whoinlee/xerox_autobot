package com.blue_telescope.xerox_autobot.view {
	import com.blue_telescope.xerox_autobot.Configuration;
	import com.blue_telescope.xerox_autobot.events.GameEvent;
	import com.blue_telescope.xerox_autobot.manager.Controller;
	import com.blue_telescope.xerox_autobot.manager.SoundController;

	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="demoEnd", type="com.blue_telescope.xerox_autobot.events.GameEvent")]
	public class AutobotDemoView extends BaseView 
	{
		private var _video:Video;
		private var _ns:NetStream;
		
		
		public function AutobotDemoView() {
			super();
			//trace("INFO AutobotView :: ");
		}
		
		override protected function configUI() : void 
		{
			_controller.registerView(Controller.AUTOBOT_DEMO_VIEW, this);
			
			_video = new Video();
			addChild(_video);
			
			var nc:NetConnection = new NetConnection();
			nc.connect(null);	//-- load sound or video

			_ns = new NetStream(nc);
			_video.attachNetStream(_ns);
			var metaSniffer:Object = new Object();
			_ns.client = metaSniffer;
			metaSniffer.onMetaData = getMetaData;
			metaSniffer.onPlayStatus = getPlayStatus;
			
			_ns.play(Configuration.autobotDemoPath);
		}

		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		override public function destroy():void
		{
			stop();
			removeChild(_video);
			_ns = null;
			_video = null;
		}
		
		public function play():void
		{
			_ns.play();
		}
		
		public function stop():void
		{
			_ns.pause();
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// EventHandlers
		////////////////////////////////////////////////////////////////////////////
		private function getMetaData(mdata:Object):void
		{
			trace("INFO AutobotDemoView :: getMetaData"); 
			_video.width = mdata.width;
			_video.height = mdata.height;
			_video.x = -_video.width/2;
			_video.y = -_video.height/2;
//			for (var prop in mdata) {
//				trace("prop1:" + prop);
//				trace("mdata[prop]:" + mdata[prop]);
//			}
		}
		
		private function getPlayStatus(pstatus:Object):void
		{ 
//			for (var prop in pstatus) {
//				trace("prop:" + prop);
//				trace("pstatus[prop]:" + pstatus[prop]);
//			}
			
			if (pstatus.code == "NetStream.Play.Complete") {
				trace("INFO AutobotDemoView :: getPlayStatus, " + pstatus.code);
				dispatchEvent(new GameEvent(GameEvent.DEMO_END));
				stop();
			}
		}
	}
}