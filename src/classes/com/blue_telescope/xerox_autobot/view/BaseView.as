package com.blue_telescope.xerox_autobot.view {
	import com.blue_telescope.xerox_autobot.data.DataModel;
	import com.blue_telescope.xerox_autobot.manager.Controller;

	import flash.display.Sprite;
	import flash.events.Event;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class BaseView extends Sprite {
			
		//-- singletons
		protected var _controller:Controller;
		protected var _dataModel:DataModel;
		
		
		public function BaseView() {
//			trace("INFO BaseView :: ");

			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			_controller = Controller.getInstance();
			_dataModel = DataModel.getInstance();
		}
		
		private function onAddedToStage(e : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			configUI();
		}
		
		protected function configUI() : void {}

		public function build() : void {}
		
		public function destroy() : void {}
	}//c
}//p