package com.blue_telescope.xerox_autobot.view {
	import com.blue_telescope.xerox_autobot.assets.SelectorViewBkg;
	import com.blue_telescope.xerox_autobot.data.TaskData;
	import com.blue_telescope.xerox_autobot.events.GameEvent;
	import com.blue_telescope.xerox_autobot.manager.Controller;
	import com.blue_telescope.xerox_autobot.ui.ActionButton;
	import com.blue_telescope.xerox_autobot.ui.TaskButton;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quint;

	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="startGame", type="com.blue_telescope.xerox_autobot.events.GameEvent")]
	public class SelectorView extends BaseView {
		
		private static const INIT_ICON_X	:Number = 89;
		private static const INIT_ICON_Y	:Number = 204;
		private static const ICON_W			:Number = 80;
		private static const ICON_OFFSET_X	:Number = 4;
		
		private static const INIT_BUTTON_X	:Number = 115;
		private static const INIT_BUTTON_Y	:Number = 520;
		private static const BUTTON_OFFSET_X:Number = 452;
		private static const BUTTON_OFFSET_Y:Number = 140;
		
		private static const TOTAL			:uint = 10;
		
		private var _bkg:SelectorViewBkg;
		private var _taskButtons:Array = [];
		private var _actionButton:ActionButton;		//"Take me to my office"
		
		private var _selectedButton:TaskButton;
		private var _selectedButtonArr:Array = [];
		private var _isActionActive:Boolean = false;
		
		
		public function SelectorView() {
			super();
		}
		
		override protected function configUI() : void 
		{
			_controller.registerView(Controller.SELECTOR_VIEW, this);
			_dataModel.reset();
			
			//-- _bkg
			_bkg = new SelectorViewBkg();
			addChild(_bkg);
			_bkg.bkg.mouseEnabled = false;
			
			//-- build actionButton
			_actionButton = new ActionButton(0, _isActionActive);
			addChild(_actionButton);
			_actionButton.x = 810;	//origLoc: 804
			_actionButton.y = 973;	//973 + 140
			
			//-- _taskButtons
			for (var i:uint=0; i<TOTAL; i++) {
				var taskButton:TaskButton = new TaskButton(i);
				var catID:uint = TaskData.getCategoryID(i);
				taskButton.x = taskButton.orgX = INIT_BUTTON_X + catID*BUTTON_OFFSET_X;
				taskButton.y = taskButton.orgY = INIT_BUTTON_Y + (i%3)*BUTTON_OFFSET_Y;
				if (i == 9) {
					taskButton.y = taskButton.orgY = 602;
				}
				addChild(taskButton);
				_taskButtons.push(taskButton);
			}
			
			reset();
		}
		
		private function reset():void
		{
			_isActionActive = false;
			_actionButton.active = false;
			_selectedButtonArr = [];
			
			//-- reset taskButton location & eventListeners
			for (var i:uint=0; i<TOTAL; i++) {
				var taskButton:TaskButton = _taskButtons[i];
				taskButton.x = taskButton.orgX;
				taskButton.y = taskButton.orgY;
				taskButton.visible = true;
				taskButton.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
				taskButton.addEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
			}
			
			_bkg.taskBar.label.visible = true;
		}
		
		private function activateAction():void
		{
			_isActionActive = true;
			_actionButton.active = true;
			_actionButton.addEventListener(MouseEvent.CLICK, onActionRequested);
		}
		
		private function backTo(targetX:Number, targetY:Number):void
		{
			TweenLite.to(_selectedButton, .4, {x:targetX, y:targetY, ease:Quint.easeOut});
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		override public function destroy():void
		{
			for (var i:uint=0; i<TOTAL; i++) {
				var taskButton:TaskButton = _taskButtons[i];
				taskButton.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
				taskButton.removeEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
				removeChild(taskButton);
			}

			if (_bkg) removeChild(_bkg);
			if (_actionButton) {
				removeChild(_actionButton);
				_actionButton.removeEventListener(MouseEvent.CLICK, onActionRequested);
			}
			
			_taskButtons = null;
			_bkg = null;
			_actionButton = null;
			
			_selectedButton = null;
			_selectedButtonArr = null;
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// EventHandlers
		////////////////////////////////////////////////////////////////////////////
		private function onActionRequested(e:MouseEvent = null):void
		{
			_actionButton.removeEventListener(MouseEvent.CLICK, onActionRequested);
			dispatchEvent(new GameEvent(GameEvent.START_GAME));
		}
		
		private function onMouseDownHandler(e:MouseEvent):void
		{
			var lastButton:TaskButton = _selectedButton;
			_selectedButton = e.target as TaskButton;
			var targetX:Number = stage.mouseX - 40;
			var targetY:Number = stage.mouseY - 37;
			_selectedButton.x = targetX;
			_selectedButton.y = targetY;
			
			_selectedButton.startDrag(false, new Rectangle(0, 0, _bkg.width, _bkg.height));
			if (lastButton) {
				//-- let the current selection to be the top
				if (getChildIndex(lastButton) > getChildIndex(_selectedButton)) swapChildren(lastButton, _selectedButton);
			}
		}
		
		private function onMouseUpHandler(e:MouseEvent):void
		{	
			_selectedButton.stopDrag();
			if (_selectedButton.hitTestObject(_bkg.taskBar)) {	
				if (_selectedButtonArr.length==0) _bkg.taskBar.label.visible = false;
				_selectedButton.selected = true;
				var targetX:Number = INIT_ICON_X + _selectedButtonArr.length * (ICON_W + ICON_OFFSET_X);
				_selectedButtonArr.push(_selectedButton);
				if ((_selectedButtonArr.length>=5) && !_isActionActive) activateAction();
				
				if (_selectedButtonArr.length < TOTAL) {
					TweenLite.to(_selectedButton, .25, {x:targetX, y:INIT_ICON_Y, ease:Quint.easeOut});
				} else {
					TweenLite.to(_selectedButton, .25, {x:targetX, y:INIT_ICON_Y, ease:Quint.easeOut, onComplete:onActionRequested});
				}
				_dataModel.addTask(_selectedButton.taskID);
			} else {
				backTo(_selectedButton.orgX, _selectedButton.orgY);
			}			
		}
		
//		private function onBoxMaskAnimated(e:Event = null):void
//		{
//			trace("INFO SelectorView :: onBoxMaskAnimated");
//			
//			reset();
//			
//			//hide t0 to t9
//			_bkg.t0.visible = false;
//			_bkg.t1.visible = false;
//			_bkg.t2.visible = false;
//			_bkg.t3.visible = false;
//			_bkg.t4.visible = false;
//			_bkg.t5.visible = false;
//			_bkg.t6.visible = false;
//			_bkg.t7.visible = false;
//			_bkg.t8.visible = false;
//			_bkg.t9.visible = false;
//			
//			//tween action button
//			TweenLite.to(_actionButton, .75, {y:973, ease:Quad.easeOut});
//		}
	}//c
}//p