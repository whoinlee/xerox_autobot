package com.blue_telescope.xerox_autobot.view {
	import com.blue_telescope.xerox_autobot.Configuration;
	import com.blue_telescope.xerox_autobot.assets.ExitButton;
	import com.blue_telescope.xerox_autobot.assets.NextButton;
	import com.blue_telescope.xerox_autobot.assets.PrevButton;
	import com.blue_telescope.xerox_autobot.assets.XeroxLogo;
	import com.blue_telescope.xerox_autobot.events.GameEvent;
	import com.blue_telescope.xerox_autobot.manager.Controller;
	import com.blue_telescope.xerox_autobot.manager.StyleManager;
	import com.blue_telescope.xerox_autobot.ui.SlideContent;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;

	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class SlideView extends BaseView {
			
		private var _bkg:Shape;
		private var _slideHolder:Sprite;
		private var _logo:MovieClip;
		private var _prevBt:Sprite;
		private var _nextBt:Sprite;
		private var _finishBt:Sprite;
		
		private var _totalSlide:uint;
		private var _currentSlideNum:uint = 0;
		private var _slideContentArr:Array = [];
		private var _slideDataArr:Array = [];
		
		private var _styleManager:StyleManager;
		
		
		public function SlideView() {
			super();
			//trace("INFO SlideView :: ");
		}
		
		override protected function configUI() : void 
		{
			//trace("INFO SlideView :: configUI");
			
			_controller.registerView(Controller.SLIDE_VIEW, this);
			_styleManager = StyleManager.getInstance();
			
			
			var bkgColor:Number = _dataModel.slideBkgColor;
			
			_bkg = new Shape();
			var bg:Graphics = _bkg.graphics;
			bg.beginFill(bkgColor);
			bg.drawRect(0,0, Configuration.STAGE_W, Configuration.STAGE_H);
			bg.endFill();
			addChild(_bkg);
			
			_slideHolder = new Sprite();
			addChild(_slideHolder);
			
//			_slideDataArr = _dataModel.slideDataArr;
//			_totalSlide = _slideDataArr.length;
//			_slideContentArr = [];
//			for (var i:uint = 0; i<_totalSlide; i++){
//				var slideContent:SlideContent = new SlideContent(_slideDataArr[i]);
//				slideContent.x = Configuration.STAGE_W*i;
//				_slideContentArr.push(slideContent);
//				_slideHolder.addChild(slideContent);
//			}
			
			_prevBt = new PrevButton();
			_prevBt.x = 696;
			_prevBt.y = 972;
			addChild(_prevBt);
			
			_nextBt = new NextButton();
			_nextBt.x = 892;
			_nextBt.y = 972;
			addChild(_nextBt);
			
			_prevBt.mouseChildren = _nextBt.mouseChildren = false;
			_prevBt.buttonMode = _nextBt.buttonMode = true;
			_prevBt.mouseEnabled = _nextBt.mouseEnabled = true;
			_prevBt.addEventListener(MouseEvent.MOUSE_DOWN, onPrevRequested);
			_nextBt.addEventListener(MouseEvent.MOUSE_DOWN, onNextRequested);
			
			_prevBt.alpha = .5;
			_prevBt.mouseEnabled = false;
			
			_finishBt = new ExitButton();
			addChild(_finishBt);
			_finishBt.x = 58;
			_finishBt.y = 972;
			_finishBt.addEventListener(MouseEvent.MOUSE_DOWN, onFinishRequested);
			
			_logo = new XeroxLogo();
			addChild(_logo);
			_logo.x = 1655;
			_logo.y = 986;
			
			if (!_styleManager.cssReady) {
				_styleManager.loadStyle(Configuration.STYLE_PATH);
				_styleManager.addEventListener(StyleManager.CSS_READY, styleLoadedHandler);
			} else {
				buildSlideContent();
			}
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		override public function destroy():void
		{
			_prevBt.removeEventListener(MouseEvent.MOUSE_DOWN, onPrevRequested);
			_nextBt.removeEventListener(MouseEvent.MOUSE_DOWN, onNextRequested);
			_finishBt.removeEventListener(MouseEvent.MOUSE_DOWN, onFinishRequested);
			
			for (var i:uint = 0; i<_totalSlide; i++){
				if (_slideContentArr[i]) {
					_slideHolder.removeChild(_slideContentArr[i]);
					_slideContentArr[i] = null;
				}
			}
			
			removeChild(_bkg);
			removeChild(_slideHolder);
			removeChild(_prevBt);
			removeChild(_nextBt);
			removeChild(_finishBt);
			removeChild(_logo);
			
			_slideContentArr = null;
			_prevBt = null;
			_nextBt = null;
			_logo = null;
			_slideHolder = null;
			_bkg = null;
		}
		
		private function buildSlideContent():void
		{
			_slideDataArr = _dataModel.slideDataArr;
			_totalSlide = _slideDataArr.length;
			_slideContentArr = [];
			for (var i:uint = 0; i<_totalSlide; i++){
				var slideContent:SlideContent = new SlideContent(_slideDataArr[i]);
				slideContent.x = Configuration.STAGE_W*i;
				_slideContentArr.push(slideContent);
				_slideHolder.addChild(slideContent);
			}
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// EventHandlers
		////////////////////////////////////////////////////////////////////////////
		private function onPrevRequested(e:MouseEvent):void
		{
			_nextBt.alpha = 1;
			_nextBt.mouseEnabled = true;
			
			_currentSlideNum--;
			if (_currentSlideNum == 0) {
				_prevBt.alpha = .5;
				_prevBt.mouseEnabled = false;
			}
			
			var targetX:Number = -(_currentSlideNum)*Configuration.STAGE_W;
			TweenLite.to(_slideHolder, .5, {x:targetX, ease:Quad.easeOut});
		}
		
		private function onNextRequested(e:MouseEvent):void
		{
			_prevBt.alpha = 1;
			_prevBt.mouseEnabled = true;
			
			_currentSlideNum++;
			if (_currentSlideNum == (_totalSlide - 1)) {
				_nextBt.alpha = .5;
				_nextBt.mouseEnabled = false;
			}
			
			var targetX:Number = -(_currentSlideNum)*Configuration.STAGE_W;
			TweenLite.to(_slideHolder, .5, {x:targetX, ease:Quad.easeOut});
		}
		
		private function onFinishRequested(e:Event):void
		{
			_finishBt.removeEventListener(MouseEvent.MOUSE_DOWN, onFinishRequested);
			dispatchEvent(new GameEvent(GameEvent.PRESENTATION_END));
		}
		
		private function styleLoadedHandler(event:Event):void 
		{
			trace("INFO SlideView :: styleLoadedHandler");
			buildSlideContent();
        }
	}
}