package com.blue_telescope.xerox_autobot.view {
	import com.base.utils.ArrayUtils;
	import com.blue_telescope.xerox_autobot.Configuration;
	import com.blue_telescope.xerox_autobot.assets.GameViewBkg;
	import com.blue_telescope.xerox_autobot.assets.UnitBox;
	import com.blue_telescope.xerox_autobot.assets.WelldonePopup;
	import com.blue_telescope.xerox_autobot.data.PlayerData;
	import com.blue_telescope.xerox_autobot.events.GameEvent;
	import com.blue_telescope.xerox_autobot.manager.Controller;
	import com.blue_telescope.xerox_autobot.manager.PopupManager;
	import com.blue_telescope.xerox_autobot.manager.SoundController;
	import com.blue_telescope.xerox_autobot.ui.ActionButton;
	import com.blue_telescope.xerox_autobot.ui.MetricPanel;
	import com.blue_telescope.xerox_autobot.ui.Player;
	import com.blue_telescope.xerox_autobot.ui.StopWatch;
	import com.blue_telescope.xerox_autobot.ui.TaskIcon;
	import com.blue_telescope.xerox_autobot.ui.TextBubble;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Circ;
	import com.greensock.easing.Expo;
	import com.greensock.easing.Quad;
	import com.greensock.easing.Quint;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.utils.clearInterval;
	import flash.utils.clearTimeout;
	import flash.utils.getTimer;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="gameEnd", type="com.blue_telescope.xerox_autobot.events.GameEvent")]
	[Event(name="autobotStart", type="com.blue_telescope.xerox_autobot.events.GameEvent")]
	[Event(name="startDemo", type="com.blue_telescope.xerox_autobot.events.GameEvent")]
	[Event(name="restartGame", type="com.blue_telescope.xerox_autobot.events.GameEvent")]
	[Event(name="startPresentation", type="com.blue_telescope.xerox_autobot.events.GameEvent")]
	public class GameView extends BaseView {

		private static const START_ICON_X		:Number = 67;	//invisible icon waiting location , 40 + 27
		private static const START_ICON_Y		:Number = 296;	
		private static const INIT_ICON_X		:Number = 593;	//the 1st in the queue location, 566 + 17
		private static const ICON_W				:Number = 54;
		private static const ICON_OFFSET_X		:Number = 4;
		//
		private static const CLOUD_DOC_X		:Number = 435;	//425
		private static const CLOUD_DOC_Y		:Number = 155;	//102
		private static const SERVER_DOC_X		:Number = 176;	
		private static const SERVER_DOC_Y		:Number = 680;
		private static const MANAGER_DOC_X		:Number = 528;	
		private static const MANAGER_DOC_Y		:Number = 685;	
		private static const SUPERVISOR_DOC_X	:Number = 1290;	
		private static const SUPERVISOR_DOC_Y	:Number = 496;		
		private static const AUTOBOT_DOC_X		:Number = 80;
		private static const AUTOBOT_DOC_Y		:Number = 785;	
		private static const PLAYER_LOCS		:Array = [[528,685],[1290,496],[1380,694], [1548,570], [1440,886], [1612,766], [1782,650], [176,680], [1070,215], [80,785]];
		
		private static const SOURCE_COLOR		:Number = 0x7053aa;
		private static const DEST_COLOR			:Number = 0x7053aa;
		private static const ACTIVE_COLOR		:Number = 0x6DAF3D;
		private static const INACTIVE_COLOR		:Number = 0x777777;
		
		//--background
		private var _bkg:GameViewBkg;
		private var _cloud:MovieClip;
		private var _taskBar:MovieClip;
		private var _metricPanel1:MetricPanel;
		private var _metricPanel2:MetricPanel;
		private var _currentMetricPanel:MetricPanel;
		private var _taskIconArr:Array = [];
		private var _actionButton:ActionButton;	//"Try it with xerox autobot" button
		private var _introPopup:MovieClip;
		private var _welldonePopup:MovieClip;
		private var _blocker:Sprite;
		private var _dummy:Sprite;
		private var _gotoVideoButton:ActionButton;
		private var _restartButton:ActionButton;
		private var _learnMoreButton:ActionButton;
		
		//--Rooms
		private var _room0:MovieClip;			//manager	
		private var _room1:MovieClip;			//supervisor		
		private var _room2:MovieClip;			//member2
		private var _room3:MovieClip;			//member3
		private var _room4:MovieClip;			//member4
		private var _room5:MovieClip;			//newHire1
		private var _room6:MovieClip;			//newHire2
		private var _room7:MovieClip;			//IT, Server, AutobotServer
		private var _room8:MovieClip;			//Instructor(Trainer)
		private var _autobotServer:MovieClip;
		
		//--Personnel		
		private var _players:Array = [];
		private var _activePlayers:Array = [];
		//
		private var _manager:Player;			//_player0
		private var _supervisor:Player;			//_player1
		private var _player2:Player;
		private var _player3:Player;
		private var _player4:Player;
		private var _newHire1:Player;			//_player5
		private var _newHire2:Player;			//_player6
		private var _serverIT:Player;			//_player7
		private var _trainer:Player;			//_player8
		private var _trainee1:Player;			//_player2 in the beginning
		private var _trainee2:Player;			//_player3 in the beginning
		private var _autobotPlayer:Player;
		//
		private var _playerHelped1:Player;
		private var _playerHelped2:Player;
		
		private var _stopWatch:StopWatch;
		private var _textBubble:TextBubble;
		private var _docArr:Array = [];
		private var _doc1:TaskIcon;
		private var _doc2:TaskIcon;
		private var _doc3:TaskIcon;
		private var _doc4:TaskIcon;
		
		private var _currentDoc:TaskIcon;
		private var _matchingDoc:TaskIcon;
		private var _currentTaskIcon:TaskIcon;
		private var _currentTaskID:uint;
		
		private var _totalTasks:uint = 0;		
		private var _nextIconIndex:uint = 0;
		private var _iconLoadingCounter:uint = 0;
		private var _taskStartTime:Number;
		private var _productionEvaluated:Boolean = false;
		private var _isPhase1:Boolean = true;	//for task3,4,5,and 9: is phase1(from the server to supervisor)  or task9 or phase2(from supervisor to manager)
		private var _isShowingClue:Boolean = false;
		private var _destPlayer:Player;
		private var _isSamplePlay:Boolean = true;
		private var _isAutobotPlay:Boolean = true;
		private var _currentAutoTaskIcon:TaskIcon = null;
		private var _isAutotaskDone:Boolean = false;
		private var _isDropIntervalRequested:Boolean = false;
		private var _isVideoSession:Boolean = false;
		private var _isIconRecycling:Boolean = false;
		
		//-- popup related
		private var _popupManager:PopupManager;
		private var _isGameEnd:Boolean = false;
		private var _isAutobotPopupOn:Boolean = false;
		
		//-- intervalIDs
		private var _loadNextIconIntervalID:Number;
		private var _evaluateProductionID:Number;
		private var _playPhoneSoundID:Number;
		//-- popup activity related (timeoutIDs)
		private var _trainingDoneID:Number;
		private var _ITHelpDoneID:Number;

		//-- sample game related
		private var _sampleTaskIcon:TaskIcon;
		
		//-- iconLoadingCounter that starts autobot demo video
		private var _startDemoCount:uint = 50;
		
		//-- for test
		private var _isTesting:Boolean = Configuration.isTesting;
		private var _iconCounterField:TextField;
		
		private var _soundController:SoundController;
		
		
		public function GameView() {
			super();
		}
		
		override protected function configUI() : void 
		{
			//-- singletons
			_controller.registerView(Controller.GAME_VIEW, this);
			_soundController = SoundController.getInstance();
			_popupManager = PopupManager.getInstance();
			_popupManager.registerView(Controller.GAME_VIEW, this);
			
			//-- environment
			_bkg = new GameViewBkg();
			addChild(_bkg);
			_cloud = _bkg.cloud;
			_taskBar = _bkg.taskBar;
			_introPopup = _bkg.introPopup;
			_introPopup.visible = false;
			
			//-- metricPanel
			_metricPanel1 = new MetricPanel();
			_metricPanel1.x = 1370;
			_metricPanel1.y = 50;
			addChild(_metricPanel1);
			
			_currentMetricPanel = _metricPanel1;
			
			//-- rooms
			for (var j:uint=0; j<=8; j++) {
				this["_room" + j] = _bkg["room" + j];
			}
			_room2.p7.stop();
			_room2.p7.visible = false;
			_room4.p7.stop();
			_room4.p7.visible = false;
			
			_autobotServer = _room7.autobot;
			_autobotServer.visible = false;
			
			//-- players
			//-- _players[9]:autobot
			for (var i:uint=0; i<=9; i++) {
				var player:Player = new Player(i);
				player.x = PLAYER_LOCS[i][0];
				player.y = PLAYER_LOCS[i][1];
				if (i!=9) {
					player.addEventListener("active", onPlayerActivated);
					player.addEventListener("inactive", onPlayerInactivated);
				}
				addChild(player);
				_players.push(player);
			}
			
			_manager = _players[PlayerData.MANAGER_INDEX];					//0:femaleManager
			_supervisor = _players[PlayerData.SUPERVISOR_INDEX];			//1:maleSupervisor
			_player2 = _players[PlayerData.MEMBER2_INDEX];					//2:maleMember2
			_player3 = _players[PlayerData.MEMBER3_INDEX];					//3:femaleMember3
			_player4 = _players[PlayerData.MEMBER4_INDEX];					//4:femaleMember4
			_newHire1 = _players[PlayerData.NEWHIRE1_INDEX];				//5:newFemale
			_newHire2 = _players[PlayerData.NEWHIRE2_INDEX];				//6:newMale
			_serverIT = _players[PlayerData.SERVER_IT_INDEX];				//7:IT guy(male)
			_trainer = _players[PlayerData.INSTRUCTOR_INDEX];				//8:instructor(male)
			_autobotPlayer = _players[PlayerData.AUTOBOT_INDEX];			//9:autobot
			
			_trainee1 = _player2;
			_trainee2 = _player3;
			
			//-- hide newHires
			_room5.person.visible = false;	//5:newHire Female
			_room6.person.visible = false;	//6:newHire Male
			if (_room5.person1) _room5.person1.visible = false;
			if (_room6.person1) _room6.person1.visible = false;
			
			//-- hide trainee1 and trainee2 in the training room
			initTrainees();
			
			//-- stopWatch and timer
			_stopWatch = new StopWatch();
			_stopWatch.x = 243;
			_stopWatch.y = -150;
			_stopWatch.visible = false;
			addChild(_stopWatch);
			
			//-- actionButton
			_actionButton = new ActionButton(1, false);
			addChild(_actionButton);
			_actionButton.x = Math.floor((Configuration.STAGE_W - _actionButton.width)/2);
			_actionButton.y = 973;
			_actionButton.active = false;
			
			_welldonePopup = new WelldonePopup();
			_welldonePopup.x = 1005;
			_welldonePopup.y = 647;
			addChild(_welldonePopup);
			_welldonePopup.visible = false;
			
			if (_isTesting) {
				_iconCounterField = _bkg.iconCounter.counterField;
			} else {
				_bkg.iconCounter.visible = false;
			}
		}
		
		private function buildMetricPanel2():void
		{
//			trace("INFO GameView :: buildMetricPanel2");
			_metricPanel2 = new MetricPanel(true);
			_metricPanel2.x = 1370;
			_metricPanel2.y = 260;
			addChild(_metricPanel2);
		}
		
		private function destroyMetricPanel2():void
		{
//			trace("INFO GameView :: destroyMetricPanel2");
			if (_metricPanel2) {
				_metricPanel2.destroy();
				removeChild(_metricPanel2);
			}
			_metricPanel2 = null;
		}
		
		private function playPhoneSound(volume:Number = .3):void
		{
			_soundController.playPhone(volume);
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		override public function build():void
		{
//			trace("INFO GameView :: build");
			buildTaskIcons();
		}
			
		public function startGame():void
		{
//			trace("INFO GameView :: startGame");
			build();
			
			if (Configuration.isTesting) {
				showStopWatch();
			} else {
				startSampleGame();
			}
			
			_soundController.playAmbience();
			TweenLite.delayedCall(1, playPhoneSound);
			_playPhoneSoundID = setInterval(playPhoneSound, 45000);
		}
		
		public function endAutobotVideo():void
		{
			trace("\n=================================");
			trace("INFO GameView :: endAutobotVideo");
			trace("\n=================================");
			
			//-- to secure some weird bug of icons being loaded continuously --//
			clearInterval(_loadNextIconIntervalID);
			_isAutotaskDone = true;
			//-----------------------------------------------------------------//

			//-- remove taskIcons
			var diff:Number = INIT_ICON_X - START_ICON_X;
			for (var k:uint = 0; k<_totalTasks; k++) {
				var taskIcon:TaskIcon = _taskIconArr[k];
				if (taskIcon) {
					var targetX:Number = taskIcon.x - diff;
					TweenLite.killTweensOf(taskIcon);
					if (k == 0) {
						TweenLite.to(taskIcon, .4, {delay: (_totalTasks)*.2, alpha:-.5, x:targetX, ease:Quad.easeOut, onComplete:destroyTaskIcons});
					} else {
						TweenLite.to(taskIcon, .4, {delay: (_totalTasks-k)*.2, alpha:-.5, x:targetX, ease:Quad.easeOut});
					}
				}
			}
			
			//-- blocker
			var initDelay:Number = 2;
			buildBlocker();
			TweenLite.to(_blocker, .75, {delay:initDelay, alpha:.25,  ease:Quad.easeOut});
				
			//-- restartButton
			_restartButton = new ActionButton(2, true);
			addChild(_restartButton);
			_restartButton.x = Math.floor((Configuration.STAGE_W - _restartButton.width)/2);
			_restartButton.targetY = Math.floor(Configuration.STAGE_H/2 - 75);
			_restartButton.y = Configuration.STAGE_H + 50;
			_restartButton.active = true;
			_restartButton.alpha = 0;
			_restartButton.mouseEnabled = false;
			_restartButton.addEventListener(MouseEvent.MOUSE_DOWN, onRestartPressed);
			
			//-- learnMoreButton
			_learnMoreButton = new ActionButton(3, true);
			addChild(_learnMoreButton);
			if (_learnMoreButton.width != _restartButton.width) {
				var maxWidth:Number = Math.max(_learnMoreButton.width, _restartButton.width);
				_restartButton.bkgWidth = _learnMoreButton.bkgWidth = maxWidth;
			}
			_learnMoreButton.x = Math.floor((Configuration.STAGE_W - _learnMoreButton.width)/2);
			_learnMoreButton.targetY = Math.floor(Configuration.STAGE_H/2 + 20);
			_learnMoreButton.y = Configuration.STAGE_H + 100;
			_learnMoreButton.active = true;
			_learnMoreButton.alpha = 0;
			_learnMoreButton.mouseEnabled = false;
			_learnMoreButton.addEventListener(MouseEvent.MOUSE_DOWN, onLearnMorePressed);
			
			TweenLite.to(_restartButton, .5, {delay:initDelay + .6, alpha:1, y:_restartButton.targetY, ease:Quint.easeOut});
			TweenLite.to(_learnMoreButton, .5, {delay:initDelay + .9, alpha:1, y:_learnMoreButton.targetY, ease:Quint.easeOut, onComplete:function(){_restartButton.mouseEnabled = true;_learnMoreButton.mouseEnabled = true;}});
		}
		
	
		////////////////////////////////////////////////////////////////////////////
		// Popup/Autobot Tasks
		////////////////////////////////////////////////////////////////////////////
		//-- traingPopup 
		public function addMembersToTraining():void
		{
			//trace("INFO GameView :: addMembersToTraining");
			
			_room7.person.gotoAndPlay("fixing");
			
			_trainee1.isTraining = true;
			_trainee2.isTraining = true;
			_trainee1.stopWorking();	//player2
			_trainee2.stopWorking();	//player3
			hideClueDocOn(_trainee1);
			hideClueDocOn(_trainee2);
			if (!_trainee1.isClueHidden || !_trainee2.isClueHidden) {
				_trainee1.hideClue();
				_trainee2.hideClue();
				resetClue(_currentTaskID);
			} else {
				setActivePlayers();
			}

			hidePerson(_trainee1.playerID, true, .3);
			hidePerson(_trainee2.playerID, true, .8);
			
			_room8.traineeP6.gotoAndStop(1);
			_room8.traineeP6.alpha = 1;
			TweenLite.to(_room8.traineeP6, .5, {alpha:0, onComplete:function(){_room8.traineeP6.visible = false;}});	
			_room8.traineeP5.gotoAndStop(1);
			_room8.traineeP5.alpha = 1;
			TweenLite.to(_room8.traineeP5, .5, {delay:.3, alpha:0, onComplete:function(){_room8.traineeP5.visible = false;}});
			
			//-- Team Member # 1 and Team Member # 2 disppear from the desks and reappear in the Training Room in front of a Trainer.  																										
			TweenLite.delayedCall(2.5, showMemberTrainees);
			
			//-- The training character animation runs for 10 seconds.		
			_trainingDoneID = setTimeout(onMemberTrainingDone, 13000);	//10 + 3(delay)				
			_currentMetricPanel.dropProduction(2);
		}
		
		public function onNoTrainingRequested():void
		{
			_currentMetricPanel.dropQuality();
		}
		
		private function onMemberTrainingDone():void
		{
//			trace("INFO GameView :: onMemberTrainingDone");
			
			//-- At this point, the trainer disappears and the two workers disappear from the Training Room and reappear at their desks.
			hideMemberTrainees();
			
			var id1:uint = _trainee1.playerID;
			var id2:uint = _trainee2.playerID;
			showPerson(id1, true, 1.3);
			showPerson(id2, true, 1.8);
			
			TweenLite.delayedCall(1.8, restartWorking, [_trainee1]);
			TweenLite.delayedCall(2.3, restartWorking, [_trainee2]);
			
			_currentMetricPanel.raiseProduction();
			_currentMetricPanel.raiseQuality();
		}
		
		private function restartWorking(targetPlayer:Player):void
		{
			targetPlayer.isTraining = false;
			targetPlayer.isTrained = true;
			targetPlayer.restartWorking();
		}
		
		//-- ITPopup
		public function assignITMembersToOccupiedCubicles():void
		{
//			trace("INFO GameView :: assignITMembersToOccupiedCubicles");

			var id1:uint;
			id1 = 4;
			
			_playerHelped1 = _players[id1];
			_playerHelped1.isAlwaysActive = true;
			_playerHelped1.stopWorking();
			
			if (_room4.person) {
				_room4.person.stop();
				_room4.person.alpha = 1;
				_room4.person.visible = true;
				TweenLite.to(_room4.person, .3, {tint:null});
			}
			if (_room4.person1) {
				_room4.person1.alpha = 1;
				_room4.person1.visible = true;
				TweenLite.to(_room4.person1, .3, {tint:null});
			}
			
			//-- the IT guys walks from the server room to each occupied cubicle. He sits at their desks and installs software. The cubicle workers stand and watch.
			var targetRoom1:MovieClip = this["_room" + id1];
			_room7.person.gotoAndStop(1);
			_room7.person.visible = false;
			_room7.walkingOut.alpha = 1;
			_room7.walkingOut.visible = true;
			_room7.walkingOut.addEventListener("doorOpen", hideRoom7ClosedDoor);
			_room7.walkingOut.addEventListener("doorClosed", showRoom7ClosedDoor);
			_room7.walkingOut.addEventListener("standing", sitAndStand1);
			_room7.walkingOut.gotoAndPlay("walking");
																												
			_currentMetricPanel.dropProduction(2);
		}
		
		public function onNoITHelpRequested():void
		{
			_currentMetricPanel.dropProduction();
			_currentMetricPanel.dropQuality();
		}
		
		private function hideRoom7ClosedDoor(e:Event):void
		{
			_room7.door.visible = false;
		}
		
		private function showRoom7ClosedDoor(e:Event):void
		{
			_room7.door.visible = true;
		}
		
		private function sitAndStand1(e:Event):void
		{
			var id:uint = _playerHelped1.playerID;		//4
			var targetRoom:MovieClip = this["_room" + id];
			
			targetRoom.person.gotoAndStop("standing");	//room4
			TweenLite.to(_room7.walkingOut, .4, {alpha:0, onComplete:function(){_room7.walkingOut.visible = false;}});	//severGuy
			targetRoom.p7.alpha = 0;					//serverGuy in the cubicle
			targetRoom.p7.visible = true;
			TweenLite.to(targetRoom.p7, .4, {alpha:1, delay:.25, onComplete:function(){targetRoom.p7.gotoAndPlay(1);}});
			
			_ITHelpDoneID = setTimeout(onITHelpDone, 10000);
		}
		
		private function sitAndStand2(e:Event):void
		{
			//trace("INFO GameView :: sitAndStand2");
			
			_room2.person.gotoAndStop("standing");
			//
			TweenLite.to(_room7.walkingOut, .4, {alpha:0, onComplete:function(){_room7.walkingOut.visible = false;}});
			//
			_room2.p7.alpha = 0;
			_room2.p7.visible = true;
			TweenLite.to(_room2.p7, .4, {alpha:1, delay:.25, onComplete:function(){_room2.p7.gotoAndPlay(1);}});
		}
		
		private function onITHelpDone():void
		{
//			trace("INFO GameView :: onITHelpDone, room7 person alpha back to 1");
			
			var id1:uint = _playerHelped1.playerID;	//4
			var targetRoom1:MovieClip = this["_room" + id1];
			
			_playerHelped1.isAlwaysActive = false;
			_room7.walkingOut.gotoAndStop(1);

			TweenLite.to(targetRoom1.person, .4, {alpha:0, onComplete:function(){targetRoom1.person.gotoAndPlay("typing");_playerHelped1.restartWorking();}});
			TweenLite.to(targetRoom1.p7, .4, {alpha:0, onComplete:function(){targetRoom1.p7.gotoAndStop(1);targetRoom1.p7.visible = false;_room7.person.gotoAndPlay("fixing");_room7.person.visible = true;}});
				
			_currentMetricPanel.raiseProduction();
			_currentMetricPanel.raiseQuality();
		}
		
		//-- newHirePopup
		public function hireNewMembers():void
		{
			hideTrainees();
			
			_room7.person.gotoAndPlay("fixing");
			_soundController.playTyping();
			
			//-- team members appear in the two empty cubicles. They become active participants in task movements.																								
			showPerson(_newHire1.playerID, true, .5);
			showPerson(_newHire2.playerID, true, 1);
			_dataModel.addActivePlayer(_newHire1.playerID);
			_dataModel.addActivePlayer(_newHire2.playerID);
			
			_room5.person.gotoAndPlay("typing");
			_room6.person.gotoAndPlay("typing");
		
			_currentMetricPanel.raiseProduction(2);
			_currentMetricPanel.dropQuality(2);
			_currentMetricPanel.raiseCost(2);
		}
		
		public function onNoNewHireRequested():void
		{
			_currentMetricPanel.dropProduction();
			_currentMetricPanel.dropQuality();
			hideTrainees();
		}
		
		//-- secondShiftPopup
		public function startSecondShift():void
		{
//			trace("INFO GameView :: startSecondShift, addNewHiresToTraining");
			
			//-- two new Team Members appear in the Training Room in front of a Trainer.
			TweenLite.delayedCall(.5, showNewHireTrainees); 
																												
			_currentMetricPanel.raiseProduction(2);
			_currentMetricPanel.dropQuality();
			_currentMetricPanel.raiseCost(2);
		}
		
		public function onNoSecondShiftRequested():void
		{
			_currentMetricPanel.dropProduction();
			_currentMetricPanel.dropQuality();
		}
		
		//-- turnOverPopup
		public function whenTheBestEmployeeQuit():void
		{
			trace("INFO GameView :: whenTheBestEmployeeQuit");
			
			_room7.person.gotoAndPlay("fixing");
			_soundController.stopTyping();
			
			//-- one of the team members disappears. Any uncompleted work at that individual's desk transfers to the supervisor's desk.
			var pID:uint = Math.floor(Math.random()*3) + 2;			//-- 2 to 4, except new hires
			var randomPlayer:Player = _players[pID];
			if (!randomPlayer.isClueHidden) {
				//-- pick another one w. no clue showing
				var newID:uint = Math.floor(Math.random()*3) + 2;
				while (newID == pID) {
					newID = Math.floor(Math.random()*3) + 2;
				}
				randomPlayer = _players[newID];
			}
			randomPlayer.stopWorking();
			randomPlayer.isTraining = true;							//-- to prevent to change its color to grey, when inactivated
			while (randomPlayer.numOfTasks>0) {
				randomPlayer.removeTask();
				_supervisor.addTask(false);																										
			}
			_dataModel.removeActivePlayer(randomPlayer.playerID);	//-- redundant, bc it's done by "stopWorking()" function in the Player class
			hideClueDocOn(randomPlayer);
			if (!randomPlayer.isClueHidden) {
				trace("ERROR GameView :: whenTheBestEmployeeQuit, this shoudn't happen as it's already coverd in line>= 639 ");
				randomPlayer.hideClue();
				resetClue(_currentTaskID);
			} else {
				setActivePlayers();
			}

			TweenLite.delayedCall(.5, hidePerson, [randomPlayer.playerID, true]);

			_currentMetricPanel.dropProduction();
			_currentMetricPanel.dropQuality();
			_currentMetricPanel.dropCost();
		}
		
		private function hideClueDocOn(targetPlayer:Player):void
		{
			//trace("INFO GameView :: hideClueDocOn");
			if (_docArr.length<=1) return;
			
			if (targetPlayer) {
//				trace("INFO GameView :: hideClueDocOn, targetPlayer.playerID:" + targetPlayer.playerID);
				var totalDoc:uint = _docArr.length;
				var isTextBubbleHidden:Boolean = false;
				var docIndex:uint;
				for (var i:uint=0; i<totalDoc; i++) {
					var doc:TaskIcon = _docArr[i];
					var targetX:Number = targetPlayer.x;
					if ((doc.targetX == targetX) && (totalDoc != 1)) {
						//-- when the doc is on the targetPlayer, but it's not the only doc on the stage
						TweenLite.killTweensOf(doc);
						TweenLite.to(doc, .4, {alpha:0, onComplete:function(){doc.visible = false;}});
						if (_textBubble) {
							if ((doc.x + 15) == (_textBubble.x)) {
								hideTextBubble();
								isTextBubbleHidden = true;
								docIndex = i;
							}
						}
						break;
					}
				}
				
				if (isTextBubbleHidden) {
					var bubbleIndex:uint = Math.floor(Math.random()*totalDoc);
					while (bubbleIndex == docIndex) {
						bubbleIndex = Math.floor(Math.random()*totalDoc);
					}
					var doc2:TaskIcon = _docArr[bubbleIndex];
					var bubbleX:Number = doc2.x + 15;
					var bubbleY:Number = doc2.y - 38;
					showTextBubble(_currentTaskID, bubbleX, bubbleY);
				}
			}
		}
		
		private function resetClue(taskID:uint, isShowingClue:Boolean = true):void
		{
//			trace("INFO GameView :: resetClue for taskID: " + taskID);

			_isShowingClue = false;
			switch (taskID) {
				case 0:
				case 1:
				case 2:
					setActivePlayers();
					if (isShowingClue) showClueOnRandomAvailPlayer();
					break;
				case 5:
					//-- from manager to supervisor, for matching, then to avail players
					if (!_isPhase1) {
						setActivePlayers();
						if (isShowingClue) showClueOnRandomAvailPlayer();
					}
					break;
			}//switch
		}
		
		private function recoverMouseEvents():void
		{
//			trace("INFO GameView :: recoverMouseEvents");
			
			if (_currentDoc) {
				_currentDoc.mouseEnabled = true;
				_currentDoc.addEventListener(MouseEvent.MOUSE_DOWN, onDoc1DownHandler);
			}
			
			if (_matchingDoc) {
				_matchingDoc.mouseEnabled = true;
				_matchingDoc.addEventListener(MouseEvent.MOUSE_DOWN, onDoc2DownHandler);
			}
			
			if (_docArr.length>0) {
				var total:uint = _docArr.length;
				for (var i:uint = 0; i<total; i++) {
					var doc:TaskIcon = _docArr[i];
					doc.mouseEnabled = true;
					doc.addEventListener(MouseEvent.MOUSE_DOWN, onDocsDownHandler);
				}
			}
		}
		
		private function automateTasksByAutobot(taskID:uint = 0):void
		{
//			trace("INFO GameView :: automateTasksByAutobot, taskID:" + taskID);
			
			//-- (The first time the player says "OK" to an AutoBot task, the AutoBot server graphic appears in the server room so that there's a visual representation of AutoBot.)
			if (!_autobotServer.visible) TweenLite.delayedCall(.5, showAutobot);

			if (!_isAutobotPlay) {																																																				
				var activePlayerIDs:Array = _dataModel.getNumActiveTeamMemberIDs();
				var totalActive:uint = activePlayerIDs.length;
//				trace("INFO GameView :: automateTasksByAutobot, anything left? activePlayerIDs.length??" + activePlayerIDs.length);
				if (totalActive > 1) {
					for (var i:uint=0;i<totalActive;i++) {
						var pIndex:uint = activePlayerIDs[i];
						_players[pIndex].isAlwaysActive = false;
					}
					//-- one team member disappears
					//-- This task, from this point on, is automated by the program.	
					var pID:uint = Math.floor(Math.random()*5) + 2;	//2 to 6
					var isActive:Boolean = (activePlayerIDs.indexOf(pID) >= 0);
					while (!isActive) {
						pID = Math.floor(Math.random()*5) + 2;
						isActive = (activePlayerIDs.indexOf(pID) >= 0);
					}
					var randomPlayer:Player = _players[pID];
					randomPlayer.stopWorking();
					hideClueDocOn(randomPlayer);
					while (randomPlayer.numOfTasks>0) {
						randomPlayer.removeTask();
						_supervisor.addTask(false);																										
					}
					_dataModel.removeActivePlayer(randomPlayer.playerID);	//redundant, bc it's done by "stopWorking()" function in the Player class
					if (!randomPlayer.isClueHidden) {
						randomPlayer.hideClue();
						resetClue(_currentTaskID);
					}
					hidePerson(pID, true, .3);
				} else if (totalActive == 1) {
					var onlyPlayer:Player = _players[activePlayerIDs[0]];
					onlyPlayer.isAlwaysActive = true;
				}
			}
			
			//trace("INFO GameView :: automateTasksByAutobot, _autobotTaskID:" + _autobotTaskID);
			_currentTaskID = taskID;
			var delay1:Number = .2;	//.3
			var delay2:Number = .4;	//.6
			var delay3:Number = .8;	//1.8 to 1
			switch (taskID) {
				case 0:
				case 1:
					TweenLite.delayedCall(delay1, showDocInCloud, [taskID]);
					TweenLite.delayedCall(delay2, showAutobotServer);
					TweenLite.delayedCall(delay3, moveDoc1ToAutobotServer);	
					break;
				case 2:
					TweenLite.delayedCall(delay1, showDocInServerRoom, [taskID]);
					TweenLite.delayedCall(delay2, showAutobotServer);
					TweenLite.delayedCall(delay3, moveDoc1ToAutobotServer);
					break;
				case 3:
				case 4:
					//-- phase1
					TweenLite.delayedCall(delay1, showDocInServerRoom, [taskID]);
					TweenLite.delayedCall(delay2, showMatchingDocInAutobotServer, [taskID]);
					TweenLite.delayedCall(delay3, matchDoc1InAutobotServer);
					//-- phase2
					//-- from matchDoc1ToAutobotServer --> to moveMatchingDocToManager();
					break;
				case 5:
					//-- phase1
					TweenLite.delayedCall(delay1, showDocInManager, [taskID]);
					TweenLite.delayedCall(delay2, showMatchingDocInSupervisor, [taskID]);
					TweenLite.delayedCall(delay3, matchDoc1InSupervisor);
					//-- phase2
					//-- from matchDoc1InSupervisor --> to moveMatchingDocToAutobotServer();
					break;
				case 6:
				case 8:
					TweenLite.delayedCall(delay1, showDocInAutobotServer, [taskID]);
					TweenLite.delayedCall(delay2, showClueOnTheDestPlayer, [_serverIT]);
					TweenLite.delayedCall(delay3, moveDoc1ToServer);	
					break;
				case 7:
					TweenLite.delayedCall(delay1, showDocInAutobotServer, [taskID]);
					TweenLite.delayedCall(delay2, showCloud);
					TweenLite.delayedCall(delay3, moveDoc1ToCloud);		
					break;
				case 9:
					TweenLite.delayedCall(delay1, showDocInServerRoom, [taskID]);
					TweenLite.delayedCall(delay2, showAutobotServer);
					TweenLite.delayedCall(delay3, moveDoc1ToAutobotServer, [true]);
					//-- phase2
					//-- from moveDoc1ToAutobotServer --> to moveDoc1ToManager();
					break;
			}
		}
		
		private function onNoAutobotRequested(e:Event = null):void
		{
//			trace("INFO PopupManager :: onNoAutobotRequested");
			
			_isAutobotPopupOn = false;
			if (e) {
				var taskIcon:TaskIcon = e.target as TaskIcon;
				taskIcon.removeEventListener("skip", onNoAutobotRequested);
			}

			_currentMetricPanel.dropProduction();
			_currentMetricPanel.dropQuality();
		}
		
		private function onAutobotRequested(e:Event = null):void
		{
			_isAutobotPopupOn = false;
			if (e) {
				var taskIcon:TaskIcon = e.target as TaskIcon;
				taskIcon.removeEventListener("ok", onAutobotRequested);
			}
		}
		
		//-- autobot tasks
		private function onAutoTaskComplete():void
		{
//			trace("INFO GameView :: onAutoTaskComplete, _currentTaskID is " + _currentTaskID);
			
			killCurrentDoc();
			hideTextBubble();
			hideClueOnTheDestPlayer();
			
			_currentMetricPanel.raiseProduction();
			_currentMetricPanel.raiseQuality();
			_currentMetricPanel.dropCost(.5);	//5/16/14
			
			TweenLite.delayedCall(1, shiftTasks);
		}
		
		private function moveDoc1ToAutobotServer(isReporting:Boolean = false):void
		{
			//trace("INFO GameView :: moveDoc1ToAutobotServer, _doc1 is " + _doc1);
			_currentDoc = _doc1;	
			if (!isReporting) {
				TweenLite.to(_currentDoc, .3, {x:AUTOBOT_DOC_X, y:AUTOBOT_DOC_Y, ease:Circ.easeOut, onComplete:onAutoTaskComplete});
			} else {
				TweenLite.to(_currentDoc, .3, {x:AUTOBOT_DOC_X, y:AUTOBOT_DOC_Y, ease:Circ.easeOut, onComplete:moveDoc1ToManager});
			}
		}
		
		private function matchDoc1InAutobotServer():void
		{
			showClueOnTheDestPlayer(_autobotPlayer, false);
			TweenLite.to(_currentDoc, .2, {x:AUTOBOT_DOC_X, y:AUTOBOT_DOC_Y, ease:Circ.easeOut, onComplete:onAutoMatchedInAutobotServer});
		}
		
		private function onAutoMatchedInAutobotServer():void
		{
			killCurrentDoc();
			hideClueOnTheDestPlayer();
			showClueOnTheDestPlayer(_manager, false);
			_currentDoc = _matchingDoc;
			_currentDoc.alpha = 1;
			moveMatchingDocToManager();
		}
		
		private function moveMatchingDocToManager():void
		{
			//trace("INFO GameView :: moveMatchingDocToManager, _currentDoc is " + _currentDoc);
			//-- 1 to .5, and the delay:.5 to .3
			TweenLite.to(_currentDoc,.3, {delay:.2, x:MANAGER_DOC_X, y:MANAGER_DOC_Y, ease:Circ.easeOut, onComplete:onAutoTaskComplete});
		}
		
		private function matchDoc1InSupervisor():void
		{
			//trace("INFO GameView :: matchDoc1InSupervisor");
			showClueOnTheDestPlayer(_supervisor, false);
			TweenLite.to(_currentDoc, .3, {x:SUPERVISOR_DOC_X, y:SUPERVISOR_DOC_Y, ease:Circ.easeOut, onComplete:onAutoMatchedInSupervisor});
		}
		
		private function onAutoMatchedInSupervisor():void
		{
			killCurrentDoc();
			hideClueOnTheDestPlayer();
			showClueOnTheDestPlayer(_autobotPlayer, false);
			_currentDoc = _matchingDoc;
			_currentDoc.alpha = 1;
			moveMatchingDocToAutobotServer();
		}
		
		private function moveMatchingDocToAutobotServer():void
		{
			TweenLite.to(_currentDoc, .3, {delay:.2, x:AUTOBOT_DOC_X, y:AUTOBOT_DOC_Y, ease:Circ.easeOut, onComplete:onAutoTaskComplete});
		}
		
		private function moveDoc1ToServer():void
		{
			//trace("INFO GameView :: moveDoc1ToServer, _doc1 is " + _doc1);
			_currentDoc = _doc1;
			TweenLite.to(_currentDoc, .3, {x:SERVER_DOC_X, y:SERVER_DOC_Y, ease:Circ.easeOut, onComplete:onAutoTaskComplete});
		}
		
		private function moveDoc1ToCloud():void
		{
			_currentDoc = _doc1;
			TweenLite.to(_currentDoc, .3, {x:CLOUD_DOC_X, y:CLOUD_DOC_Y, ease:Circ.easeOut, onComplete:onAutoTaskComplete});
			TweenLite.delayedCall(1, hideCloud);
		}
		
		private function moveDoc1ToManager():void
		{
			hideClueOnTheDestPlayer();
			showClueOnTheDestPlayer(_manager, false);
			_currentDoc = _doc1;
			TweenLite.to(_currentDoc, .3, {delay:.2, x:MANAGER_DOC_X, y:MANAGER_DOC_Y, ease:Circ.easeOut, onComplete:onAutoTaskComplete});
		}
		

		////////////////////////////////////////////////////////////////////////////
		// Private Methods
		////////////////////////////////////////////////////////////////////////////
		public function reset():void
		{
			trace("INFO GameView :: reset");
			
			destroyEndUIs();
			
			_taskBar.textMC.alpha = 1;
			_taskBar.textMC.visible = true;
			_introPopup.visible = false;
			_welldonePopup.visible = false;
			
			_metricPanel1.reset();
			_metricPanel1.alpha = 1;
			_currentMetricPanel = _metricPanel1;
			
			destroyMetricPanel2();
			
			//-- hide IT guys in member desks and autobotServer
			_room2.p7.stop();
			_room2.p7.visible = false;
			_room4.p7.stop();
			_room4.p7.visible = false;
			_autobotServer.visible = false;
			
			//-- reset players
			for (var j:uint=0; j<=9 ; j++) {
				_players[j].reset();
			}
			_trainee1 = _player2;
			_trainee2 = _player3;
			initTrainees();
			
			for (var i:uint=2; i<=4; i++) {
				var targetRoom:MovieClip = this["_room" + i];
				targetRoom.person.alpha = 1;
				targetRoom.person.gotoAndPlay(1);
				targetRoom.person.visible = true;
				if (targetRoom.person1) {
					targetRoom.person1.alpha = 1;
					targetRoom.person1.visible = true;
				}
			}
			_room5.person.visible = false;	//5:newHire Female
			_room6.person.visible = false;	//6:newHire Male
			if (_room5.person1) _room5.person1.visible = false;
			if (_room6.person1) _room6.person1.visible = false;
			
			//-- remove previous taskIcons & docs, to secure
			destroyTaskIcons();
			killDocsInActivePlayers();
			killCurrentDoc();

			_docArr = [];
			_taskIconArr = [];
			_activePlayers = [];
			
			_totalTasks = 0;
			_nextIconIndex = 0;
			_iconLoadingCounter= 0;
			_currentTaskIcon = null;
			_currentDoc = null;
			_matchingDoc = null;
			_currentAutoTaskIcon = null;
			
			_isPhase1 = true;
			_productionEvaluated = false;
			_isGameEnd = false;
			_isAutobotPopupOn = false;
			_isSamplePlay = false;
			_isAutobotPlay = false;
			_isAutotaskDone = false;
			_isDropIntervalRequested = false;
			_isVideoSession = false;
			_isIconRecycling = false;
			
			_stopWatch.y = -150;
			_stopWatch.visible = false;
			
			//-- actionButton
			_actionButton.active = false;
			_actionButton.alpha = 1;
			_actionButton.visible = true;
			_actionButton.y = 973;
			
			clearInterval(_loadNextIconIntervalID);
			clearInterval(_evaluateProductionID);
			clearTimeout(_trainingDoneID);
			clearTimeout(_ITHelpDoneID);
			clearInterval(_playPhoneSoundID);
		}
		
		private function buildTaskIcons(isAutomated:Boolean = false):void
		{
//			trace("INFO GameView :: buildTaskIcons");
			
			var selectedTaskDataArr:Array = _dataModel.selectedTaskDataArr;
			_totalTasks = selectedTaskDataArr.length;
			_taskIconArr = [];
			for (var i:uint = 0; i<_totalTasks; i++) {
				var taskID:uint = selectedTaskDataArr[i].taskID;
				var taskIcon:TaskIcon = new TaskIcon(taskID, false, true);
				taskIcon.x = START_ICON_X;
				taskIcon.targetX = START_ICON_X;
				taskIcon.y = START_ICON_Y;
				taskIcon.isAutomated = isAutomated;
				taskIcon.visible = false;
				addChild(taskIcon);
				_taskIconArr.push(taskIcon);
			}
		}
		
		private function destroyTaskIcons():void
		{
//			trace("INFO GameView :: destroyTaskIcons");
			
			var total:uint = _taskIconArr.length;
			for (var k:uint = 0; k<total; k++) {
				var taskIcon:TaskIcon = _taskIconArr[k];
				if (taskIcon) {
					taskIcon.destroy();
					removeChild(taskIcon);
					taskIcon = null;
				}
			}
			_taskIconArr = [];
		}
		
		private function buildBlocker(pAlpha:Number = 0):void
		{
//			trace("INFO GameView :: buildBlocker");

			_blocker = new UnitBox();
			_blocker.width = Configuration.STAGE_W;
			_blocker.height = Configuration.STAGE_H;
			_blocker.alpha = pAlpha;
			_blocker.mouseEnabled = false;
			addChild(_blocker);
			
			_dummy = new Sprite();	//to create a child on top of the _blocker
			addChild(_dummy);
		}
		
		private function destroyBlocker():void
		{
//			trace("INFO GameView :: destroyBlocker");	

			if (_blocker) {
				removeChild(_blocker);
				_blocker = null;
			}
			
			if (_dummy) {
				removeChild(_dummy);
				_dummy = null;
			}
		}
		
		private function showPerson(id:uint, isFading:Boolean = false, pDelay:Number = 0):void
		{
//			trace("INFO GameView :: showPerson, room " + id);
			var targetRoom:MovieClip = this["_room" + id];
			if (targetRoom.person) {
				if (isFading) {
					targetRoom.person.alpha = 0;
					targetRoom.person.visible = true;
					TweenLite.killTweensOf(targetRoom.person);
					TweenLite.to(targetRoom.person, .5, {delay:pDelay, alpha:1, ease:Quad.easeOut, onStart:function(){targetRoom.smokeMC.gotoAndPlay(2);_soundController.playPoof();}, onComplete:function(){targetRoom.person.gotoAndPlay(1);}});
				} else {
					targetRoom.person.alpha = 1;
					targetRoom.person.visible = true;	
				}
			}
			if (targetRoom.person1) {
				if (isFading) {
					targetRoom.person1.alpha = 0;
					targetRoom.person1.visible = true;
					TweenLite.killTweensOf(targetRoom.person1);
					TweenLite.to(targetRoom.person1, .5, {delay:pDelay, alpha:1, ease:Quad.easeOut});
				} else {
					targetRoom.person1.alpha = 1;
					targetRoom.person1.visible = true;	
				}
			}
		}
		
		private function hidePerson(id:uint, isFading:Boolean = false, pDelay:Number = 0, soundVolume:Number=.6):void
		{
			//trace("INFO GameView :: hidePerson, playerID:" + id);
			var targetRoom:MovieClip = this["_room" + id];
			if (targetRoom.person) {
				if (isFading) {
					TweenLite.killTweensOf(targetRoom.person);
					TweenLite.to(targetRoom.person, .5, {delay:pDelay, alpha:0, ease:Quad.easeOut, onStart:function(){targetRoom.smokeMC.gotoAndPlay(2);_soundController.playPoof(soundVolume);}, onComplete:function(){targetRoom.person.visible = false;}});
				} else {
					targetRoom.smokeMC.gotoAndPlay(2);
					targetRoom.person.visible = false;
					_soundController.playPoof(soundVolume);
				}
			}
			if (targetRoom.person1) {
				if (isFading) {
					TweenLite.killTweensOf(targetRoom.person1);
					TweenLite.to(targetRoom.person1, .5, {delay:pDelay, alpha:0, ease:Quad.easeOut, onComplete:function(){targetRoom.person1.visible = false;}});
				} else {
					targetRoom.person1.visible = false;
				}
			}
		}
		
		private function initTrainees():void
		{
			_room8.trainer.gotoAndPlay(2);
			_room8.traineeP5.gotoAndPlay(2);
			_room8.traineeP6.gotoAndPlay(2);
			_room8.trainer.alpha = 1;
			_room8.traineeP5.alpha = 1;
			_room8.traineeP6.alpha = 1;
			_room8.trainer.visible = true;
			_room8.traineeP5.visible = true;
			_room8.traineeP6.visible = true;

			_room8.traineeP2.gotoAndStop(1);
			_room8.traineeP3.gotoAndStop(1);
			_room8.traineeP2.alpha = 1;
			_room8.traineeP3.alpha = 1;
			_room8.traineeP2.visible = false;
			_room8.traineeP3.visible = false;
			_room8.traineeF.gotoAndStop(1);
			_room8.traineeM.gotoAndStop(1);
			_room8.traineeF.alpha = 1;
			_room8.traineeM.alpha = 1;
			_room8.traineeF.visible = false;
			_room8.traineeM.visible = false;
		}
		
		public function hideTrainees():void
		{
			if (_room8.trainer.visible) {
				_room8.trainer.stop();
				TweenLite.to(_room8.trainer, .5, {delay:0, alpha:0});
			}
			
			if (_room8.traineeP5.visible) {
				_room8.traineeP5.stop();
				TweenLite.to(_room8.traineeP5, .5, {delay:0, alpha:0});
			}
			
			if (_room8.traineeP6.visible) {
				_room8.traineeP6.stop();
				TweenLite.to(_room8.traineeP6, .5, {delay:.3, alpha:0});
			}
		}
		
		private function showMemberTrainees():void
		{
			_room8.trainer.gotoAndPlay(2);
			_room8.trainer.visible = true;
			_room8.traineeP2.alpha = 0;
			_room8.traineeP2.visible = true;
			_room8.traineeP3.alpha = 0;
			_room8.traineeP3.visible = true;
			
			TweenLite.to(_room8.traineeP2, .5, {delay:0, alpha:1, onStart:function(){_room8.smokeMCM.gotoAndPlay(2);_soundController.playPoof();}, onComplete: function(){_room8.traineeP2.gotoAndPlay(2);}});
			TweenLite.to(_room8.traineeP3, .5, {delay:.5, alpha:1, onStart:function(){_room8.smokeMC3.gotoAndPlay(2);_soundController.playPoof();}, onComplete: function(){_room8.traineeP3.gotoAndPlay(2);}});
		}
		
		private function hideMemberTrainees():void
		{
			TweenLite.to(_room8.traineeP2, .5, {delay:0, alpha:0, onStart:function(){_room8.smokeMCM.gotoAndPlay(2);_soundController.playPoof();}, onComplete: function(){_room8.traineeP2.gotoAndStop(1);_room8.traineeP2.visible = false;}});
			TweenLite.to(_room8.traineeP3, .5, {delay:.5, alpha:0, onStart:function(){_room8.smokeMC3.gotoAndPlay(2);_soundController.playPoof();}, onComplete: function(){_room8.traineeP3.gotoAndStop(1);_room8.traineeP3.visible = false;}});
			TweenLite.to(_room8.trainer, .5, {delay:1, alpha:0, onComplete: function(){_room8.trainer.stop();_room8.trainer.visible = false;}});
		}
		
		private function showNewHireTrainees():void
		{
			_room8.trainer.play();
			_room8.trainer.alpha = 1;
			_room8.trainer.visible = true;
			
			_room8.traineeF.alpha = 0;
			_room8.traineeF.visible = true;
			_room8.traineeM.alpha = 0;
			_room8.traineeM.visible = true;
			
			TweenLite.to(_room8.traineeF, .5, {delay:0, alpha:1, onStart:function(){_room8.smokeMCF.gotoAndPlay(2);_soundController.playPoof();}, onComplete: function(){_room8.traineeF.gotoAndPlay(2);}});
			TweenLite.to(_room8.traineeM, .5, {delay:.5, alpha:1, onStart:function(){_room8.smokeMCM.gotoAndPlay(2);_soundController.playPoof();}, onComplete: function(){_room8.traineeM.gotoAndPlay(2);}});
		}
		
		private function hideNewHireTrainees():void
		{
			if (_room8.traineeF.visible)
			TweenLite.to(_room8.traineeF, .5, {delay:0, alpha:0, onStart:function(){_room8.smokeMCF.gotoAndPlay(2);_soundController.playPoof();},  onComplete: function(){_room8.traineeF.gotoAndStop(1);_room8.traineeP2.visible = false;}});	//female P5
			if (_room8.traineeM.visible)
			TweenLite.to(_room8.traineeM, .5, {delay:.5, alpha:0, onStart:function(){_room8.smokeMCM.gotoAndPlay(2);_soundController.playPoof();},  onComplete: function(){_room8.traineeM.gotoAndStop(1);_room8.traineeP3.visible = false;}});	//male	P6
			if (_room8.trainer.visible)
			TweenLite.to(_room8.trainer, .5, {delay:1, alpha:0, onComplete: function(){_room8.trainer.stop();_room8.trainer.visible = false;}});		//trainer
		}
		
		private function startSampleGame():void
		{
			trace("INFO GameView :: startSampleGame");	
			
			//---------------------------------------------//
			//-- to secure some weird bug
			//---------------------------------------------//
			clearInterval(_loadNextIconIntervalID);
			killDocsInActivePlayers();
			killCurrentDoc();
			//---------------------------------------------//
			
			_isSamplePlay = true;
			
			if (Configuration.isTesting)
			_bkg.iconCounter.visible = false;
			
			//-- show taskBar text
			_taskBar.textMC.alpha = 1;
			_taskBar.textMC.visible = true;
			TweenLite.to(_taskBar.textMC, .75, {delay:.3, alpha:0, onComplete:function(){_taskBar.textMC.visible=false;}});
			
			//-- set sampleTaskIcon
			_sampleTaskIcon = new TaskIcon(0, false, true);
			_sampleTaskIcon.x = START_ICON_X;
			_sampleTaskIcon.y = START_ICON_Y;
			_sampleTaskIcon.active = false;
			_sampleTaskIcon.alpha = 0;
			addChild(_sampleTaskIcon);
			_currentTaskIcon = _sampleTaskIcon;
			_currentTaskID = _currentTaskIcon.taskID;
			
			//-- _introPopup
			_introPopup.scaleX = _introPopup.scaleY = 0;
			_introPopup.alpha = 0;
			_introPopup.visible = true;
			
			//-- init animations
			TweenLite.to(_introPopup, .5, {alpha:1, scaleX:1, scaleY:1, ease:Quint.easeOut});
			TweenLite.to(_sampleTaskIcon, .4, {delay:1, x:INIT_ICON_X, alpha:1, tint:ACTIVE_COLOR, ease:Quad.easeOut, onComplete:onCurrentTaskIconTweenComplete});
			
			//-- show hint for task0
			TweenLite.delayedCall(1.25, showDocInCloud, [0]);
			TweenLite.delayedCall(1.6, showTextBubble, [0, CLOUD_DOC_X + 15, CLOUD_DOC_Y - 38]);
			TweenLite.delayedCall(2.2, showClueOnTheDestPlayer, [_player3]);
		}
		
		private function showStopWatch():void
		{
			_room7.person.gotoAndPlay("fixing");
			
			//-- stopWatch
			var targetY:Number = 145;
			_stopWatch.y = -150;
			_stopWatch.visible = true;
			_stopWatch.addEventListener(GameEvent.DROP_INTERVAL, onDropIntervalRequested);
			_stopWatch.addEventListener(GameEvent.ACTIVATE_AUTOBOT, activateAction);
			_stopWatch.addEventListener(GameEvent.GAME_END, gameEnd);
			
			_player3.removeTask();
			_player3.activatePlayer();
			
			TweenLite.to(_stopWatch, .5, {delay:.5, y:targetY, ease:Back.easeOut});
			TweenLite.to(_welldonePopup, .5, {delay:1, scaleY:0, alpha:0, ease:Quint.easeOut, onComplete:function(){_welldonePopup.visible = false;}});
			TweenLite.delayedCall(1.3, startMainGame);
		}
		
		private function startMainGame():void
		{
			trace("INFO GameView :: startMainGame, setInterval loadNextIcon");
			
			_isSamplePlay = false;
			_stopWatch.start();
			
			_metricPanel1.reset();
			_currentMetricPanel = _metricPanel1;
			
			_currentTaskIcon = _taskIconArr[0];
			_currentTaskIcon.active = true;
			_currentTaskID = _currentTaskIcon.taskID;
			
			_room7.person.gotoAndPlay("fixing");
			_soundController.playTick();
			
			//-- setInterval
			_nextIconIndex = 0;
			_iconLoadingCounter = 0;
			loadNextIcon();
			_loadNextIconIntervalID = setInterval(loadNextIcon, _dataModel.iconLoadInInterval);
			
			if (_isTesting) {
				_bkg.iconCounter.visible = true;
			}
		}
		
		private function showAutobot():void
		{
			_autobotServer.visible = true;
			_autobotServer.alpha = 0;
			TweenLite.to(_autobotServer, .2, {alpha:1, ease:Quint.easeOut});
		}
		
		private function shiftTasks():void
		{
//			trace("-----------------------------------------------------------------");
//			trace("INFO GameView :: shiftTasks, _currentTaskID:" + _currentTaskID);
			
			if (_isSamplePlay|| _isGameEnd || _isAutotaskDone ) return;
			
			hideTextBubble();
			
			_isPhase1 = true;
			_activePlayers = [];
			
			//-- popup the first(current) icon and add it to the last in the queue, and set it inactive
			_currentTaskIcon = _taskIconArr.shift();
			_taskIconArr.push(_currentTaskIcon);
			_currentTaskIcon.active = false;
			
			//-- set the last icon at the start location (leftmost pos. on the taskbar)
			_currentTaskIcon.visible = false;
			_currentTaskIcon.x = _currentTaskIcon.targetX = START_ICON_X;
			
			//-- reset the current to the first icon in the queue
			_currentTaskIcon = _taskIconArr[0];		
			_currentTaskIcon.active = true;
			_currentTaskID = _currentTaskIcon.taskID;
			
			//-- shift the icons that are already visible, to the right
			var targetX:Number;
			for (var i:uint=0; i<_totalTasks; i++) {
				var taskIcon:TaskIcon = _taskIconArr[i];
				if (taskIcon.targetX == START_ICON_X) {
					_nextIconIndex = i;
					break; 
				}
				//-- if in the active icon queue, shift to the right by (ICON_W + Offset)
				targetX = INIT_ICON_X - i*(ICON_W + ICON_OFFSET_X);
				taskIcon.targetX = targetX;
				taskIcon.alpha = 1;
				TweenLite.killTweensOf(taskIcon);
				if (i == 0) {
					//-- currentTaskIcon
					TweenLite.to(taskIcon, .25, {delay:.1*i, alpha:1, x:targetX, ease:Quad.easeOut, onComplete:onCurrentTaskIconTweenComplete});
				}  
				else {
					TweenLite.to(taskIcon, .25, {delay:.1*i, alpha:1, x:targetX, ease:Quad.easeOut});
				}
			}//for

			//if (_loadNextIconIntervalID == 0) {
			if (_isIconRecycling) {
				trace("INFO GameView :: shiftTasks, _isIconRecycling is true, recycling");
				//-- starts recycling, when finishes cycling all the icons in the original array
				clearInterval(_loadNextIconIntervalID);
				if (!_isAutotaskDone) {
					trace("INFO GameView :: shiftTasks, setInterval loadNextIcon");
					_loadNextIconIntervalID = setInterval(loadNextIcon, _dataModel.iconLoadInInterval);
				}
			}//if
		}
		
		private function loadNextIcon():void
		{
//			trace("----------------");
			trace("\nINFO GameView :: 1 loadNextIcon, next iconLoadingCounter is " + (_iconLoadingCounter+1));
			
			if (!_isAutobotPlay) {
				//-- regular play
				if ( _nextIconIndex >= _totalTasks ) {
					trace("INFO GameView :: loadNextIcon, isIconRecycling .....");
					//-- all icons are visible, when a new set of task starts
					clearInterval(_loadNextIconIntervalID);
					//_loadNextIconIntervalID = 0;
					_isIconRecycling = true;
					return;
				}
			} else {
				//-- autobot sequence, _isAutobotPlay
//				trace("INFO GameView :: loadNextIcon, _isAutobotPlay && _iconLoadingCounter:" + _iconLoadingCounter);
				if (!_isVideoSession && (_iconLoadingCounter == 50)) {	//-- 1sec * 50 = 50 sec	5/16/14
					trace("INFO GameView :: loadNextIcon, !_isVideoSession && (_iconLoadingCounter == 50)");
					_startDemoCount = 50;
					_isVideoSession = true;
					dispatchEvent(new GameEvent(GameEvent.START_DEMO));
				} else if (!_isAutotaskDone && (_iconLoadingCounter == (_startDemoCount + 2)) ) {
					//-- 2 more icons loaded and autobot sequence performed behind the video, then end
					trace("INFO GameView :: loadNextIcon, callling destroyGotoVideoButton, startDemoCount:" + _startDemoCount);
					trace("INFO GameView :: loadNextIcon, callling destroyGotoVideoButton, _loadNextIconIntervalID:" + _loadNextIconIntervalID);
					clearInterval(_loadNextIconIntervalID);
					destroyGotoVideoButton();
					_isAutotaskDone = true;
					_isDropIntervalRequested = false;

					//-- reset icon location
					for (var k:uint = 0; k<_totalTasks; k++) {
						var taskIcon:TaskIcon = _taskIconArr[k];
						if (taskIcon) {
							trace(k + ", 1 taskIcon.x is " + taskIcon.x);
							if (taskIcon.x > START_ICON_X) {
								taskIcon.x = INIT_ICON_X - k*(ICON_W + ICON_OFFSET_X);
							} else {
								taskIcon.alpha = 0;
							}
							//trace(k + ", 2 taskIcon.x is " + taskIcon.x);
						}
					}
					return;
				} else if (_isAutotaskDone) {
					clearInterval(_loadNextIconIntervalID);
					return;
				}
			}

			_iconLoadingCounter++;
			if (_isTesting) _iconCounterField.text = _iconLoadingCounter + "";
			trace("\nINFO GameView :: 2 loadNextIcon, iconLoadingCounter is " + _iconLoadingCounter);

			if ((_iconLoadingCounter > _totalTasks) && !_isAutobotPlay) {
				//-- shuffle after cycling the 1st set of selected tasks
				var firstHalfArr:Array = _taskIconArr.slice(0, (_nextIconIndex+1));
				var lastHalfArr:Array = _taskIconArr.slice((_nextIconIndex+1));
				var shuffledLastHalf:Array = ArrayUtils.shuffle(lastHalfArr);
				_taskIconArr = firstHalfArr.concat(shuffledLastHalf);
			}
			var targetX:Number = INIT_ICON_X - (_nextIconIndex)*(ICON_W + ICON_OFFSET_X);
			var nextIcon:TaskIcon = _taskIconArr[_nextIconIndex];
			if (nextIcon) {
				nextIcon.visible = true;
				nextIcon.targetX = targetX;
				nextIcon.active = (nextIcon == _currentTaskIcon)? true:false;
				//the last one in the queue, for not being looked overlapped
				if (_nextIconIndex == 9) nextIcon.alpha = 0;
				TweenLite.killTweensOf(nextIcon);
				if (nextIcon.active) {
					TweenLite.to(nextIcon, .4, {x:targetX, ease:Quad.easeOut, onComplete:onCurrentTaskIconTweenComplete});
				} else {
					TweenLite.to(nextIcon, .4, {x:targetX, ease:Quad.easeOut});
				}
			}
			_nextIconIndex++;
			if (_isAutobotPlay && (_nextIconIndex >= _totalTasks)) _nextIconIndex = (_totalTasks-1);
//			trace("INFO GameView :: loadNextIcon, 2 _nextIconIndex is " + _nextIconIndex);
			if (_isDropIntervalRequested) {
				trace("INFO GameView :: _isDropIntervalRequested, setInterval loadNextIcon");
				_isDropIntervalRequested = false;
				clearInterval(_loadNextIconIntervalID);
				_dataModel.decreaseIconLoadInInterval();	//interval decreases, from 4 to 3, or from 3 to 2
				_loadNextIconIntervalID = setInterval(loadNextIcon, _dataModel.iconLoadInInterval);
			}
		}
		
		private function onCurrentTaskIconTweenComplete():void
		{
//			trace("INFO GameView :: onCurrentTaskIconTweenComplete!!!!!!, _currentTaskIcon is " + _currentTaskIcon + ", _currentTaskID is " + _currentTaskID);
			
			_currentTaskID = _currentTaskIcon.taskID;
			_currentTaskIcon.active = true;

			if (_currentTaskIcon.isAutomated) {
				automateTasksByAutobot(_currentTaskID);
			} else if (!_isGameEnd) {
				if (!_isSamplePlay) {
					showHint(_currentTaskID);
				}
				startProductionTimer();
			}
		}
		
		private function showTextBubble(taskID:uint, pX:Number, pY:Number):void
		{
//			trace("INFO GameView :: showTextBubble");
			
			if (_textBubble == null) {
				_textBubble = new TextBubble(taskID);
				addChild(_textBubble);
			} else {
				_textBubble.isMatching = !_isPhase1;
				_textBubble.taskID = taskID;
			}
			_textBubble.x = pX;
			_textBubble.y = pY;
			_textBubble.show();
		}
		
		private function hideTextBubble():void
		{
			//trace("INFO GameView :: hideTextBubble");
			if (_textBubble) _textBubble.hide();
		}
		
		private function glowOn(target:*, glowColor:Number = SOURCE_COLOR, pDelay:Number = 0, isFading:Boolean = false, initAlpha:Number = 0):void
		{
			//trace("INFO GameView :: glowOn, " + target);
			TweenMax.killTweensOf(target);
			if (isFading) {
				target.alpha = initAlpha;
			} 
			TweenMax.to(target, .5, {delay:pDelay, alpha:1, ease:Quad.easeOut, glowFilter:{color:glowColor, alpha:.8, blurX:15, blurY:15, strength:2}});
		}
		
		private function glowOff(target:*):void
		{
			//trace("INFO GameView :: glowOff, " + target);
			TweenMax.killTweensOf(target);
			TweenMax.to(target, .3, {glowFilter:{alpha:0}});
		}
			
		private function showHint (taskID:uint):void
		{
//			trace("INFO GameView :: showHint, taskID:" + taskID);
			
			_currentTaskID = taskID;
			var delayDuration:Number = .5;
			var bubbleX:Number;
			var bubbleY:Number;
			switch (taskID) {
				/////////////
				//category0//
				/////////////
				case 0:
				case 1:
					//-- from the cloud to any active players, or to autobot server when automated
					showDocInCloud(taskID);
					bubbleX = CLOUD_DOC_X + 15;
					bubbleY = CLOUD_DOC_Y - 38;
					TweenLite.delayedCall(.2, showTextBubble, [taskID, bubbleX, bubbleY]);
					setActivePlayers();
					TweenLite.delayedCall(delayDuration, showClueOnRandomAvailPlayer);
					break;
				case 2:
					//-- from the server to any active players
					showDocInServerRoom(taskID);
					bubbleX = SERVER_DOC_X + 15;
					bubbleY = SERVER_DOC_Y - 38;
					TweenLite.delayedCall(.2, showTextBubble, [taskID, bubbleX, bubbleY]);
					setActivePlayers();
					TweenLite.delayedCall(delayDuration, showClueOnRandomAvailPlayer);
					break;
				/////////////
				//category2//
				/////////////
				case 6:	
				case 8:
					//-- from active players to the server
					setActivePlayers();
					showDocsInActivePlayers(taskID);
					if (_docArr[0]) {
						bubbleX = _docArr[0].x + 15;
						bubbleY = _docArr[0].y - 38;
						TweenLite.delayedCall(.2, showTextBubble, [taskID, bubbleX, bubbleY]);
					}
					TweenLite.delayedCall(delayDuration, showClueOnTheDestPlayer, [_serverIT]);
					break;
				case 7:
					setActivePlayers();
					showDocsInActivePlayers(taskID);
					bubbleX = _docArr[0].x + 15;
					bubbleY = _docArr[0].y - 38;
					TweenLite.delayedCall(.2, showTextBubble, [taskID, bubbleX, bubbleY]);
					TweenLite.delayedCall(delayDuration, showCloud);
					break;
				/////////////
				//category1//
				/////////////
				case 3:
				case 4:
					//-- from the server to supervisor, for matching, then to manager
					if (_isPhase1) {
						showDocInServerRoom(taskID);
						bubbleX = SERVER_DOC_X + 15;
						bubbleY = SERVER_DOC_Y - 38;
						TweenLite.delayedCall(.2, showTextBubble, [taskID, bubbleX, bubbleY]);
						TweenLite.delayedCall(delayDuration, showMatchingDocInSupervisor, [taskID]);
					} else {
//						trace("phase2!!!!!");
						showClueOnTheDestPlayer(_supervisor);
						TweenLite.delayedCall(delayDuration, showClueOnTheDestPlayer, [_manager]);
					}
					break;
				case 5:
					//-- from manager to supervisor, for matching, then to avail players
					if (_isPhase1) {
						//trace("task5 phase1!!!!!");
						showDocInManager(taskID);
						bubbleX = MANAGER_DOC_X + 15;
						bubbleY = MANAGER_DOC_Y - 38;
						TweenLite.delayedCall(.2, showTextBubble, [taskID, bubbleX, bubbleY]);
						TweenLite.delayedCall(delayDuration, showMatchingDocInSupervisor, [taskID]);
					} else {
						//trace("task5 phase2!!!!!");
						showClueOnTheDestPlayer(_supervisor, false);
						setActivePlayers();
						TweenLite.delayedCall(delayDuration, showClueOnRandomAvailPlayer);
					}
					break;
				/////////////
				//category3//
				/////////////
				case 9:
					//-- from the server to supervisor, then to manager
					if (_isPhase1) {
						showDocInServerRoom(taskID);
						bubbleX = SERVER_DOC_X + 15;
						bubbleY = SERVER_DOC_Y - 38;
						TweenLite.delayedCall(.2, showTextBubble, [taskID, bubbleX, bubbleY]);
						TweenLite.delayedCall(delayDuration, showClueOnTheDestPlayer, [_supervisor]);
					} else {
						//-- phase2 task9
						TweenLite.delayedCall(delayDuration, showClueOnTheDestPlayer, [_manager]);
					}
					break;
			}//switch
		}//function
		
		private function hideHint():void
		{
//			trace("INFO GameView :: hideHint, taskID:");
			
			hideTextBubble();
			killMatchingDoc();
			killDocsInActivePlayers();
			killCurrentDoc();
			hideClueOnTheDestPlayer();
			hideAvailPlayerRooms();
		}
				
		private function showDestination():void
		{
//			trace("INFO GameView :: showDestination, taskID-" + _currentTaskID);
			
			var isAutomated:Boolean = _currentTaskIcon.isAutomated;
			switch (_currentTaskID){
				case 0:
				case 1:
				case 2:
					if (isAutomated) {
						showAutobotServer();
					} else {
						setActivePlayers();
						showAvailPlayerRooms();
					}
					break;
				case 7:
					showCloud();
					break;
				case 9:
					if (_isPhase1 && isAutomated) {
						showAutobotServer();
					} 
					break;
			}//switch
		}
		
		private function hideDestination():void
		{
//			trace("INFO GameView :: hideDestination, taskID-" + _currentTaskID);
			
			var isAutomated:Boolean = _currentTaskIcon.isAutomated;
			switch (_currentTaskID) {
				case 0:
				case 1:
				case 2:
					if (isAutomated) {
						hideClueOnTheDestPlayer();
					} else {
						hideAvailPlayerRooms();
					}
					break;
				case 6:	
				case 8:
					hideClueOnTheDestPlayer();
					break;
				case 7:
					hideCloud();
					break;
				case 9:
					if (isAutomated) {
						hideClueOnTheDestPlayer();
					}
					break;
			}//switch
		}
		
		private function createDoc(roomID:String = "cloud", docID:uint = 1, taskID:uint = 0):void
		{
			//trace("INFO GameView :: createDoc, taskID:" + taskID);
			
			//-- from general document to a specific icon by taskID
			var targetDoc:TaskIcon = new TaskIcon(taskID);
			switch (roomID.toLowerCase())
 			{
				case "cloud":
					targetDoc.x = CLOUD_DOC_X;
					targetDoc.y = CLOUD_DOC_Y;
					break;
				case "server":
					targetDoc.x = SERVER_DOC_X;
					targetDoc.y = SERVER_DOC_Y;
					break;
				case "supervisor":
					targetDoc.x = SUPERVISOR_DOC_X;
					targetDoc.y = SUPERVISOR_DOC_Y;
					break;
				case "manager":
					targetDoc.x = MANAGER_DOC_X;
					targetDoc.y = MANAGER_DOC_Y;
					break;
				case "autobot":
					targetDoc.x = AUTOBOT_DOC_X;
					targetDoc.y = AUTOBOT_DOC_Y;
					break;
				default:
					trace("ERROR GameView :: createDoc, roomID is not in the list");
					return;
			}
			targetDoc.scaleX = targetDoc.scaleY = 1.5;
			targetDoc.targetX = targetDoc.x;
			targetDoc.targetY = targetDoc.y;
			targetDoc.buttonMode = targetDoc.mouseEnabled = !_isAutobotPlay;
			if (_currentTaskIcon.isAutomated) targetDoc.buttonMode = targetDoc.mouseEnabled = false;
			targetDoc.mouseChildren = false;
			addChild(targetDoc);
			
			if (docID == 1) {
				_doc1 = targetDoc;
			} else {
				_doc2 = targetDoc;
				if (_doc1) {
					if (getChildIndex(_doc1)<getChildIndex(_doc2)) swapChildren(_doc1, _doc2);
				}
			}
		}

		private function showDocIn(roomID:String = "cloud", taskID:uint = 0):void
		{
			var targetX:Number;
			var targetY:Number;
			switch (roomID.toLowerCase())
 			{
				case "cloud":
					targetX = CLOUD_DOC_X;
					targetY = CLOUD_DOC_Y;
					break;
				case "server":
					targetX = SERVER_DOC_X;
					targetY = SERVER_DOC_Y;
					break;
				case "manager":
					targetX = MANAGER_DOC_X;
					targetY = MANAGER_DOC_Y;
					break;
				case "autobot":
					targetX = AUTOBOT_DOC_X;
					targetY = AUTOBOT_DOC_Y;
					break;
				default:
					trace("ERROR showDoc, roomID is not in the list");
					return;
			}	
			if (_doc1) {
				_doc1.x = targetX;
				_doc1.y = targetY;
				_doc1.visible = true;
			} else {
				createDoc(roomID, 1, taskID);
			}
			if (!_isAutobotPlay && !_currentTaskIcon.isAutomated) {
				_doc1.addEventListener(MouseEvent.MOUSE_DOWN, onDoc1DownHandler);
			}
			_currentDoc = _doc1;
			_currentDoc.alpha = 0;
			glowOn(_currentDoc, DEST_COLOR, 0.25, true);
		}
		
		private function showDocInCloud(taskID:uint):void
		{
			showDocIn("cloud", taskID);
		}
		
		private function showDocInServerRoom(taskID:uint):void
		{
			showDocIn("server", taskID);
		}
		
		private function showDocInManager(taskID:uint):void
		{
			showDocIn("manager", taskID);
		}
		
		private function showDocInAutobotServer(taskID:uint):void
		{
			showDocIn("autobot", taskID);
		}
		
		private function showDocsInActivePlayers(taskID:uint):void
		{
			//trace("INFO GameView :: showDocsInActivePlayers, _docArr.length is " + _docArr.length);
			if (_docArr.length == 0) {
				var total:uint = _activePlayers.length;
				if (total == 0) {
					trace("ERROR GameView :: showDocsInActivePlayers, no active players left!!!!!");
					_supervisor.activatePlayer();
					_activePlayers = [_supervisor];
					total = 1;
				}
				for (var i:uint = 0; i<total; i++) {
					var player:Player = _activePlayers[i];
					var doc:TaskIcon = new TaskIcon(taskID);
					doc.x = player.x;
					doc.y = player.y;
					doc.targetX = doc.x;
					doc.targetY = doc.y;
					doc.scaleX = doc.scaleY = 1.5;
					doc.buttonMode = doc.mouseEnabled = true;
					doc.mouseChildren = false;
					doc.alpha = 0;
					doc.addEventListener(MouseEvent.MOUSE_DOWN, onDocsDownHandler);
					addChild(doc);
					_docArr.push(doc);
					glowOn(doc, SOURCE_COLOR, .3*i, true);
				}
			} else {
				var docTotal:uint = _docArr.length;
				for (var j:uint = 0; j<docTotal; j++) {
					var targetDoc:TaskIcon = _docArr[j];
					targetDoc.alpha = 0;
					glowOn(targetDoc, SOURCE_COLOR, .3*j, true);
				}
			}
		}
		
		private function killDocsInActivePlayers():void
		{
			var docTotal:uint = _docArr.length;
			if (docTotal > 0) {
				trace("INFO GameView :: killDocsInActivePlayers, _docArr.length is " + _docArr.length);
				
				for (var j:uint = 0; j<docTotal; j++) {
					var doc:TaskIcon = _docArr[j];
					if (doc) {
						doc.removeEventListener(MouseEvent.MOUSE_DOWN, onDocsDownHandler);
						TweenLite.killTweensOf(doc);
						TweenLite.to(doc, .3, {delay:j*.2, alpha:0, onComplete:function() {doc.visible = false;doc.destroy();if (contains(doc)) removeChild(doc);}});
					}
				}
			}
			_docArr = [];
		}
		
		private function showMatchingDocInSupervisor(taskID:uint):void
		{
//			trace("INFO GameView :: showMatchingDocInSupervisor");
			if (_doc2) {
				_doc2.x = SUPERVISOR_DOC_X;
				_doc2.y = SUPERVISOR_DOC_Y;
				_doc2.alpha = 1;
				_doc2.visible = true;
			} else {
				createDoc("supervisor", 2, taskID);
			}
			_matchingDoc = _doc2;
			_matchingDoc.alpha = 1;
			_matchingDoc.visible = true;
			glowOn(_matchingDoc, DEST_COLOR, .25);
			
			var isShowingIcon:Boolean = !_currentTaskIcon.isAutomated;
			showClueOnTheDestPlayer(_supervisor, isShowingIcon);
		}
		
		private function showMatchingDocInAutobotServer(taskID:uint):void
		{
//			trace("INFO GameView :: showMatchingDocInAutobotServer");
			if (_doc2) {
				_doc2.x = AUTOBOT_DOC_X;
				_doc2.y = AUTOBOT_DOC_Y;
				_doc2.alpha = 1;
				_doc2.visible = true;
			} else {
				createDoc("autobot", 2, taskID);
			}
			_matchingDoc = _doc2;
			_matchingDoc.alpha = 1;
			_matchingDoc.visible = true;
			glowOn(_matchingDoc, DEST_COLOR, .25);
			
			showClueOnTheDestPlayer(_autobotPlayer, false);
		}
		
		private function killMatchingDoc():void
		{
//			trace("INFO GameView :: killMatchingDoc");
			if (_matchingDoc) {
				if (contains(_matchingDoc)) removeChild(_matchingDoc);
				_matchingDoc = null;
				_doc2 = null;
			}
		}

		private function setActivePlayers(exceptIndex:int = -1):void
		{
//			trace("INFO GameView :: setActivePlayers");
			_activePlayers = [];
			var activePlayerIDs:Array = _dataModel.activePlayerIDs;
			var total:uint = activePlayerIDs.length;
			if (total == 0) {
				trace("ERROR GameView :: setActivePlayers, this shouldn't happen!!");
				_supervisor.activatePlayer();
				activePlayerIDs = [1];
				total = 1;
			}
			for (var i:uint = 0; i<total; i++) {
				var id:uint = activePlayerIDs[i];
				if (id != exceptIndex) {
					var player:Player = _players[id];
					_activePlayers.push(player);
				}
			}
		}
		
		private function showClueOnRandomAvailPlayer(isShowingIcon:Boolean = true):void
		{
//			trace("INFO GameView :: showClueOnRandomAvailPlayer, _currentTaskID:" + _currentTaskID);
			var total:uint = _activePlayers.length;
			var index:uint = Math.floor(Math.random()*total);
			var randomPlayer:Player = _activePlayers[index];
			showClueOnTheDestPlayer(randomPlayer, isShowingIcon);
		}
		
		private function showClueOnTheDestPlayer(targetPlayer:Player, isShowingIcon:Boolean = true):void
		{
			if (!_isShowingClue) {
				if (targetPlayer) {
//					trace("INFO GameView :: showClueOnTheDestPlayer, playerID:" + targetPlayer.playerID);
					targetPlayer.showClue(_currentTaskID, isShowingIcon);
					_isShowingClue = true;
					_destPlayer = targetPlayer;
				}
			}
		}
		
		private function hideClueOnTheDestPlayer():void
		{
//			trace("INFO GameView :: hideClueOnTheDestPlayer");	
			if (_isShowingClue && _destPlayer) {
				_destPlayer.hideClue();
				_isShowingClue = false;
				_destPlayer = null;
			}
		}
		
		private function showAvailPlayerRooms():void
		{
//			trace("INFO GameView :: showAvailPlayerRooms, activePlayer total is " + _activePlayers.length);
			var total:uint = _activePlayers.length;
			for (var i:uint = 0; i<total; i++) {
				if (_activePlayers[i] != _destPlayer) {
					var roomID:uint = Player(_activePlayers[i]).playerID;
					var target:MovieClip = this["_room" +roomID];
					glowOn(target, DEST_COLOR);
				}
			}
		}
		
		private function hideAvailPlayerRooms():void
		{
//			trace("INFO GameView :: hideAvailPlayerRooms");
			var total:uint = _activePlayers.length;
			for (var i:uint = 0; i<total; i++) {
				var roomID:uint = Player(_activePlayers[i]).playerID;
				var target:MovieClip = this["_room" +roomID];
				glowOff(target);
			}
		}
		
		private function hideAllPlayerRooms():void
		{
//			trace("INFO GameView :: hideAllPlayerRooms");
			for (var i:uint = 0; i<8; i++) {
				var target:MovieClip = this["_room" + i];
				glowOff(target);
			}
		}

		private function showCloud():void
		{
			glowOn(_cloud, DEST_COLOR);
		}
		
		private function hideCloud():void
		{
			glowOff(_cloud);
		}
			
		private function showAutobotServer():void
		{
			showClueOnTheDestPlayer(_autobotPlayer, false);
		}
				
		private function killCurrentDoc():void
		{
			if (_currentDoc != null) {
				trace("INFO GameView :: killCurrentDoc");
				_currentDoc.destroy();
				if (contains(_currentDoc)) removeChild(_currentDoc);
				if (_currentDoc == _doc1) {
					_currentDoc.removeEventListener(MouseEvent.MOUSE_DOWN, onDoc1DownHandler);
					_doc1 = null;
				} else if (_currentDoc == _doc2) {
					_currentDoc.removeEventListener(MouseEvent.MOUSE_DOWN, onDoc2DownHandler);
					_doc2 = null;
				} else {
					_currentDoc.removeEventListener(MouseEvent.MOUSE_DOWN, onDocsDownHandler);
				}
			}
			_currentDoc = null;
			_docArr = [];
		}
		
		private function backToOrgOnFail():void
		{
			_currentTaskIcon.active = true;
		}
		
		private function onMatchedInSupervisor():void
		{
//			trace("INFO GameView :: onMatchedInSupervisor, _currrentTaskID is " + _currentTaskID);
			_currentTaskIcon.active = true;
			if (_currentTaskID == 5) {
				//5
				setActivePlayers(1);
				showClueOnRandomAvailPlayer(false);
			} else {
				//3,4,and 9
				showClueOnTheDestPlayer(_manager, false);
			}
			
			if (_currentTaskID != 9) {
				//3,4,5
				killCurrentDoc();
				_currentDoc = _matchingDoc;
				_currentDoc.alpha = 1;
				_matchingDoc.addEventListener(MouseEvent.MOUSE_DOWN, onDoc2DownHandler);
			} 
			var bubbleX:Number = _currentDoc.x + 15;
			var bubbleY:Number = _currentDoc.y - 38;
			showTextBubble(_currentTaskID, bubbleX, bubbleY);
		}
		
		private function startProductionTimer():void
		{
			trace("INFO GameView :: startProductionTimer, set _taskStartTime, _currentTaskID:" + _currentTaskID);
			_taskStartTime = getTimer();
			clearInterval(_evaluateProductionID);
			_evaluateProductionID = setInterval(evaluateProduction, 3000);	//5000 to 3000, 05/16/14
		}
		
		private function evaluateProduction():void
		{
			var now:Number = getTimer();
			var elapsedTime:Number = now - _taskStartTime;
			//trace("INFO GameView :: evaluateProduction, elapsedTime is " + elapsedTime);

			if (elapsedTime>=3000) {	//05/16/14, from 5000 to 3000
				_currentMetricPanel.dropProduction();
			} else {
				clearInterval(_evaluateProductionID);
				_currentMetricPanel.raiseProduction();
			}
		}
		
		private function killCurrentMouseEvents():void
		{
//			trace("INFO GameView :: killCurrentMouseEvents");
			if (_currentDoc) {
				_currentDoc.stopDrag();
				_currentDoc.mouseEnabled = false;
				_currentDoc.removeEventListener(MouseEvent.MOUSE_DOWN, onDoc1DownHandler);
			}
			
			if (_matchingDoc) {
				_matchingDoc.stopDrag();
				_matchingDoc.mouseEnabled = false;
				_matchingDoc.removeEventListener(MouseEvent.MOUSE_DOWN, onDoc2DownHandler);
			}
			
			if (_docArr.length>0) {
				var total:uint = _docArr.length;
				for (var i:uint = 0; i<total; i++) {
					var doc:TaskIcon = _docArr[i];
					doc.mouseEnabled = false;
					doc.removeEventListener(MouseEvent.MOUSE_DOWN, onDocsDownHandler);
				}
			}
		}
		

		////////////////////////////////////////////////////////////////////////////
		// EventHandlers
		////////////////////////////////////////////////////////////////////////////
		private function onDropIntervalRequested(e:GameEvent = null):void
		{
			_isDropIntervalRequested = true;
		}
				
		private function onPlayerActivated(e:Event):void
		{
			var targetPlayer:Player = e.target as Player;
			var id:uint = targetPlayer.playerID;
			if (id>=9) return;	//no targetRoom for the autobot
			
			var targetRoom:MovieClip = this["_room" + id];
			if (targetRoom.person) {
				targetRoom.person.play();
				targetRoom.person.alpha = 1;
				targetRoom.person.visible = true;
				TweenLite.to(targetRoom.person, .3, {tint:null});
			}
			if (targetRoom.person1) {
				targetRoom.person1.alpha = 1;
				targetRoom.person1.visible = true;
				TweenLite.to(targetRoom.person1, .3, {tint:null});
			}
		}
		
		private function onPlayerInactivated(e:Event):void
		{
			var targetPlayer:Player = e.target as Player;
			var id:uint = targetPlayer.playerID;
			if (id>=9) return;	//no targetRoom for the autobot
			
			var targetRoom:MovieClip = this["_room" + id];
			if (id != 7) {
				//-- except the server room
				if (targetRoom.person) {
					targetRoom.person.stop();
					if (!targetPlayer.isTraining) TweenLite.to(targetRoom.person, .3, {tint:INACTIVE_COLOR, ease:Quad.easeOut});
				}
				if (targetRoom.person1) 
				TweenLite.to(targetRoom.person1, .3, {tint:INACTIVE_COLOR});
			} 
			else {
				targetRoom.person.gotoAndStop(1);
			}
		}
		
		private function onDoc1DownHandler(e:MouseEvent):void
		{
//			trace("INFO GameView :: onDoc1DownHandler, _currentDoc is " + _currentDoc);
			
			hideTextBubble();
			if (_isSamplePlay) {
				_currentDoc.startDrag(false, new Rectangle(0, 0, _bkg.width, _bkg.height));
				addEventListener(MouseEvent.MOUSE_UP, onSampleDocUpHandler);
				//-- hide "office task bar" text
//				TweenLite.to(_taskBar.textMC, .5, {delay:.75, alpha:0, onComplete:function(){_taskBar.textMC.visible=false;}});
				//-- hide intro popup
				TweenLite.to(_introPopup, .3, {delay:.75, scaleX:0, scaleY:0, alpha:0, ease:Quint.easeOut, onComplete:function(){_introPopup.visible = false;}});
				return;
			}
			
			addEventListener(MouseEvent.MOUSE_UP, onDoc1UpHandler);
			showDestination();
			
			_currentDoc.startDrag(false, new Rectangle(0, 0, _bkg.width, _bkg.height));
			if (_doc2 && contains(_doc2)) {
				//-- category1 (make decision/process info) tasks:task3,4,5
				if (getChildIndex(_doc1) < getChildIndex(_doc2)) {
					swapChildren(_doc1, _doc2);
				}
			} 
			
			if (_currentTaskID == 9 && !_isPhase1) {
				_supervisor.hideClue();
				showClueOnTheDestPlayer(_manager);
			}
		}
		
		private function onSampleDocUpHandler(e:MouseEvent):void
		{
//			trace("INFO GameView :: onSampleDocUpHandler");
			
			removeEventListener(MouseEvent.MOUSE_UP, onSampleDocUpHandler);
			_currentDoc.stopDrag();

			if (_currentDoc.hitTestObject(_room3)) {
				trace("INFO SUCCESS!!!!! SAMPLE");
				_player3.addTask();
				hideClueOnTheDestPlayer();
				clearInterval(_evaluateProductionID);
				evaluateProduction();
				_currentMetricPanel.raiseQuality();
				killCurrentDoc();
				
				//-- kill _sampleIcon
				TweenLite.to(_sampleTaskIcon, .3, {alpha:0, ease:Quad.easeOut, onComplete:function(){removeChild(_sampleTaskIcon);_sampleTaskIcon = null;}});
				hideTextBubble();
				
//				//-- hide "office task bar" text
//				TweenLite.to(_taskBar.textMC, .5, {alpha:0, onComplete:function(){_taskBar.textMC.visible=false;}});
				
				//-- showWelldonePopup here
				_welldonePopup.alpha = 0;
				_welldonePopup.scaleY = 0;
				_welldonePopup.visible = true;
				TweenLite.to(_welldonePopup, .4, {delay:.75, alpha:1, scaleY:1, ease:Quint.easeOut});
				TweenLite.delayedCall(2.5, showStopWatch);
			} else {
				trace("INFO FAIL!!!!!!!!! SAMPLE");
				TweenLite.to(_currentDoc, .2, {x:_currentDoc.targetX, y:_currentDoc.targetY, ease:Expo.easeOut, onComplete:backToOrgOnFail});
				_currentMetricPanel.dropQuality();
			}
		}

		private function onDoc1UpHandler(e:MouseEvent):void
		{	
//			trace("INFO GameView :: onDoc1UpHandler, _currentTaskID:" + _currentTaskID);

			removeEventListener(MouseEvent.MOUSE_UP, onDoc1UpHandler);
			hideDestination();
			if (_currentDoc) {
				_currentDoc.stopDrag();
			} else {
				return;
			}
			
			var hit:Boolean = false;
			var isAutomated:Boolean = _currentTaskIcon.isAutomated;
			if (_currentTaskID <=2) {
			///////////////////////////////////
			//-- category0 tasks: task0, 1, 2
			///////////////////////////////////
				var targetRoom:MovieClip;
				var hitPlayer:Player;
				if (isAutomated) {
					targetRoom = _autobotServer;
					hitPlayer = _serverIT;
					if (_currentDoc.hitTestObject(targetRoom)) {
						hit = true;
						hitPlayer.addTask();
					}
				} else {
					var total:uint = _activePlayers.length;
					for (var i:uint = 0; i<total; i++) {
						targetRoom = this["_room" + _activePlayers[i].playerID];
						if (_currentDoc.hitTestObject(targetRoom)) {
							hit = true;
							hitPlayer = _activePlayers[i];
							hitPlayer.addTask();
							break;
						}
					}//for
				}//else (isAutomated)
				
				if (hit) {
					trace("INFO SUCCESS!!!!! " + _currentTaskID);
					hideClueOnTheDestPlayer();
					clearInterval(_evaluateProductionID);
					evaluateProduction();
					_currentMetricPanel.raiseQuality();
					_dataModel.setCompleted(_currentTaskID);
					killCurrentDoc();
					shiftTasks();
				} else {
					trace("INFO FAIL!!!!!!!!! " + _currentTaskID);
					TweenLite.to(_currentDoc, .2, {x:_currentDoc.targetX, y:_currentDoc.targetY, ease:Expo.easeOut, onComplete:backToOrgOnFail});
					_currentMetricPanel.dropQuality();
				} // else (hit)
			
			} else if (_currentTaskID == 9) {
			///////////////////////////////////
			//-- category3 task: task9
			///////////////////////////////////
				if (_isPhase1) {
					//-- phase1:from the server to supervisor
					if (_currentDoc.hitTestObject(_room1)) {
						trace("INFO phase1 SUCCESS!!!!! taskID: " + _currentTaskID);
						_isPhase1 = false;	
						evaluateProduction();
						startProductionTimer();		//-- restart the timer
						hideClueOnTheDestPlayer();			
						_currentMetricPanel.raiseQuality();		
						_supervisor.addTask();
						TweenLite.to(_currentDoc, .25, {x:SUPERVISOR_DOC_X, y:SUPERVISOR_DOC_Y, ease:Expo.easeOut, onComplete:onMatchedInSupervisor});
					} else {
						trace("INFO phase1 FAIL!!!!!!!! " + _currentTaskID);
						_isPhase1 = true;
						_currentMetricPanel.dropQuality();
						TweenLite.to(_currentDoc, .2, {x:_currentDoc.targetX, y:_currentDoc.targetY, ease:Expo.easeOut, onComplete:backToOrgOnFail});
					}
				} else {
					//-- phase2
					if (_currentDoc.hitTestObject(_room0)) {
						trace("INFO phase2 SUCCESS!!!!! manager " + _currentTaskID);
						_doc1.removeEventListener(MouseEvent.MOUSE_DOWN, onDoc1DownHandler);
						_manager.addTask();
						clearInterval(_evaluateProductionID);
						evaluateProduction();
						_currentMetricPanel.raiseQuality();
						_dataModel.setCompleted(_currentTaskID);
						hideClueOnTheDestPlayer();
						killCurrentDoc();
						shiftTasks();
					} else {
						trace("INFO FAIL!!!!!!!!! " + _currentTaskID);
						_currentMetricPanel.dropQuality();
						TweenLite.to(_currentDoc, .2, {x:SUPERVISOR_DOC_X, y:SUPERVISOR_DOC_Y, ease:Expo.easeOut, onComplete:backToOrgOnFail});
					}
				}
			} else {
			///////////////////////////////////
			//-- category1 task: task3, 4, 5
			///////////////////////////////////
				if (_currentDoc.hitTestObject(_room1)) {	
					trace("INFO phase1 SUCCESS!!!!! combined _currentTaskID:" + _currentTaskID);
					_isPhase1 = false;
					_supervisor.addTask();
					hideClueOnTheDestPlayer();
					evaluateProduction();
					startProductionTimer();
					_currentMetricPanel.raiseQuality();
					TweenLite.to(_currentDoc, .25, {x:_doc2.x, y:_doc2.y, ease:Expo.easeOut, onComplete:onMatchedInSupervisor});	
				} else {
					//-- phase1
					trace("INFO phase1 FAIL!!!! _currentTaskID:" + _currentTaskID);
					_isPhase1 = true;
					_currentMetricPanel.dropQuality();
					TweenLite.to(_currentDoc, .2, {x:_currentDoc.targetX, y:_currentDoc.targetY, ease:Expo.easeOut, onComplete:backToOrgOnFail});
				}
			}
		}
		
		private function onDoc2DownHandler(e:MouseEvent):void
		{
//			trace("INFO GameView :: onDoc2DownHandler");

			hideTextBubble();
			
			if (_currentTaskID == 5) {
				setActivePlayers(PlayerData.SUPERVISOR_INDEX);
				showAvailPlayerRooms();
				TweenLite.delayedCall(.25, showClueOnRandomAvailPlayer);
			} else {
				//-- task3, task4, or task9
				showClueOnTheDestPlayer(_manager);
			}
			
			addEventListener(MouseEvent.MOUSE_UP, onDoc2UpHandler);
			_currentDoc.startDrag(false, new Rectangle(0, 0, _bkg.width, _bkg.height));
		}
		
		private function onDoc2UpHandler(e:MouseEvent):void
		{	
//			trace("INFO GameView :: onDoc2UpHandler, all happening in supervisor's desk");
			
			removeEventListener(MouseEvent.MOUSE_UP, onDoc2UpHandler);
			if (_currentDoc) {
				_currentDoc.stopDrag();
			} else {
				//at the end of the game
				return;
			}
			
			var hit:Boolean = false;
			if (_currentTaskID == 3 || _currentTaskID == 4 || _currentTaskID == 9) {
				if (_currentDoc.hitTestObject(_room0)) {
					hit = true;
					_manager.addTask();
				}
			} else if (_currentTaskID == 5) {
				var hitPlayer:Player;
				var total:uint = _activePlayers.length;
				var targetRoom:MovieClip;
				for (var i:uint = 0; i<total; i++) {
					targetRoom = this["_room" + _activePlayers[i].playerID];
					if (_currentDoc.hitTestObject(targetRoom)) {
						hit = true;
						hitPlayer = _activePlayers[i];
						hitPlayer.addTask();
						break;
					}
				}
				hideAvailPlayerRooms();
			} else {
				return;
			}

			if (hit) {
				trace("INFO SUCCESS!!!!!, task: " + _currentTaskID);
				hideClueOnTheDestPlayer();
				clearInterval(_evaluateProductionID);
				evaluateProduction();
				_currentMetricPanel.raiseQuality();
				_dataModel.setCompleted(_currentTaskID);
				killCurrentDoc();
				shiftTasks();
			} else {
				trace("INFO FAIL!!!!, task: " + _currentTaskID);
				_currentMetricPanel.dropQuality();
				if (_currentTaskID != 9) {
					TweenLite.to(_currentDoc, .2, {x:_currentDoc.targetX, y:_currentDoc.targetY, ease:Expo.easeOut, onComplete:backToOrgOnFail});
				} else {
					TweenLite.to(_currentDoc, .2, {x:SUPERVISOR_DOC_X, y:SUPERVISOR_DOC_Y, ease:Expo.easeOut, onComplete:backToOrgOnFail});
				}
			}
		}

		private function onDocsDownHandler(e:MouseEvent):void
		{
			//trace("INFO GameView :: onDocsDownHandler, _currentTaskID:" + _currentTaskID);
			
			hideTextBubble();
			
			_currentDoc = e.target as TaskIcon;
			if (_currentTaskID == 7) {
				showCloud();
				_currentDoc.addEventListener(MouseEvent.MOUSE_UP, onDocsUpInCloud);
			} else {
				//-- task6 or task8
				_currentDoc.addEventListener(MouseEvent.MOUSE_UP, onDocsUpInServerRoom);
			}
			
			//-- remove all other docsa
			while (_docArr.length > 1) {
				var doc:TaskIcon = _docArr.shift();
				if (doc != _currentDoc) {
					doc.removeEventListener(MouseEvent.MOUSE_DOWN, onDocsDownHandler);
					doc.destroy();
					removeChild(doc);
				} else {
					_docArr.push(doc);
				}
			}

			_currentDoc.startDrag(false, new Rectangle(0, 0, _bkg.width, _bkg.height));
		}
		
		private function onDocsUpInServerRoom(e:MouseEvent):void
		{
//			trace("INFO GameView :: onDocsUpInServerRoom, taskID: " + _currentTaskID);

			//-- task6 and task8
			_currentDoc.removeEventListener(MouseEvent.MOUSE_UP, onDocsUpInServerRoom);
			if (_currentDoc) {
				_currentDoc.stopDrag();
			} else {
				return;
			}

			if (_currentDoc.hitTestObject(_room7)) {
				trace("INFO SUCCESS!!!!!, onDocsUpInServerRoom - taskID:" + _currentTaskID);
				clearInterval(_evaluateProductionID);
				evaluateProduction();
				_currentMetricPanel.raiseQuality();
				_serverIT.addTask();
				_dataModel.setCompleted(_currentTaskID);
				hideClueOnTheDestPlayer();
				killCurrentDoc();
				shiftTasks();
			} else {
				trace("INFO FAIL!!!!!!!!!, onDocsUpInServerRoom - taskI:" + _currentTaskID);
				_currentMetricPanel.dropQuality();
				TweenLite.to(_currentDoc, .2, {x:_currentDoc.targetX, y:_currentDoc.targetY, ease:Expo.easeOut, onComplete:backToOrgOnFail});
			}
		}
		
		private function onDocsUpInCloud(e:MouseEvent):void
		{
			//trace("INFO GameView :: onDocsUpInCloud");
			
			_currentDoc.removeEventListener(MouseEvent.MOUSE_UP, onDocsUpInCloud);
			_currentDoc.stopDrag();
			if (_currentDoc.hitTestObject(_cloud))  {
				trace("INFO SUCCESS!!!!!, onDocsUpInCloud");
				hideCloud();
				clearInterval(_evaluateProductionID);
				evaluateProduction();
				_currentMetricPanel.raiseQuality();
				_dataModel.setCompleted(_currentTaskID);
				_cloud.smokeMC.gotoAndPlay(2);
				_soundController.playPoof();
				killCurrentDoc();
				shiftTasks();
			} else {
				trace("INFO FAIL!!!!!!!!!, onDocsUpInCloud :: _currentDoc.targetX? " + _currentDoc.targetX);
				_currentMetricPanel.dropQuality();
				TweenLite.to(_currentDoc, .2, {x:_currentDoc.targetX, y:_currentDoc.targetY, ease:Expo.easeOut, onComplete:backToOrgOnFail});
			}
		}
		
		private function onHideClueOnSupervisor(e:Event):void
		{
//			trace("INFO GameView :: onHideClueOnSupervisor, _currentTaskID:" + _currentTaskID);
			glowOff(_currentDoc);
			switch (_currentTaskID) {
				case 3:
				case 4:
				case 9:
					hideClueOnTheDestPlayer();
					break;
				case 5:
					hideAvailPlayerRooms();
					break;
			}
		}
		
		private function activateAction(e:GameEvent = null):void
		{
			_stopWatch.removeEventListener(GameEvent.ACTIVATE_AUTOBOT, activateAction);
			
			_actionButton.active = true;
			_actionButton.addEventListener(MouseEvent.MOUSE_DOWN, onTryAutobotRequested);
			
			if (_popupManager.isPopupOpen) _popupManager.onTurnOverHappened();
		}
		
		private function gameEnd(e:GameEvent = null):void
		{
			trace("INFO GameView :: gameEnd!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			
			_isGameEnd = true;
			clearGame();
			_stopWatch.reset();
	
			//-- center the actionButton
			var targetY:Number = Math.floor(Configuration.STAGE_H/2) - 10;
			TweenLite.to(_actionButton, .5, {delay:.3, y:targetY, ease:Quad.easeOut});
			
			//-- calculate the number of icons on the taskbar
			var numOfTasksOnTheTaskBar:uint = 0;
			for (var i:uint = 0; i<_totalTasks; i++) {
				var task:TaskIcon = _taskIconArr[i];
				if (task.visible && task.alpha >.1) numOfTasksOnTheTaskBar++;
			}
			_dataModel.setIncompleteTotal(numOfTasksOnTheTaskBar);
			
			//-- show the summary popup
			_popupManager.openSummaryPopup();
			
			dispatchEvent(new GameEvent(GameEvent.GAME_END));
		}
		
		private function onTryAutobotRequested(e:MouseEvent):void
		{
			trace("INFO GameView :: onTryAutobotRequested");
			
			_actionButton.removeEventListener(MouseEvent.MOUSE_DOWN, onTryAutobotRequested);
			
			if (!_isGameEnd) clearGame();
			_popupManager.closePopup();	//-- close summary popup if open
			
			TweenLite.to(_actionButton, .5, {delay:.2, alpha:0, ease:Quint.easeOut, onComplete:function(){_actionButton.visible = false;}});
			setAutobotView();
		}
		
		private function clearGame():void
		{
			trace("INFO GameView :: clearGame");
			
			clearInterval(_playPhoneSoundID);
			clearInterval(_loadNextIconIntervalID);
			clearInterval(_evaluateProductionID);
			
			_stopWatch.removeEventListener(GameEvent.DROP_INTERVAL, onDropIntervalRequested);
			_stopWatch.removeEventListener(GameEvent.GAME_END, gameEnd);
			_stopWatch.stop();
			
			_currentMetricPanel.stopBlinking();
			_popupManager.closePopup();
			_soundController.stopAllSounds();

			TweenMax.killAll();
			killCurrentMouseEvents();
			
			for (var i:uint = 0; i<=7; i++) {
//				trace("INFO GameView :: clearGame, stop person animation, _room" + i + ".person?" + this["_room" + i].person);
				this["_room" + i].person.gotoAndStop(1);
				_players[i].reset();
			}
			
			hideCloud();
			hideAvailPlayerRooms();
			TweenLite.delayedCall(.1, hideTextBubble);
			TweenLite.delayedCall(.2, killMatchingDoc);
			TweenLite.delayedCall(.3, hideClueOnTheDestPlayer);
			TweenLite.delayedCall(.4, killDocsInActivePlayers);
			TweenLite.delayedCall(.5, killCurrentDoc);
			TweenLite.delayedCall(.6, hideAllPlayerRooms);	//-- kill one more time to make sure
			
			var blockerAlpha:Number = (Configuration.isTesting)? .25:0;
			buildBlocker(blockerAlpha);
			swapChildren(_actionButton, _dummy);
		}
		
		private function setAutobotView():void
		{
			//-- Autobot Sequence Session
			trace("INFO GameView :: setAutobotView");	
			
			//-- reset stop watch
			_stopWatch.reset();
			
			//-- clear taskIcons and rebuild
			var diff:Number = INIT_ICON_X - START_ICON_X;
			for (var k:uint = 0; k<_totalTasks; k++) {
				var taskIcon:TaskIcon = _taskIconArr[k];
				if (taskIcon) {
					var targetX:Number = taskIcon.x - diff;
					TweenLite.killTweensOf(taskIcon);
					if (k == 0) {
						TweenLite.to(taskIcon, .4, {delay: .5 + (_totalTasks)*.2, alpha:-.5, x:targetX, ease:Quad.easeOut, onComplete:onTaskIconsHidden});
					} else {
						TweenLite.to(taskIcon, .4, {delay: .5 + (_totalTasks-k)*.2, alpha:-.5, x:targetX, ease:Quad.easeOut});
					}
				}
			}
			
			//-- remove remaining players		
			for (var j:uint = 2; j<=6; j++) {
				hidePerson(j, true, .75 + j*.3);
			}
		
			_room8.trainer.gotoAndStop(1);
			_room8.traineeF.gotoAndStop(1);
			_room8.traineeM.gotoAndStop(1);
			_room8.traineeP2.gotoAndStop(1);
			_room8.traineeP3.gotoAndStop(1);
			_room8.traineeP5.gotoAndStop(1);
			_room8.traineeP6.gotoAndStop(1);
			TweenLite.to(_room8.trainer, .5, {delay:1, alpha:0, onComplete:function() {_room8.trainer.visible = false;}});
			TweenLite.to(_room8.traineeF, .5, {delay:1.3, alpha:0, onComplete:function() {_room8.traineeF.visible = false;}});
			TweenLite.to(_room8.traineeM, .5, {delay:1.6, alpha:0, onComplete:function() {_room8.traineeM.visible = false;}});
			TweenLite.to(_room8.traineeP2, .5, {delay:1.6, alpha:0, onComplete:function() {_room8.traineeP2.visible = false;}});
			TweenLite.to(_room8.traineeP3, .5, {delay:1.3, alpha:0, onComplete:function() {_room8.traineeP3.visible = false;}});
			TweenLite.to(_room8.traineeP5, .5, {delay:1.9, alpha:0, onComplete:function() {_room8.traineeP5.visible = false;}});
			TweenLite.to(_room8.traineeP6, .5, {delay:1.6, alpha:0, onComplete:function() {_room8.traineeP6.visible = false;}});
			
			//-- metric panel
			TweenLite.to(_metricPanel1, .75, {delay:4, alpha:.5, ease:Quad.easeOut});
			buildMetricPanel2();
			_metricPanel2.alpha = 0;
			_metricPanel2.visible = true;
			TweenLite.to(_metricPanel2, .75, {delay:4.5, alpha:1, ease:Quad.easeOut});
			_currentMetricPanel = _metricPanel2;
			
			//-- gotoVideo button
			_gotoVideoButton = new ActionButton(4, false);
			addChild(_gotoVideoButton);
			_gotoVideoButton.x = Math.floor((Configuration.STAGE_W - _gotoVideoButton.width)/2);
			_gotoVideoButton.y = 973;
			_gotoVideoButton.active = false;
			_gotoVideoButton.addEventListener(MouseEvent.MOUSE_DOWN, onStartDemoRequested);
			_gotoVideoButton.alpha = 0;
			TweenLite.to(_gotoVideoButton, .75, {delay:5, alpha:1, onComplete:function() {_gotoVideoButton.active = true;}});
		}
		
		private function resetMetricPanel():void
		{
			trace("INFO GameView :: resetMetricPanel, _metricPanel1 is reset to _metricPanel2");
			
			_metricPanel2.reset();
			_currentMetricPanel = _metricPanel2;
		}
		
		private function onTaskIconsHidden():void
		{
			trace("INFO GameView :: onTaskIconsHidden");

			var isAutomated:Boolean = true;
			destroyTaskIcons();
			buildTaskIcons(isAutomated);
			
			TweenLite.delayedCall(3, startAutobotGame);
		}
		
		private function startAutobotGame():void
		{
			trace("\n=================================");
			trace("INFO GameView :: startAutobotGame");
			trace("\n=================================");
			
			_soundController.playAmbience(.3);
			_soundController.playPhone(.2);
			_playPhoneSoundID = setTimeout(playPhoneSound, 30000);
			
			_isSamplePlay = false;
			_isGameEnd = false;
			_isAutobotPlay	= true;
			_isAutotaskDone	= false;
			_isIconRecycling = false;
			_isDropIntervalRequested = false;
			
			destroyBlocker();
			resetMetricPanel();
			
			_currentTaskIcon = _taskIconArr[0];
			_currentTaskIcon.active = true;
			_currentTaskID = _currentTaskIcon.taskID;
			
			_room7.person.gotoAndPlay("fixing");
			
			//-- setInterval
			_nextIconIndex = 0;
			_iconLoadingCounter = 0;
			_startDemoCount = 50;
			loadNextIcon();
			trace("INFO GameView :: startAutobotGame, setInterval loadNextIcon");
			_loadNextIconIntervalID = setInterval(loadNextIcon, 1000);	//1500 to 1100, 05/16/14
			
			dispatchEvent(new GameEvent(GameEvent.AUTOBOT_START));
		}
		
		private function onStartDemoRequested(e:MouseEvent):void
		{
			trace("\n=================================");
			trace("INFO GameView :: onStartDemo(Video)Requested, videoSession started, gotoVideoButton pressed");
			trace("\n=================================");
			
			_soundController.stopAmbience();
			_soundController.stopPhone();
			_soundController.stopAllSounds();
			clearTimeout(_playPhoneSoundID);
			
			_startDemoCount = _iconLoadingCounter;
			_isVideoSession = true;
			dispatchEvent(new GameEvent(GameEvent.START_DEMO));	
			TweenLite.to(_gotoVideoButton, .5, {delay:1, alpha:0, onComplete:destroyGotoVideoButton});
		}
		
		private function destroyGotoVideoButton():void
		{
			if (_gotoVideoButton) {
				_gotoVideoButton.removeEventListener(MouseEvent.MOUSE_DOWN, onStartDemoRequested);
				_gotoVideoButton.destroy();
				removeChild(_gotoVideoButton);
				_gotoVideoButton = null;
			}
		}
		
		private function onRestartPressed(e:MouseEvent):void
		{
			_restartButton.removeEventListener(MouseEvent.MOUSE_DOWN, onRestartPressed);
			_learnMoreButton.removeEventListener(MouseEvent.MOUSE_DOWN, onLearnMorePressed);
			
			TweenLite.to(_learnMoreButton, .5, {alpha:0, y:Configuration.STAGE_H + 150, ease:Quint.easeOut, onComplete:function(){dispatchEvent(new GameEvent(GameEvent.RESTART_GAME));destroyEndUIs();}});
			TweenLite.to(_restartButton, .5, {delay:.3, alpha:0, y:Configuration.STAGE_H + 50, ease:Quint.easeOut});
		}
		
		private function onLearnMorePressed(e:MouseEvent):void
		{
			_restartButton.removeEventListener(MouseEvent.MOUSE_DOWN, onRestartPressed);
			_learnMoreButton.removeEventListener(MouseEvent.MOUSE_DOWN, onLearnMorePressed);
			_soundController.stopAllSounds();
			
			TweenLite.to(_learnMoreButton, .5, {alpha:0, y:Configuration.STAGE_H + 150, ease:Quint.easeOut, onComplete:function(){dispatchEvent(new GameEvent(GameEvent.START_PRESENTATION));destroyEndUIs();}});
			TweenLite.to(_restartButton, .5, {delay:.3, alpha:0, y:Configuration.STAGE_H + 50, ease:Quint.easeOut});
		}
		
		private function destroyEndUIs():void
		{
			_popupManager.closePopup();
			
			if (_restartButton) {
				_restartButton.destroy();
				removeChild(_restartButton);
			}
			if (_learnMoreButton) {
				_learnMoreButton.destroy();
				removeChild(_learnMoreButton);
			}
			_restartButton = null;
			_learnMoreButton = null;
			destroyBlocker();
		}
 }//c
}//p