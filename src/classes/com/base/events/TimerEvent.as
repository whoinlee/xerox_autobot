﻿package com.base.events
{
	import flash.events.Event;
	
	/*
	 * Author      : Ken Tylman
	 * URL         : http://www.base.com
	 * Class       : TimerEvent
	 * Modified    : August 20, 2012
	 * Description : Used to send timer events.
	 * */
	public class TimerEvent extends Event
	{
		/* Sent when a timer has advanced. */
		public static const TIMER_STEP:String = "TIMER_STEP";

		/*
		 * Function    : TimerEvent
		 * Description : Constructor
		 * Input
		 *     a_type - The event to dispatch. Relayed to the super class.
		 *     a_bubbles - Relayed to the super class.
		 *     a_cancelable - Relayed to the super class.
		 * Output
		 *     none
		 * Returns
		 *     none
		 * */
		public function TimerEvent(a_type:String, a_bubbles:Boolean = false,
			a_cancelable:Boolean = false):void
		{
			/* Initialize the super class. */
			super(a_type, a_bubbles, a_cancelable);
		} /* End TimerEvent */

	} /* End TimerEvent class */
} /* End com.base.events package */