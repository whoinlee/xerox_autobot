package com.base.utils {
	import com.base.events.TimerEvent;

	import flash.events.Event;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.Dictionary;

	
	/*
	* Author      : Ken Tylman
	* URL         : http://www.base.com
	* Class       : Slider
	* Modified    : August 20, 2012
	* Description : Implements a basic sound manager class.
	* */
	public class SoundManager
	{
		/* If compiler flag defined for IOS. */
//		if (CONFIG::IOS == 1)
//		{
//			/* Comment this line out if not building with at least air 3.5 */
//			SoundMixer.audioPlaybackMode = AudioPlaybackMode.AMBIENT;
//		}
		
		/* Define constants used. */
		public static const STOPPED:String = "STOPPED";
		public static const PLAYING:String = "PLAYING";
		
		/* Sound channels. */
		private static var m_musicSoundChannel:SoundChannel; 					// Only a single channel for music
		private static const m_soundChannelSize:Number = 10;					// How many sound channels
		private static var m_availableChannels:Array = new Array();
		private static var m_oldestChannels:Array = new Array();
		private static var m_effectsSoundChannel:Array = new Array(SoundManager.m_soundChannelSize); 	// Array to store the sound channels
		
		/* Dictionaries to store sounds. */
		private static var m_soundLibrary:Dictionary = new Dictionary();		// Library of all stored sounds
		private static var m_soundLibraryVolume:Dictionary = new Dictionary();
		
		/* Mute flag. True if sound is muted. */
		private static var m_mute:Boolean = true;
		
		/* Music properties. */
		private static var m_musicName:String = "";
		private static var m_musicLoops:Boolean = true;
		private static var m_musicState:String = SoundManager.STOPPED;
		private static var m_musicStoppedPosition:Number = 0;
		private static var m_musicFadeStartTime:uint;
		private static var m_musicFadeTime:uint;
		
		/* Volume Properties. */
		private static var m_musicVolume:Number = 1.0;
		private static var m_musicTargetVolume:Number = 1.0;
		private static var m_globalVolume:Number = 1.0;
		private static var m_effectsVolume:Number = 1.0;
		private static var m_fadingMusicVolume:Boolean = false;
		
		private static var m_animTimer:AnimationTimer = null;
		/*
		* Function    : SoundManager
		* Description : Constructor
		* Input
		*     Constructor
		* Output
		*     none
		* Returns
		*     void
		* */
		public function SoundManager():void
		{
		} /* End SoundManager() */
		
		/*
		* Function    : storeAudio
		* Description : Constructor
		* Input
		*     a_assetName - The name of the sound to store.
		*     a_audioClass - The sound class to store.
		*     a_volume - The volume level for the sound.
		* Output
		*     none
		* Returns
		*     void
		* */
		public static function storeAudio(a_assetName:String, a_audioClass:Class, a_volume:Number = 1.0):void
		{
			SoundManager.m_soundLibrary[a_assetName] = new a_audioClass;
			SoundManager.m_soundLibraryVolume[a_assetName] = a_volume;
		} /* End storeAudio() */
		
		/*
		* Function    : setMusic
		* Description : Constructor
		* Input
		*     a_assetName - The name of the sound to use for the music. If empty, the
		*                   last sound set for music will be used.
		*     a_loop - True if the music is to loop.
		* Output
		*     none
		* Returns
		*     void
		* */
		public static function setMusic(a_assetName:String = "", a_loop:Boolean = true):void
		{
			/* Use the last sound set for music. */
			if ("" == a_assetName)
			{
				a_assetName = SoundManager.m_musicName;
			}
			
			/* Use a new sound for music. */
			if (a_assetName != SoundManager.m_musicName)
			{
				SoundManager.m_musicName = a_assetName;
				SoundManager.m_musicLoops = a_loop;
			}
		} /* End setMusic() */
		
		/*
		* Function    : playMusic
		* Description : plays the music
		* Input
		*     a_assetName - The name of the sound to use for the music. If empty, the
		*                   last sound set for music will be used.
		*     a_loop - True if the music is to loop.
		*     a_volume - The volume of the music.
		* Output
		*     none
		* Returns
		*     void
		* */
		public static function playMusic(a_assetName:String = "", a_loop:Boolean = true,
			a_volume:Number = 1.0):void
		{
			/* The music must be stopped before playing new music. */
			if (SoundManager.STOPPED != SoundManager.m_musicState)
			{
				return;
			}
			
			/* Use the last sound set for music. */
			if ("" == a_assetName)
			{
				a_assetName = SoundManager.m_musicName;
			}
			
			/* Use a new sound for music. */
			if (a_assetName != SoundManager.m_musicName)
			{
				SoundManager.m_musicName = a_assetName;
				SoundManager.m_musicLoops = a_loop;
			}
			
			/* Sound is muted or no sound is set for music. */
			if ((true == SoundManager.m_mute) || ("" == SoundManager.m_musicName))
			{
				return;
			}
			
			/* Get the music sound from the stored sounds. */
			if (null != SoundManager.m_soundLibrary[SoundManager.m_musicName])
			{
				var l_loops:Number = 0;
				
				/* Loop indefinitely. */
				if (true == a_loop)
				{
					l_loops = int.MAX_VALUE;
				}
				
				/* Set the volume of the sound and play the music. */
				var l_transform:SoundTransform = new SoundTransform();
				l_transform.volume = SoundManager.m_musicVolume*SoundManager.m_globalVolume*
					SoundManager.m_soundLibraryVolume[SoundManager.m_musicName]*a_volume;
				SoundManager.m_musicSoundChannel = SoundManager.m_soundLibrary[SoundManager.m_musicName].play(
					0,	l_loops, l_transform);
				SoundManager.m_musicState = SoundManager.PLAYING;
			}
			else
			{
				throw new Error("No such asset: " + a_assetName);
			}
		} /* End playMusic() */
		
		/*
		* Function    : setGlobalVolume
		* Description : Sets the global volume level.
		* Input
		*     a_volume - The volume level.
		* Output
		*     none
		* Returns
		*     void
		* */
		public static function setGlobalVolume(a_volume:Number = 1.0):void
		{
			SoundManager.m_globalVolume = a_volume;
			SoundManager.setMusicVolume(SoundManager.m_musicVolume);
		} /* End setGlobalVolume() */
		
		/*
		* Function    : setEffectsVolume
		* Description : Sets the effects volume level.
		* Input
		*     a_volume - The volume level.
		* Output
		*     none
		* Returns
		*     void
		* */
		public static function setEffectsVolume(a_volume:Number = 1.0):void
		{
			SoundManager.m_effectsVolume = a_volume;
		} /* End setEffectsVolume() */
		
		/*
		* Function    : setMusicVolume
		* Description : Sets the music volume level.
		* Input
		*     a_volume - The volume level.
		* Output
		*     none
		* Returns
		*     void
		* */
		public static function setMusicVolume(a_volume:Number = 1.0, a_fade:Boolean = false,
			a_fadeTime:uint = 0):void
		{
			/* Music is playing and sound is not muted. */
			if ((null != SoundManager.m_musicSoundChannel) && (false == SoundManager.m_mute))
			{
				/* The music is not to be faded. */
				if ((false == a_fade) || (null == SoundManager.m_animTimer))
				{
					/* Set the music volume. */
					SoundManager.m_musicVolume = a_volume;
					
					var l_transform:SoundTransform = new SoundTransform();
					l_transform.volume = SoundManager.m_musicVolume*SoundManager.m_globalVolume;
					SoundManager.m_musicSoundChannel.soundTransform = l_transform;
					SoundManager.m_fadingMusicVolume = false;
				}
				/* The music is to be faded to the new volume. */
				else
				{
					/* The music is already fading. */
					if (true == SoundManager.m_fadingMusicVolume)
					{
						/* Set the start volume to the current volume. */
						SoundManager.m_musicVolume = SoundManager.m_musicSoundChannel.soundTransform.volume;
					}
					/* Set the target volume and fade info. */
					SoundManager.m_musicTargetVolume = a_volume*SoundManager.m_globalVolume*
						SoundManager.m_soundLibraryVolume[SoundManager.m_musicName];
					
					SoundManager.m_fadingMusicVolume = true;
					SoundManager.m_musicFadeStartTime = SoundManager.m_animTimer.m_elapsedMilliseconds;
					SoundManager.m_musicFadeTime = a_fadeTime;
				}
			}
			else
			{
				/* Store the music volume. */
				SoundManager.m_musicVolume = a_volume;
			}
		} /* End setMusicVolume() */
		
		/*
		* Function    : initialize
		* Description : initialize the sound manager.
		* Input
		*     a_animationTimer - The timer to use for volume fades..
		* Output
		*     none
		* Returns
		*     void
		* */
		public static function initialize(a_animationTimer:AnimationTimer):void
		{
			SoundManager.m_animTimer = a_animationTimer;
			
			if(null != SoundManager.m_animTimer)
			{
				SoundManager.m_animTimer.addEventListener(TimerEvent.TIMER_STEP, onStep);
			}
			
			/* Set up the available sound channels. */
			for (var i:uint = 0; i < SoundManager.m_soundChannelSize; i++)
			{
				SoundManager.m_availableChannels.push(i);
			}
		} /* End initialize() */
		
		/*
		* Function    : onStep
		* Description : This method processes each animation step for the timer specified
		*               in the initialize method.
		* Input
		*     event - The animation step event.
		* Output
		*     none
		* Returns
		*     void
		* */
		private static function onStep(event:TimerEvent):void
		{				
			/* There is no music set. */
			if (null == SoundManager.m_musicSoundChannel)
			{
				/* Unset the fading flag. */
				SoundManager.m_fadingMusicVolume = false;
			}

			/* The music is not fading. */
			if (false == SoundManager.m_fadingMusicVolume)
			{
				return;
			}
			
			var l_animTimer:AnimationTimer = event.target as AnimationTimer;
			
			/* Calculate the time since the music started fading. */
			var l_timeDiff:uint = l_animTimer.m_elapsedMilliseconds -
				SoundManager.m_musicFadeStartTime;
			
			/* Calculate the current music volume. */
			var l_volume:Number  = AnimationCurve.getPosition(AnimationCurve.QUADRATIC_INOUT,
				l_timeDiff, SoundManager.m_musicVolume, SoundManager.m_musicTargetVolume,
				SoundManager.m_musicFadeTime);
			
			/* The time has exceeded the fade time. */
			if (l_timeDiff >= SoundManager.m_musicFadeTime)
			{
				/* Set the music volume to the target and disable the fading. */
				l_volume = SoundManager.m_musicTargetVolume;
				SoundManager.m_musicVolume = SoundManager.m_musicTargetVolume;
				SoundManager.m_fadingMusicVolume = false;
			}
			
			/* Set the music volume. */
			var l_transform:SoundTransform = new SoundTransform();
			l_transform.volume = l_volume*SoundManager.m_globalVolume;
			SoundManager.m_musicSoundChannel.soundTransform = l_transform;
			
			/* The music is not fading and the volume is 0. */
			if ((false == SoundManager.m_fadingMusicVolume) && (0 == SoundManager.m_musicVolume))
			{
				/* Stop the music to save cycles. */
				SoundManager.stopMusic();
			}
		} /* End onStep() */
		
		/*
		* Function    : playSound
		* Description : Plays a sound.
		* Input
		*     a_assetName - The name of the sound to play.
		*     a_volume - The volume to play this sound instance at.
		*     a_priority - True if this is a priority sound. If there are no available sound
		*                  channels, then a priority sound will stop a non-priority sound
		*                  to use it's channel. If there are no available channels and all
		*                  sounds playing are priority sounds, this sound will not play.
		* Output
		*     none
		* Returns
		*     void
		* */
		public static function playSound(a_assetName:String, a_volume:Number = 1.0,
			a_priority:Boolean = false, loops:int = 0):SoundChannel
		{
			var l_function:Function;
			var l_completeHandler:Function =
				function(event:Event):void {onComplete(event, l_nextChannel, l_function);};
			l_function = l_completeHandler;
			
			/* Sound is muted. */
			if (true == SoundManager.m_mute)
			{
				return null;
			}
			
			var l_nextChannel:uint;
			
			/* Get the next available sound channel. */
			if (0 != SoundManager.m_availableChannels.length)
			{
				l_nextChannel = SoundManager.m_availableChannels.shift();
			}
			/* There are no available channels. The sound is a priority sound and there is a non-priority
			*  sound that can be stopped. */
			else if ((true == a_priority) && (0 != SoundManager.m_oldestChannels.length))
			{
				l_nextChannel = SoundManager.m_oldestChannels.shift();
				SoundManager.m_effectsSoundChannel[l_nextChannel].stop();
			}
			/* Can't play the sound. */
			else
			{
				return null;
			}
			
			/* Set the sound volume. */
			var l_transform:SoundTransform = new SoundTransform();
			l_transform.volume = SoundManager.m_effectsVolume*SoundManager.m_globalVolume*
				SoundManager.m_soundLibraryVolume[a_assetName]*a_volume;

			/* Play the sound. */
			SoundManager.m_effectsSoundChannel[l_nextChannel] =
				SoundManager.m_soundLibrary["SILENCE"].play();
			SoundManager.m_effectsSoundChannel[l_nextChannel] =
				SoundManager.m_soundLibrary[a_assetName].play(0, loops, l_transform);
//				SoundManager.m_soundLibrary[a_assetName].play(0, 1, l_transform);
			SoundManager.m_effectsSoundChannel[l_nextChannel].addEventListener(Event.SOUND_COMPLETE,
				l_completeHandler);
			
			/* Remove the current channel from the oldest channel array. */
			var l_newArray:Array = new Array;
			
			for (var i:uint = 0; i < SoundManager.m_oldestChannels.length; i++)
			{
				if (l_nextChannel != SoundManager.m_oldestChannels[i])
				{
					l_newArray.push(SoundManager.m_oldestChannels[i]);
				}
			}
			
			SoundManager.m_oldestChannels = l_newArray;
			
			/* If the sound is non-priority then add it to the end of the oldest channel array. */
			if (false == a_priority)
			{
				SoundManager.m_oldestChannels.push(l_nextChannel);
			}
			
			return SoundManager.m_effectsSoundChannel[l_nextChannel];
			
		} /* End playSound() */
		
		/*
		* Function    : onComplete
		* Description : Handles the completion of a sound playing.
		* Input
		*     event - The complete event.
		*     a_soundChannel - The sound channel that finished playing.
		* Output
		*     none
		* Returns
		*     void
		* */
		public static function onComplete(event:Event, a_soundChannel:uint, a_function:Function):void
		{
			SoundManager.m_effectsSoundChannel[a_soundChannel].removeEventListener(Event.SOUND_COMPLETE,
				a_function);
			
			/* Make the channel available again. */
			SoundManager.m_availableChannels.push(a_soundChannel);
		} /* End onComplete() */
		
		/*
		* Function    : stopMusic
		* Description : Stops the music.
		* Input
		*     a_remove - True if the music properties are to be cleared.
		* Output
		*     none
		* Returns
		*     void
		* */
		public static function stopMusic(a_remove:Boolean = false):void
		{
			/* Music is not being played. */
			if (null != SoundManager.m_musicSoundChannel)
			{
				SoundManager.m_musicStoppedPosition = SoundManager.m_musicSoundChannel.position;
				SoundManager.m_musicSoundChannel.stop();
			}
			
			/* Clear the music properties. */
			if (true == a_remove)
			{
				SoundManager.m_musicSoundChannel = null;
				SoundManager.m_musicName = "";
				SoundManager.m_musicStoppedPosition = 0;
			}
			
			SoundManager.m_musicState = SoundManager.STOPPED;
			
			if (true == SoundManager.m_fadingMusicVolume)
			{
				SoundManager.m_fadingMusicVolume = false;
				SoundManager.m_musicVolume = SoundManager.m_musicTargetVolume;
			}
		} /* End stopMusic() */
		
		/*
		* Function    : setMute
		* Description : Mutes the audio.
		* Input
		*     a_mute - True to mute.
		*     a_playMusic - True to play the music when unmuting.
		* Output
		*     none
		* Returns
		*     void
		* */
		public static function setMute(a_mute:Boolean = true, a_playMusic:Boolean = true):void
		{
			/* The sound is already muted. */
			if (a_mute == SoundManager.m_mute)
			{
				return;
			}
			
			SoundManager.m_mute = a_mute;
			
			/* Stop all sounds. */
			if (true == SoundManager.m_mute)
			{
				for (var i:uint = 0; i < SoundManager.m_effectsSoundChannel.length; i++)
				{
					if (null != SoundManager.m_effectsSoundChannel[i])
					{
						SoundManager.m_effectsSoundChannel[i].stop();
					}
				}
				SoundManager.stopMusic();
			}
			/* Play the music. */
			else if (true == a_playMusic)
			{				
				SoundManager.playMusic(SoundManager.m_musicName, SoundManager.m_musicLoops,
					SoundManager.m_musicVolume);
			}
		} /* End setMute() */
		
	} /* End SoundManager class */
} /* End com.base.utils package */