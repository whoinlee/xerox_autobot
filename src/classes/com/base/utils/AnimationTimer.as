﻿package com.base.utils
{
	import com.base.events.TimerEvent;
	
	import flash.display.Sprite;
	import flash.utils.getTimer;
	import flash.events.Event;
	
	
	/*
	 * Author      : Ken Tylman
	 * URL         : http://www.base.com
	 * Class       : AnimationTimer
	 * Modified    : August 20, 2012
	 * Description : Implements a timer used to manage animations.
	 * Notes       : The ENTER_FRAME event is used to drive the timer as it
	 *               seems a more consistent approach than using a
	 *               flash.util.timer.
	 * */
	public class AnimationTimer extends Sprite
	{
		/* Used to store the total elapsed milliseconds since the last whole
		 * second. */
		public var m_elapsedMilliseconds:uint;
		public var m_timeDiff:uint;
		private var m_currentTime:uint;
		
		private var m_running:Boolean = false;
		
		/* The time in milliseconds between dispatching animation events. */
		public var m_delay:uint;
		
		/* The last time an animation event was dispatched. */
		public var m_lastTime:uint;
		
		/*
		 * Function    : AnimationTimer
		 * Description : Constructor
		 * Input
		 *     a_delay - The time in milliseconds between dispatching animation
		 *               events.
		 * Output
		 *     none
		 * Returns
		 *     none
		 * */
		public function AnimationTimer(a_delay:uint):void
		{
			/* Initialize the timer properties. */
			this.m_elapsedMilliseconds = 0;
			this.m_delay = a_delay;			
		} /* End AnimationTimer() */
		
		/*
		 * Function    : resetTimer
		 * Description : Resets the timer to its initial values.
		 * Input
		 *     none
		 * Output
		 *     none
		 * Returns
		 *     none
		 * */
		public function resetTimer():void
		{			
			/* Reinitialize the timer values. */
			this.m_elapsedMilliseconds = 0;
			
		} /* End resetTimer() */

		/*
		 * Function    : stopTimer
		 * Description : Stops the timer.
		 * Input
		 *     none
		 * Output
		 *     none
		 * Returns
		 *     none
		 * */
		public function stopTimer():void
		{
			if (true == this.m_running)
			{
				/* Stop listening to the ENTER_FRAME events. */
				this.removeEventListener(Event.ENTER_FRAME, this.onEnterFrameHandler);
				
				this.m_running = false;
			}
		} /* End stopTimer() */
		
		/*
		 * Function    : stopTimer
		 * Description : Starts the timer.
		 * Input
		 *     none
		 * Output
		 *     none
		 * Returns
		 *    none
		 * */
		public function startTimer():void
		{
			if (false == this.m_running)
			{
				/* Set the last time to the current time. The first animation event
				 * should be dispatched m_delay milliseconds from now. */
				this.m_lastTime = getTimer();
				
				/* Start listening to the ENTER_FRAME events. */
				this.addEventListener(Event.ENTER_FRAME, this.onEnterFrameHandler);
				
				this.m_running = true;
			}
		} /* End startTimer() */
		
		/*
		 * Function    : onEnterFrameHandler
		 * Description : Processes the ENTER_FRAME events and dispatches animation
		 *               events based on the m_delay time.
		 * Input
		 *     e - ENTER_FRAME event.
		 * Output
		 *     none
		 * Returns
		 *     none
		 * */
		public function onEnterFrameHandler(e:Event):void
		{
			/* Get and store the current time. */
			this.m_currentTime = getTimer();
			
			/* It is time to dispatch an animation event. */
			if ((this.m_currentTime - this.m_lastTime) >= this.m_delay)
			{
				var l_event:TimerEvent;
				
				/* Calculate the total elapsed time. */
				this.m_elapsedMilliseconds += this.m_currentTime - this.m_lastTime;
				
				/* Calculate the time elapsed since the last ENTER_FRAME event. */
				this.m_timeDiff = this.m_currentTime - this.m_lastTime;
				
				/* Create the new animation event to dispatch. */
				l_event = new TimerEvent(TimerEvent.TIMER_STEP);
				
				/* Dispatch the event. */
				dispatchEvent(l_event);
				
				/* Store the current time as the last time an animation
				 * event was dispatched. */
				this.m_lastTime = getTimer();
			}
		} /* End onTimer */

	} /* End AnimationTimer class */
} /* End com.base.utils package */