﻿package com.base.utils
{
	import com.base.utils.AnimationTimer;
	
	/*
	 * Author      : Ken Tylman
	 * URL         : http://www.base.com
	 * Class       : AnimationCurve
	 * Modified    : August 20, 2012
	 * Description : Calculates positions along a line using various
	 *               easing functions.
	 * Notes       : This class is based on work located at the following.
	 *				 http://timotheegroleau.com/Flash/experiments/easing_function_generator.htm
	 *				 http://www.robertpenner.com/easing/penner_chapter7_tweening.pdf
	 * */
	public class AnimationCurve
	{
		/* Identify the various easing functions available. */
		public static const NO_EASE:String = "NO_EASE";
		public static const QUADRATIC_IN:String = "QUADRATIC_IN";
		public static const QUADRATIC_OUT:String = "QUADRATIC_OUT";
		public static const QUADRATIC_INOUT:String = "QUADRATIC_INOUT";
		public static const CUBIC_IN:String = "CUBIC_IN";
		public static const CUBIC_OUT:String = "CUBIC_OUT";
		public static const CUBIC_INOUT:String = "CUBIC_INOUT";
		public static const QUARTIC_IN:String = "QUARTIC_IN";
		public static const QUARTIC_OUT:String = "QUARTIC_OUT";
		public static const QUARTIC_INOUT:String = "QUARTIC_INOUT";
		public static const QUINTIC_IN:String = "QUINTIC_IN";
		public static const QUINTIC_OUT:String = "QUINTIC_OUT";
		public static const QUINTIC_INOUT:String = "QUINTIC_INOUT";
		public static const SINE_IN:String = "SINE_IN";
		public static const SINE_OUT:String = "SINE_OUT";
		public static const SINE_INOUT:String = "SINE_INOUT";
		public static const EXP_IN:String = "EXP_IN";
		public static const EXP_OUT:String = "EXP_OUT";
		public static const EXP_INOUT:String = "EXP_INOUT";
		public static const CIRCULAR_IN:String = "CIRCULAR_IN";
		public static const CIRCULAR_OUT:String = "CIRCULAR_OUT";
		public static const CIRCULAR_INOUT:String = "CIRCULAR_INOUT";
		
		/*
		 * Function    : AnimationCurve
		 * Description : Constructor
		 * Input
		 *     none
		 * Output
		 *     none
		 * Returns
		 *     none
		 * */
		public function AnimationCurve()
		{
		} /* End AnimationCurve() */
		
		/*
		 * Function    : getPosition
		 * Description : Returns a position between the given values using the
		 *               specified easing function and time values.
		 * Input
		 *     a_easing - The easing function to use.
		 *     a_time - The time elapsed.
		 *     a_begin - The begining position value.
		 *     a_end - The ending position value.
		 *     a_duration - The amount of time to traverse the entire distance from
		 *                  a_begin to a_end.
		 * Output
		 *     none
		 * Returns
		 *     Number - The position corresponding to the input given.
		 * */
		public static function getPosition(a_easing:String,
		   a_time:Number, a_begin:Number, a_end:Number, a_duration:Number):Number
		{
			/* Calculate the total distance.*/
			var l_change:Number = a_end - a_begin;
			
			/* Store the time locally. Some of the easing functions modify the
			 * value. */
			var l_time:Number = a_time;
			/* Calculate the resulting position. */
			switch (a_easing)
			{
				case AnimationCurve.QUADRATIC_IN:
					return(l_change*(l_time/=a_duration)*l_time + a_begin);
					
				case AnimationCurve.QUADRATIC_OUT:
					return(-l_change*(l_time/=a_duration)*(l_time - 2) + a_begin);
					
				case AnimationCurve.QUADRATIC_INOUT:
					if((l_time/=a_duration/2) < 1)
					{
						return(l_change/2*l_time*l_time + a_begin);
					}
					else
					{
						return(-l_change/2*((--l_time)*(l_time-2) - 1) + a_begin);
					}
					
				case AnimationCurve.CUBIC_IN:
					return(l_change*Math.pow(l_time/a_duration, 3) + a_begin);
					
				case AnimationCurve.CUBIC_OUT:
					return(l_change*(Math.pow(l_time/a_duration - 1, 3) + 1) + a_begin);
					
				case AnimationCurve.CUBIC_INOUT:
					if((l_time/=a_duration/2) < 1)
					{
						return(l_change/2*Math.pow(l_time, 3) + a_begin);
					}
					else
					{
						return(l_change/2*(Math.pow(l_time-2, 3) + 2)  + a_begin);
					}
					
				case AnimationCurve.QUARTIC_IN:
					return(l_change*Math.pow(l_time/a_duration, 4) + a_begin);
					
				case AnimationCurve.QUARTIC_OUT:
					return(-l_change*(Math.pow(l_time/a_duration - 1, 4) - 1) + a_begin);
					
				case AnimationCurve.QUARTIC_INOUT:
					if((l_time/=a_duration/2) < 1)
					{
						return(l_change/2*Math.pow(l_time, 4) + a_begin);
					}
					else
					{
						return(-l_change/2*(Math.pow(l_time-2, 4) - 2)  + a_begin);
					}
					
				case AnimationCurve.QUARTIC_IN:
					return(l_change*Math.pow(l_time/a_duration, 4) + a_begin);
					
				case AnimationCurve.QUARTIC_OUT:
					return(-l_change*(Math.pow(l_time/a_duration - 1, 4) - 1) + a_begin);
					
				case AnimationCurve.QUARTIC_INOUT:
					if((l_time/=a_duration/2) < 1)
					{
						return(l_change/2*Math.pow(l_time, 4) + a_begin);
					}
					else
					{
						return(-l_change/2*(Math.pow(l_time-2, 4) - 2)  + a_begin);
					}
					
				case AnimationCurve.QUINTIC_IN:
					return(l_change*Math.pow(l_time/a_duration, 5) + a_begin);
					
				case AnimationCurve.QUINTIC_OUT:
					return(l_change*(Math.pow(l_time/a_duration - 1, 5) + 1) + a_begin);
					
				case AnimationCurve.QUINTIC_INOUT:
					if((l_time/=a_duration/2) < 1)
					{
						return(l_change/2*Math.pow(l_time, 5) + a_begin);
					}
					else
					{
						return(l_change/2*(Math.pow(l_time-2, 5) + 2)  + a_begin);
					}
					
				case AnimationCurve.SINE_IN:
					return(l_change*(1 - Math.cos(l_time/a_duration*(Math.PI/2))) + a_begin);
					
				case AnimationCurve.SINE_OUT:
					return(l_change*Math.sin(l_time/a_duration * (Math.PI/2)) + a_begin);
					
				case AnimationCurve.SINE_INOUT:
					if((l_time/=a_duration/2) < 1)
					{
						return(l_change/2*Math.pow(l_time, 5) + a_begin);
					}
					else
					{
						return(l_change/2*(1 - Math.cos(Math.PI*l_time/a_duration)) + a_begin);
					}
					
				case AnimationCurve.EXP_IN:
					return(l_change*Math.pow(2, 10*(l_time/a_duration - 1)) + a_begin);
					
				case AnimationCurve.EXP_OUT:
					return(l_change*(-Math.pow(2, -10*l_time/a_duration) + 1) + a_begin);
					
				case AnimationCurve.EXP_INOUT:
					if((l_time/=a_duration/2) < 1)
					{
						return(l_change/2*Math.pow(2, 10*(l_time - 1)) + a_begin);
					}
					else
					{
						return(l_change/2*(-Math.pow(2, -10*--l_time) + 2) + a_begin);
					}
					
				case AnimationCurve.CIRCULAR_IN:
					return(l_change*(1 - Math.sqrt(1 - (l_time/=a_duration)*l_time)) + a_begin);
					
				case AnimationCurve.CIRCULAR_OUT:
					return(l_change*Math.sqrt(1 - (l_time = l_time/a_duration - 1)*l_time) + a_begin);
					
				case AnimationCurve.CIRCULAR_INOUT:
					if((l_time/=a_duration/2) < 1)
					{
						return(l_change/2*(1 - Math.sqrt(1 - l_time*l_time)) + a_begin);
					}
					else
					{
						return(l_change/2*(Math.sqrt(1 - (l_time -= 2)*l_time) + 1) + a_begin);
					}
					
				case AnimationCurve.NO_EASE:
				default:
					return(l_change*l_time/a_duration + a_begin);
			} /* End a_easing switch */
			
			return(a_begin);
		} /* End getCurvePositionAsNumber() */
		
		
	} /* End AnimationCurve class */
} /* End com.base.utils package */